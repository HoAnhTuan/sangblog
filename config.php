<?php
date_default_timezone_set("asia/ho_chi_minh");
$root = realpath(dirname(__FILE__));
$root = str_replace("\\", '/', $root);
$domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)) || (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'))
    ? "https://" : "http://";
$url = $protocol . $domain;
$domain_name = 'keonhacai9.com';

define('ASSET_VERSION', '3.27');
define('BASE_URL', $url . '/');
define('DOMAIN_PLAY', $url . '/');
define('BASE_ADMIN_URL', BASE_URL . "admin/");
define('MEDIA_NAME', "picture/"); //Tên đường dẫn lưu media
define('MEDIA_PATH', str_replace('\\', '/', $root . DIRECTORY_SEPARATOR . MEDIA_NAME)); //Đường dẫn lưu media
define('MEDIA_HIDE_FOLDER', 'mcith|thumb');
define('MEDIA_URL', "https://$domain_name/" . MEDIA_NAME);
define('MEDIA_URL_CDN', MEDIA_URL);
define('API_DATACENTER', "http://api.asoikeo.com/");
define('TEMPLATES_ASSETS', BASE_URL . 'public/');

// vì chạy crontab trên server nên cần phải để thế này
define('DB_DEFAULT_HOST', 'localhost');
define('DB_DEFAULT_USER', 'keonhacai9');
define('DB_DEFAULT_PASSWORD', 'E9bBS:i.Z{hNGQVCCo1Z');
define('DB_DEFAULT_NAME', 'keonhacai9');

// if ($domain === $domain_name) {
//     define('DB_DEFAULT_USER', 'keonhacai9');
//     define('DB_DEFAULT_PASSWORD', 'E9bBS:i.Z{hNGQVCCo1Z');
//     define('DB_DEFAULT_NAME', 'keonhacai9');
// }else {
//     define('DB_DEFAULT_USER', 'root');
//     define('DB_DEFAULT_PASSWORD', '');
//     define('DB_DEFAULT_NAME', 'keonhacai9');
// }


define('MAINTAIN_MODE', FALSE); //Bảo trì

if ($domain === $domain_name) {
    define('DEBUG_MODE', FALSE);
    define('CACHE_MODE', TRUE);
    define('CACHE_FILE_MODE', TRUE);
    define('CACHE_ADAPTER', 'redis');
    define('CACHE_PREFIX_NAME', 'KNC9_');
} else {
    define('DEBUG_MODE', TRUE);
    define('CACHE_MODE', FALSE);
    define('CACHE_FILE_MODE', FALSE);
    define('CACHE_ADAPTER', 'file');
    define('CACHE_PREFIX_NAME', 'KNC9T_');
}
define('CACHE_TIMEOUT_LOGIN', 1800);

//Config zalo
define('ZALO_APP_ID_CFG', '');
define('ZALO_APP_SECRET_KEY_CFG', '');
define('ZALO_CAL_BACK', BASE_URL . 'auth/loginzalo');

//Config zalo
define('API_KEY', '');

define('FB_API', '');
define('FB_SECRET', '');
define('FB_VER', 'v2.9');

define('GG_API', '');
define('GG_SECRET', '');
define('GG_KEY', ''); //AIzaSyAhR8OG9cUL1jDfAAc6i35nt5Ki1ZJnykA
define('GG_CAPTCHA_MODE', FALSE);
define('GG_CAPTCHA_SITE_KEY', '');
define('GG_CAPTCHA_SECRET_KEY', '');
