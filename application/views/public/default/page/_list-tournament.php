<?php if (!empty($data)):?>
<div class="row no-gutters border-top border-left mt-2">
    <?php foreach ($data as $item): ?>
        <a href="javascript:;" rel="nofollow" data-tournament-id="<?php echo $item->tournament_id ?>" title="<?php echo $item->title ?>"
           class="col-6 col-md-3 mb-1 btn border-right border-bottom btnChooseTournament <?php echo $item->tournament_id == 17 ? "btn-danger" : "btn-secondary" ?> d-flex align-items-center pl-1">
            <?php echo getLogoTournament($item->tournament_id, $item->title, "logo-tournament mr-2") ?>
            <?php echo $item->name ?>
        </a>
    <?php endforeach; ?>
</div>
<?php endif;?>