<section class="section mt-70">
	<section class="section-header category-name">
		<h1 class="category-title">Bảng xếp hạng</h1>
	</section>
	<section class="section-body">
		<div class="table-result-bongda">
			<h3 class="title-separate mb-3">Ngoại hạng anh</h3>
			<?php $this->load->view(TEMPLATE_PATH . 'page/_list-tournament', ['data' => $list_tournament]) ?>
			<div id="ajax_content" class="ajax-content">
				<?php
				if (!empty($data_tournament)) foreach ($data_tournament as $oneTournament) :
					if (!empty($oneTournament->is_round)) :
						$data_ranking = json_decode($oneTournament->data_ranks);
						if (!empty($data_ranking)) : foreach ($data_ranking as $key) :
								$tablerows = $key->tablerows;
				?>
								<div data-tournament-id="<?php echo $oneTournament->tournament_id ?>" class="<?php echo $oneTournament->tournament_id != 17 ? "d-none" : "" ?>">
									<div class="py-2 px-3 text-center text-white text-capitalize"><?php echo $key->name ?></div>
									<div class="overflow-auto">
										<table class="table table-striped table-bxh">
											<thead>
												<tr>
													<td>#</td>
													<td class=" team">Đội</td>
													<td>Trận</td>
													<td>Thắng</td>
													<td>Hòa</td>
													<td>Bại</td>
													<td>HS</td>
													<td>Điểm</td>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($tablerows as $index => $item) : ?>
													<tr>
														<td><?php echo $index + 1 ?></td>
														<td><?php echo $item->team->mediumname ?></td>
														<td><?php echo $item->total ?></td>
														<td><?php echo $item->winTotal ?></td>
														<td><?php echo $item->drawTotal ?></td>
														<td><?php echo $item->lossTotal ?></td>
														<td><?php echo $item->goalDiffTotal ?></td>
														<td><?php echo $item->pointsTotal ?></td>
													</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
						<?php endforeach;
						endif;
					else :  ?>
						<div class="">
							Đang cập nhật
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
</section>