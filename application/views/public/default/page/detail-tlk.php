<?php if (!empty($oneItem)) : ?>
    <style type="text/css">
        /***Page tỷ lệ kèo**/
        .tb-bet-data .odd-competition .keo-search:hover td {
            background-color: #ccc;
        }

        .tb-bet-data .TYLETT_2a {
            background-image: linear-gradient(to right, #dc3545 0%, #41b8ba 100%);
            color: #fff;
            padding: 5px 10px;
            border: 1px solid #fff;
        }

        .tb-bet-data .TYLETT_3a {
            background-color: #e0ebff;
            border: 1px solid #fff;
            text-align: left;
            padding-left: 7px;
            padding-right: 7px;
        }

        .tb-bet-data .TYLETT_3b {
            background-color: #e0ebff;
            border: 1px solid #fff;
            text-align: left;
            padding-left: 7px;
            padding-right: 7px;
        }

        .tb-bet-data .TYLETT_3c {
            background-color: #e0ebff;
            border: 1px solid #fff;
            text-align: left;
            padding-left: 7px;
            padding-right: 7px;
            color: #4c4c4c;
        }

        .tb-bet-data .TYLETT_3d {
            background-color: #e0ebff;
            border: 1px solid #fff;
            text-align: left;
            padding-left: 7px;
            padding-right: 7px;
            color: #4c4c4c;
        }

        .tb-bet-data .TYLETT_3a,
        .tb-bet-data .TYLETT_3b,
        .tb-bet-data .TYLETT_3c,
        .tb-bet-data .TYLETT_3d {
            background-color: unset;
            vertical-align: top;
        }

        .tb-bet-data .diem_so .open_diem_so {
            margin-bottom: 0;
            background-color: #ccc;
            padding: 10px;
            text-align: left;
            font-weight: 600;
            position: relative;
        }

        .tb-bet-data .diem_so .open_diem_so::after {
            content: "";
            border-top: solid 6px #333;
            border-left: solid 6px transparent;
            border-right: solid 6px transparent;
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .tb-bet-data .diem_so ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            display: none;
            flex-wrap: wrap;
            align-items: center;
        }

        .tb-bet-data .diem_so ul li {
            border-bottom: 1px solid #fff;
        }

        .tb-bet-data .diem_so ul li p {
            margin-bottom: 0;
        }

        .tb-bet-data .diem_so ul li p:not(.ti_so) {
            padding: 5px 0;
            background: #ccc;
            color: #000;
            font-weight: 600;
        }

        .tb-bet-data .diem_so ul li .ti_so {
            background-color: #575757;
            color: #fff;
            font-weight: 600;
        }

        .tb-bet-data .diem_so ul li:not(:first-child) {
            border-left: 1px solid #fff;
        }

        .tb-bet-data .diem_so ul li:not(:last-child) {
            flex-grow: 1;
            flex-basis: 5%;
            width: 5%;
            max-width: 5%;
        }

        .tb-bet-data .diem_so ul li:last-child {
            flex-grow: 2;
            background-color: #ccc;
            height: 52px;
            line-height: 52px;
            font-size: 13px;
            font-weight: 600;
        }

        .tb-bet-data .diem_so ul.active {
            display: flex;
        }

        .table-header td {
            background-color: #dc3545;
            color: #fff;
            font-weight: 700;
            border: 1px solid #fff;
            padding: 5px 0;
        }

        .float-menu {
            padding: 0;
            margin: 0;
            list-style: none;
            position: fixed;
            top: 35%;
            right: 1%;
            z-index: 2;
            background: #fff;
            border-radius: 5px;
            box-shadow: 1px 3px 5px 1px rgba(0, 0, 0, 0.4);
        }

        .float-menu li {
            display: block;
            text-align: center;
            padding: 5px;
        }

        .float-menu li:hover {
            color: #41b8ba;
        }

        .float-menu li:hover a {
            color: #41b8ba;
        }

        .float-menu a {
            font-weight: 600;
            color: #000;
        }

        @media (min-width: 769px) {
            .tb-bet-data .dong2 {
                background: rgba(194, 32, 38, 0.14);
                border-bottom-color: #cea193;
                font-weight: 700;
                font-size: 16px;
            }

            .tb-bet-data .dong1 {
                background: rgba(194, 32, 38, 0.14);
                border-bottom-color: #cea193;
                font-weight: 700;
                font-size: 16px;
            }

            .tb-bet-data .dong4 {
                background-color: #e0ebff;
            }

            .tb-bet-data .dong3 {
                background-color: #e0ebff;
            }

            .tb-bet-data .chutyle {
                color: #ee6363;
            }

            .tb-bet-data .do {
                color: #810101;
            }
        }

        @media (max-width: 992px) {
            .tylekeo_container {
                font-size: 12px;
            }

            .tylekeo_container .open_diem_so {
                background-color: #e4e4e4;
                color: #333;
                text-align: left;
                font-weight: 700;
                margin: 0;
                padding: 10px;
                cursor: pointer;
                position: relative;
            }

            .tylekeo_container .open_diem_so span {
                font-size: 14px;
            }

            .tylekeo_container .open_diem_so::after {
                content: "";
                border-top: solid 6px #333;
                border-left: solid 6px transparent;
                border-right: solid 6px transparent;
                position: absolute;
                right: 10px;
                top: 9px;
            }

            .tylekeo_container .blink_me {
                animation: blinker 2s linear infinite;
            }

            .tylekeo_container .chutyle {
                color: #ee6363;
            }

            .tylekeo_container .TYLETT_3a {
                color: #464646;
                text-align: center;
                line-height: 15px;
                padding: 3px;
                vertical-align: top;
                border: 2px solid #fff;
            }

            .tylekeo_container .TYLETT_3a img {
                vertical-align: middle;
            }

            .tylekeo_container .TYLETT_3c {
                color: #464646;
                text-align: left;
                padding-left: 3px;
                padding-right: 3px;
                padding: 3px;
                vertical-align: top;
                border: 2px solid #fff;
            }

            .tylekeo_container .dong1 {
                background-color: #ecb0b091;
                font-weight: 700;
                font-size: 12px;
                border-bottom-color: #cea193;
            }

            .tylekeo_container .dong2 {
                background: rgba(194, 32, 38, 0.14);
                border-bottom-color: #cea193;
                font-weight: 700;
                font-size: 12px;
            }

            .tylekeo_container .dong3 {
                background-color: #e0ebfd;
                line-height: 18px;
            }

            .tylekeo_container .dong4 {
                background-color: #c1d6fb;
                line-height: 18px;
            }

            .tylekeo_container .dong:hover {
                background-color: #c7c1c1;
            }

            .tylekeo_container .do {
                color: #810101;
            }

            .tylekeo_container .diem_so ul {
                list-style: none;
                padding: 0;
                margin: 0;
                background-color: #e4e4e4;
                border-top: 1px solid #fff;
                display: none;
            }

            .tylekeo_container .diem_so ul::after {
                content: "";
                display: block;
                clear: both;
            }

            .tylekeo_container .diem_so li {
                float: left;
                border-right: 1px solid #fff;
                border-bottom: 1px solid #000;
            }

            .tylekeo_container .diem_so li p {
                font-weight: 700;
                font-size: 12px;
            }

            .tylekeo_container .diem_so li .ti_so {
                background-color: #0000009e;
                color: #fff;
                margin: 0;
                padding: 5px;
            }

            .tylekeo_container .diem_so li:last-child {
                width: 75%;
                text-align: center;
                padding: 25px 0;
            }

            .tylekeo_container .diem_so ul.open {
                display: block;
            }

            .tylekeo_container .diem_so ul.active {
                display: block;
            }

            .tylekeo_container .TYLETT_2a {
                color: #fff;
                text-align: left;
                padding-left: 10px;
                font-size: 12px;
            }

            .tylekeo_container .TYLETT_3 {
                background-color: #c6d4f1;
                line-height: 18px;
            }

            .tylekeo_container .TYLETT_4 {
                background-color: #436590;
                line-height: 22px;
            }

            .tylekeo_container .TYLETT_3_1 {
                background-color: #e4e4e4;
                line-height: 18px;
            }

            .tylekeo_container .TYLETT_3b {
                padding: 3px;
                vertical-align: top;
                border: 2px solid #fff;
            }

            .tylekeo_container .TYLETT_3d {
                padding: 3px;
                vertical-align: top;
                border: 2px solid #fff;
                color: #464646;
                text-align: right;
                padding-right: 3px;
            }

            .tylekeo_container .keo-dong td {
                line-height: 15px;
            }

            .tylekeo_container .hiep_1 {
                background-color: #eee8cd;
                font-family: Tahoma, Arial, Helvetica, sans-serif;
                font-weight: 700;
                font-size: 12px;
            }

            .tylekeo_container .hiep_1 img {
                width: 15px;
            }

            .tylekeo_container .hiep_1_btn {
                position: relative;
                cursor: pointer;
                color: #690;
            }

            .tylekeo_container .hiep_1_btn::after {
                content: "";
                border-top: solid 5px #690;
                border-left: solid 5px transparent;
                border-right: solid 5px transparent;
                position: absolute;
                right: -15px;
                top: 5px;
            }

            .tylekeo_container .open_diem_so.active::after {
                transform: rotate(180deg);
            }

            .tylekeo_container .filter_item {
                position: relative;
                padding: 5px 5px 5px 30px;
                display: block;
                cursor: pointer;
                color: #464646;
                margin-right: 20px;
            }

            .tylekeo_container .filter_item input {
                position: absolute;
                opacity: 0;
                cursor: pointer;
            }

            .tylekeo_container .filter_item input:checked~.checked_icon:after {
                display: block;
            }

            .tylekeo_container .filter_item:hover input~.checked_icon {
                background-color: #ccc;
            }

            .tylekeo_container .filter_item .checked_icon:after {
                left: 7px;
                top: 2px;
                width: 5px;
                height: 10px;
                border: solid #464646;
                border-width: 0 2px 2px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            .tylekeo_container .checked_icon {
                position: absolute;
                top: 2px;
                left: 0;
                height: 20px;
                width: 20px;
                background-color: #eee;
                border-radius: 3px;
                transition: background-color 0.2s;
                -webkit-transition: background-color 0.2s;
            }

            .tylekeo_container .checked_icon:after {
                content: "";
                position: absolute;
                display: none;
            }

            .tylekeo_container #open_filter {
                float: right;
                border: 1px solid #000;
                padding: 8px 30px 8px 15px;
                cursor: pointer;
                border-radius: 3px;
                position: relative;
                margin: 12px 5px;
            }

            .tylekeo_container #open_filter::after {
                content: "";
                border-top: solid 6px #464646;
                border-left: solid 6px transparent;
                border-right: solid 6px transparent;
                position: absolute;
                right: 10px;
                top: 12px;
            }

            .tylekeo_container #open_filter.active:after {
                transform: rotate(180deg);
            }

            .tylekeo_container #filter-select-all {
                padding: 5px 15px;
                border-radius: 5px;
                margin: 0 10px 10px 0;
            }

            .tylekeo_container #filter-clear {
                padding: 5px 15px;
                border-radius: 5px;
            }

            .tylekeo_container #filter li {
                display: inline-block;
            }

            .tylekeo_container .search_tyle {
                width: 50%;
                display: inline-block;
                padding: 5px;
                margin: 10px 5px 3px 0;
                border: 1px solid #464646;
                border-radius: 3px;
                position: relative;
            }

            .tylekeo_container .search_tyle input {
                border: none;
                -webkit-appearance: none;
                padding: 5px 25px 5px 5px;
                display: inline-block;
                vertical-align: middle;
                outline: none;
                width: 100%;
            }

            .tylekeo_container .search_tyle span.search-span {
                background-repeat: no-repeat;
                background-size: contain;
                font-size: 0;
                width: 25px;
                height: 25px;
                border: none;
                background-color: transparent;
                display: inline-block;
                vertical-align: middle;
                cursor: pointer;
                position: absolute;
                right: 5px;
                top: 5px;
            }

            .tylekeo_container .search_tyle span.clear-span {
                display: none;
                font-size: 15px;
                border: none;
                vertical-align: middle;
                cursor: pointer;
                position: absolute;
                right: 0;
                top: 4px;
                font-weight: 500;
                padding: 3px 10px;
            }

            .tylekeo_container #search-clear {
                padding: 7px 15px;
                background-color: #436590;
                color: #fff;
                border-radius: 5px;
                font-weight: 600;
            }

            .tylekeo_container .guild-live {
                font-weight: 500;
                margin: 0 0 5px;
            }

            .tylekeo_container #myBtn {
                display: none;
                position: fixed;
                bottom: 20px;
                right: 5px;
                z-index: 99;
                padding: 0;
                border: none;
                margin: 0;
                outline: none;
                cursor: pointer;
                border-radius: 50px;
            }

            .tylekeo_container #myBtn:hover {
                background-color: #eee;
            }

            .tylekeo_container .btn-keo-ngay {
                padding: 5px;
                margin: 5px 0;
                color: #fff;
                background-color: #0e64a0;
            }

            .tylekeo_container .btn-keo-ngay span {
                height: 5px;
                width: 5px;
                background-color: red;
                border-radius: 50%;
                display: none;
                vertical-align: middle;
                margin-right: 3px;
            }

            .tylekeo_container .desktop-only {
                display: inline-block;
            }

            .tylekeo_container .clear {
                clear: both;
                display: block;
            }

            .tylekeo_container .live-match {
                cursor: pointer;
                display: block;
                margin-top: 5px;
                position: relative;
                font-size: 14px;
                color: #f53030;
            }

            .tylekeo_container .live-match td {
                color: #000;
            }

            .tylekeo_container .live-match::after {
                content: "";
                border-top: solid 6px #464646;
                border-left: solid 6px transparent;
                border-right: solid 6px transparent;
                position: absolute;
                right: -15px;
                top: 5px;
            }

            .tylekeo_container .popup-list {
                position: absolute;
                right: 0;
                display: none;
                background-color: #fff;
                padding: 20px 15px;
                border-bottom-right-radius: 3px;
                border-bottom-left-radius: 3px;
                box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16);
                border-top: 1px solid #ccc;
                z-index: 9999;
            }

            .tylekeo_container #live-match-popup {
                position: absolute;
                top: 20px;
                left: 0;
                width: 300px;
                padding: 0;
                cursor: initial;
                color: #000;
            }

            .tylekeo_container .player-wraper {
                position: relative;
                height: 590px;
                width: 1000px;
                margin: 0 auto;
            }

            .float-menu {
                top: unset;
                bottom: 0;
                right: 0;
                left: 0;
                text-align: center;
            }

            .float-menu li {
                display: inline-block;
                padding: 5px 22px;
            }
        }
    </style>
    <main class="main-detail" data-url="<?php echo getUrlPost($oneItem) ?>">
        <div class="container">
            <div class="row">
                <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
                <article class="col-12">
                    <div class="bg-white p-3">
                        <h1 class="font-23"><?php echo $oneItem->title ?></h1>
                        <div class="d-flex flex-wrap justify-content-between my-3">
                            <div class="font-13 text-secondary"><?php echo date_post_vn($oneItem->updated_time) ?> - <?php echo timeAgo($oneItem->updated_time, "H:i") ?></div>

                            <!--	                --><?php //$this->load->view(TEMPLATE_PATH . "block/rate") 
                                                        ?>
                        </div>
                        <div class="font-14 font-weight-bold">
                            <?php echo $oneItem->description ?>
                        </div>
                        <div class="content-news">
                            <div class="odd-pro tylekeo_container">
                                <table class="table table-bordered table-header">
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" width="9%" class="text-center td-1a"><span>Giờ</span></td>
                                            <td rowspan="2" width="27%" class="text-center td-2a"><span>Trận đấu</span></td>
                                            <td colspan="3" width="32%" class="text-center td-1b"><span>Cả trận</span></td>
                                            <td colspan="3" width="32%" class="text-center td-2b"><span>Hiệp 1</span></td>
                                        </tr>
                                        <tr>
                                            <td width="12%" class="text-center td-1c"><span>Tỷ lệ</span></td>
                                            <td width="12%" class="text-center td-2c"><span>Tài xỉu</span></td>
                                            <td width="8%" class="text-center td-3c"><span>1x2</span></td>
                                            <td width="12%" class="text-center td-1c"><span>Tỷ lệ</span></td>
                                            <td width="12%" class="text-center td-2c"><span>Tài xỉu</span></td>
                                            <td width="8%" class="text-center td-3c"><span>1x2</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="tylekeo" class="tylekeo overflow-auto">
                                </div>
                            </div>
                            <div class="content-news text-justify">
                                <?php echo $oneItem->content ?>
                            </div>
                        </div>
                        <div class=""></div>
                    </div>
                </article>
            </div>
        </div>
    </main>
    <script>
        document.addEventListener("DOMContentLoaded", function(event) {
            const dataTlk = `<?= !empty($data_ty_le_keo) ? $data_ty_le_keo : ''  ?>`;
            document.getElementById('tylekeo').innerHTML = dataTlk;
        });
    </script>
<?php endif; ?>