<?php if(!empty($oneItem)):
    $data_info_stadium = !empty($data_info->stadium) ? $data_info->stadium : null;
    $data_info_referee = !empty($data_info->referee) ? $data_info->referee : null;
    $data_info_manager = !empty($data_info->manager) ? $data_info->manager : null;
    $data_info_season = !empty($data_info->season) ? $data_info->season : null;
    $data_info_jerseys = !empty($data_info->jerseys) ? $data_info->jerseys : null;
    ?>
    <main class="container main-detail"  data-url="<?php echo getUrlPost($oneItem) ?>">
        <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
            <div class="col-12 py-3 row no-gutters text-center">
                <div class="col-6 col-lg-3 order-md-1">
                    <?php echo getLogoClub($oneItem->home_id,$oneItem->name_home,"",80,80) ?>
                    <a href="<?php echo getUrlMatch($oneItem) ?>" title="<?php echo $oneItem->name_home ?>" class="text-danger font-16 font-weight-bold d-block"><?php echo $oneItem->name_home ?></a>
                    <p class="d-none d-md-block"><?php echo !empty($data_info_manager) ? "HLV trưởng: {$data_info_manager->home->name}" : '' ?></p>
                </div>

                <div class="col-6 col-lg-3 order-md-3">
                    <?php echo getLogoClub($oneItem->away_id,$oneItem->name_away,"",80,80) ?>
                    <a href="<?php echo getUrlMatch($oneItem) ?>" title="<?php echo $oneItem->name_away ?>" class="text-danger font-16 font-weight-bold d-block"><?php echo $oneItem->name_away ?></a>
                    <p class="d-none d-md-block"><?php echo !empty($data_info_manager) ? "HLV trưởng: {$data_info_manager->away->name}" : '' ?></p>
                </div>
                <div class="col-12 col-lg-6 order-md-2">
                    <?php if($oneItem->match_status === "live"): ?>
                        <div class="text-black2 font-16 font-weight-bold position-relative"><span class="live-match"><?php echo !empty($oneItem->elapsed) ? $oneItem->elapsed . "'" : $oneItem->status ?></span></div>
                        <div class="text-black2 font-30 font-weight-bold text-uppercase font-italic"><?php echo $oneItem->score_home . " - " . $oneItem->score_away ?></div>
                    <?php else: ?>
                        <div class="text-black2 font-16 font-weight-bold"><?php echo timeAgo($oneItem->start_time,"H:i") ?></div>
                        <?php if(time() >= $oneItem->start_timestamp): ?>
                            <div class="text-black2 font-weight-bold font-italic"><strong class="font-30 text-danger"><?php echo $oneItem->status ?></strong></div>
                        <?php else: ?>
                            <div class="text-black2 font-weight-bold font-italic"><?php echo (time() + 60 * 60 > $oneItem->start_timestamp ? '<strong class="font-30 text-danger">Sắp đá</strong>' : "<span class='text-uppercase font-30'>VS</span>") ?></div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <div>
                        <?php echo !empty($data_info_stadium) ? "Sân vận động: {$data_info_stadium->name}, {$data_info_stadium->city}, {$data_info_stadium->country}" : '' ?><br>
                        <?php echo !empty($data_info_referee) ? "Trọng tài: {$data_info_referee->name}" : '' ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <section class="col-12 col-lg-9">
                        <div class="py-2 d-flex align-items-center">
                            <div class="fb-share-button"
                                 data-href="<?php echo !empty($SEO['url']) ? $SEO['url'] : ''?>"
                                 data-layout="button_count">
                            </div>
                            <span class="font-14 text-left btn">Share để xem không giật lag!!!</span>
                        </div>
                        <?php
                        if(isset($_GET['server'])) $urlIframe = getUrlPlayer($oneItem,$this->input->get('server'));
                        else $urlIframe = getUrlPlayerOther($oneItem,$this->input->get('server_other'));
                        ?>
                        <?php if (!empty($data_link) || !empty($data_link_wb)): ?>
                            <div class="embed-responsive embed-responsive-16by9 bg-secondary video-play">
                                <iframe id="jwplayer" width="100%" height="500"
                                        src="<?php echo $urlIframe ?>"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        <?php else: ?>
                            <div class="embed-responsive bg-secondary video-play">
                                <?php echo getThumbnailStatic(MEDIA_URL . "before-live.jpg", "", '', "trực tiếp bóng đá natra.tv", "img-fluid w-100") ?>
                            </div>
                        <?php endif; ?>
                    </section>
                    <section class="col-12 col-lg-9 order-lg-1 my-3">
                        <div class="box_server">
                            <ul class="list-inline mb-0">
                                <?php if(!empty($data_link)) foreach ($data_link as $index => $item): ?>
                                    <li class="list-inline-item mb-2">
                                        <a href="<?php echo getUrlMatchNoLink($oneItem)."?server=".$index ?>" title="Link <?php echo $index+1 ?>" class="btn btn-outline-danger <?php echo (isset($_GET['server']) && $this->input->get('server') == $index) ? 'active' : '' ?>">Link <?php echo $index+1 ?></a>
                                    </li>
                                <?php endforeach; ?>
                                <?php if (!empty($data_link_wb)) foreach ($data_link_wb as $index => $item): ?>
                                    <li class="list-inline-item mb-2">
                                        <a href="<?php echo getUrlMatchNoLink($oneItem) . "?server_other=" . $index ?>"
                                           title="Link <?php echo count($data_link) + $index + 1 ?>"
                                           class="btn btn-success mr-2 <?php echo (isset($_GET['server_other']) && $this->input->get('server_other') == $index) ? 'active' : '' ?>">Link <?php echo count($data_link) + $index + 1 ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </section>

                    <aside class="col-12 col-lg-3 pl-lg-0 d-flex flex-column">
                        <div class="bg-secondary d-flex justify-content-around py-2">
                            <a href="https://natra.tv/soi-keo" rel="nofollow" title="" class="btn btn-primary">Soi kèo</a>
                            <a href="https://k8vip011.cc/" rel="nofollow" title="" class="btn btn-warning">Đặt cược 1</a>
                            <a href="https://www.1126bet.com/?uagt=natra" rel="nofollow" title="" class="btn btn-danger">Đặt cược 2</a>
                        </div>
                        <iframe src="https://www6.cbox.ws/box/?boxid=853507&boxtag=yvw8tw" width="100%" height="100%" allowtransparency="yes" allow="autoplay" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto"></iframe>
                    </aside>
                </div>
            </div>
            <?php if(!empty($data_stats_form)): ?>
                <section class="col-12 my-3 my-lg-0">
                    <h3 class="title-bg-danger mb-3">Các trận đấu gần nhất của 2 đội</h3>
                    <div class="overflow-auto">
                        <table class="table table-striped">
                            <tbody>

                            <tr>
                                <td class="border-bottom-success text-uppercase text-nowrap">
                                    <?php if(!empty($data_stats_form->teams->home->form)) foreach ($data_stats_form->teams->home->form as $item): ?>
                                        <?php if($item->type === "W"): ?>
                                            <span class="rounded-circle text-white py-1 px-2 bg-success" title="Thắng">t</span>
                                        <?php endif; ?>
                                        <?php if($item->type === "D"): ?>
                                            <span class="rounded-circle text-white py-1 px-2 bg-warning" title="Hòa">h</span>
                                        <?php endif; ?>
                                        <?php if($item->type === "L"): ?>
                                            <span class="rounded-circle text-white py-1 px-2 bg-danger" title="Bại">b</span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                                <td class="border-bottom-success text-uppercase text-nowrap text-right">
                                    <?php if(!empty($data_stats_form->teams->away->form)) foreach ($data_stats_form->teams->away->form as $item): ?>
                                        <?php if($item->type === "W"): ?>
                                            <span class="rounded-circle text-white py-1 px-2 bg-success" title="Thắng">t</span>
                                        <?php endif; ?>
                                        <?php if($item->type === "D"): ?>
                                            <span class="rounded-circle text-white py-1 px-2 bg-warning" title="Hòa">h</span>
                                        <?php endif; ?>
                                        <?php if($item->type === "L"): ?>
                                            <span class="rounded-circle text-white py-1 px-2 bg-danger" title="Bại">b</span>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </section>
            <?php endif; ?>

            <?php if (!empty($data_lineup)): ?>
                <section class="col-12 my-3 my-lg-0">
                    <div class="title-bg-danger">Đội hình thi đấu</div>
                    <div class="overflow-auto border-bottom">
                        <table class="table mb-0 border-bottom">
                            <thead>
                            <tr>
                                <th class="text-center" width="40%"><?php echo getLogoClub($oneItem->home_id, $oneItem->name_home, "", 80, 80) ?></th>
                                <th class="text-center" width="10%" class="font-weight-bolder"></th>
                                <th class="text-center" width="40%"><?php echo getLogoClub($oneItem->away_id, $oneItem->name_away, "", 80, 80) ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center font-26">
                                    <?php echo !empty($data_lineup->home->startinglineup->formation) ? $data_lineup->home->startinglineup->formation : '' ?>
                                </td>
                                <td class="text-center">Sơ đồ thi đấu</td>
                                <td class="text-center  font-26">
                                    <?php echo !empty($data_lineup->away->startinglineup->formation) ? $data_lineup->away->startinglineup->formation : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <?php if (!empty($data_lineup->home->startinglineup->players)) foreach ($data_lineup->home->startinglineup->players as $item): ?>
                                        <p><?php echo $item->name ?> <strong><?php echo $item->playername ?></strong> (<?php echo $item->shirtnumber ?>)</p>
                                    <?php endforeach; ?>
                                </td>
                                <td>Cầu thủ đá chính</td>
                                <td class="text-right">
                                    <?php if (!empty($data_lineup->away->startinglineup->players)) foreach ($data_lineup->away->startinglineup->players as $item): ?>
                                        <p><?php echo $item->name ?> <strong><?php echo $item->playername ?></strong> (<?php echo $item->shirtnumber ?>)</p>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-left">
                                    <?php if (!empty($data_lineup->home->substitutes)) foreach ($data_lineup->home->substitutes as $item): ?>
                                        <p><strong><?php echo $item->playername ?></strong> (<?php echo $item->shirtnumber ?>)</p>
                                    <?php endforeach; ?>
                                </td>
                                <td>Cầu thủ dự bị</td>
                                <td class="text-right">
                                    <?php if (!empty($data_lineup->away->substitutes)) foreach ($data_lineup->away->substitutes as $item): ?>
                                        <p><strong><?php echo $item->playername ?></strong> (<?php echo $item->shirtnumber ?>)</p>
                                    <?php endforeach; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            <?php endif; ?>
            <!--<div style="width: 100%;height: 880px;overflow:hidden;position:relative;">
                        <iframe frameborder="0" style="width: 100%;height: 1090px;max-height: 5000px;min-height: 490px;top: -67.6px; position: absolute;" src="https://cs.betradar.com/ls/widgets/?/cmd368lspc/vi/Europe:Berlin/page/sportcenter/soccer#matchId=<?php /*echo $oneItem->match_id */?>"></iframe>
                    </div>-->

            <?php if(!empty($data_event)): ?>
                <section class="col-12 my-3 my-lg-0">
                    <div class="title-bg-danger">DIỄN BIẾN TRẬN ĐẤU</div>
                    <div class="overflow-auto border-bottom">
                        <table class="table mb-0 border-bottom">
                            <?php if(!empty($data_event->events)) foreach ($data_event->events as $item):
                                if(in_array($item->type,['card','goal'])):
                                    ?>
                                    <tr>
                                        <td class="text-right">
                                            <?php if($item->team === "home") echo getTypeEventMatchHome($item); ?>
                                        </td>
                                        <td class="text-center"><?php echo $item->time ?>'</td>
                                        <td>
                                            <?php if($item->team === "away") echo getTypeEventMatchAway($item); ?>
                                        </td>
                                    </tr>
                                <?php endif; endforeach; ?>
                        </table>
                    </div>
                </section>
            <?php endif; ?>

            <?php if(!empty($data_recently)): ?>
                <section class="col-12 col-lg-6 my-3 my-lg-0">
                    <div class="title-bg-danger">THÀNH TÍCH ĐỐI ĐẦU (6 TRẬN GẦN NHẤT)</div>
                    <div class="overflow-auto">
                        <table class="table">
                            <tr>
                                <td class="font-weight-bold">07/03/2020</td>
                                <td class="text-right">
                                    Great Olympics
                                    <img loading="lazy" src="https://mitom.net/wp-content/uploads/2020/12/16577.png" alt="" class="logo-tournament">
                                </td>
                                <td class="text-center">
                                    <span class="btn btn-primary">2-0</span>
                                </td>
                                <td>
                                    <img loading="lazy" src="https://mitom.net/wp-content/uploads/2020/12/10056.png" alt="" class="logo-tournament">
                                    Ebusua Dwarfs
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">07/03/2020</td>
                                <td class="text-right">
                                    Great Olympics
                                    <img loading="lazy" src="https://mitom.net/wp-content/uploads/2020/12/16577.png" alt="" class="logo-tournament">
                                </td>
                                <td class="text-center">
                                    <span class="btn btn-primary">2-0</span>
                                </td>
                                <td>
                                    <img loading="lazy" src="https://mitom.net/wp-content/uploads/2020/12/10056.png" alt="" class="logo-tournament">
                                    Ebusua Dwarfs
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">07/03/2020</td>
                                <td class="text-right">
                                    Great Olympics
                                    <img loading="lazy" src="https://mitom.net/wp-content/uploads/2020/12/16577.png" alt="" class="logo-tournament">
                                </td>
                                <td class="text-center">
                                    <span class="btn btn-primary">2-0</span>
                                </td>
                                <td>
                                    <img loading="lazy" src="https://mitom.net/wp-content/uploads/2020/12/10056.png" alt="" class="logo-tournament">
                                    Ebusua Dwarfs
                                </td>
                            </tr>
                        </table>
                    </div>
                </section>
            <?php endif; ?>

            <?php if(!empty($data_statistic)): ?>
                <section class="col-12 my-3 my-lg-0">
                    <div class="title-bg-danger">THỐNG KÊ TRONG TRẬN ĐẤU</div>
                    <div class="overflow-auto border-bottom">
                        <table class="table border-bottom text-center mb-0">
                            <thead>
                            <tr>
                                <th><?php echo getLogoClub($oneItem->home_id,$oneItem->name_home,"",80,80) ?></th>
                                <th class="font-weight-bolder">Số liệu thống kê về trận đấu</th>
                                <th><?php echo getLogoClub($oneItem->away_id,$oneItem->name_away,"",80,80) ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($data_statistic->index)) foreach ($data_statistic->index as $type):
                                $values = $data_statistic->values->{$type};
                                ?>
                                <tr>
                                    <td><?php echo !empty($values->value->home) ? $values->value->home : 0 ?></td>
                                    <td><?php echo $values->name ?></td>
                                    <td><?php echo !empty($values->value->away) ? $values->value->away : 0 ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </section>
            <?php endif; ?>

            <section class="col-12 order-lg-1 my-3 my-lg-0">
                <h3 class="title-bg-danger mb-3">Bảng xếp hạng giải <?php echo $oneItem->league_name ?></h3>
                <?php
                if (!empty($data_ranks) && count($data_ranks) > 1):
                    foreach ($data_ranks as $key) :
                        $tablerows = $key->tablerows;
                        ?>
                        <div class="ajax-content">
                            <div class="bg-main py-2 px-3 text-center text-white text-capitalize"><?php echo $key->name ?></div>
                            <div class="overflow-auto">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="border-bottom-success">#</th>
                                        <th class="border-bottom-success">Đội</th>
                                        <th class="border-bottom-success">ST</th>
                                        <th class="border-bottom-success">Thắng</th>
                                        <th class="border-bottom-success">Hòa</th>
                                        <th class="border-bottom-success">Bại</th>
                                        <th class="border-bottom-success">HS</th>
                                        <th class="border-bottom-success">Điểm</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tablerows as $index => $item) : ?>
                                        <tr class="<?php echo ($item->team->uid == $oneItem->home_id || $item->team->uid == $oneItem->away_id) ? "font-weight-bolder text-danger" : "" ?>">
                                            <td class="border-bottom-success"><?php echo $index + 1 ?></td>
                                            <td class="border-bottom-success"><?php echo $item->team->mediumname ?></td>
                                            <td class="border-bottom-success"><?php echo $item->total ?></td>
                                            <td class="border-bottom-success"><?php echo $item->winTotal ?></td>
                                            <td class="border-bottom-success"><?php echo $item->drawTotal ?></td>
                                            <td class="border-bottom-success"><?php echo $item->lossTotal ?></td>
                                            <td class="border-bottom-success"><?php echo $item->goalDiffTotal ?></td>
                                            <td class="border-bottom-success text-green"><?php echo $item->pointsTotal ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <p class="mt-5">T: Thắng | H: Hòa | B: Bại | HS: Hiệu số | Phong độ: từ trái qua phải là trận gần đây nhất =>
                        trận xa nhất</p>
                <?php else :
                    $data_ranks = $data_ranks[0]->tablerows;
                    ?>
                    <div class="ajax-content">
                        <div class="overflow-auto">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="border-bottom-success">#</th>
                                    <th class="border-bottom-success">Đội</th>
                                    <th class="border-bottom-success">ST</th>
                                    <th class="border-bottom-success">Thắng</th>
                                    <th class="border-bottom-success">Hòa</th>
                                    <th class="border-bottom-success">Bại</th>
                                    <th class="border-bottom-success">HS</th>
                                    <th class="border-bottom-success">Điểm</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($data_ranks as $index => $item) : ?>
                                    <tr class="<?php echo ($item->team->uid == $oneItem->home_id || $item->team->uid == $oneItem->away_id) ? "font-weight-bolder text-danger" : "" ?>">
                                        <td class="border-bottom-success"><?php echo $index + 1 ?></td>
                                        <td class="border-bottom-success"><?php echo $item->team->mediumname ?></td>
                                        <td class="border-bottom-success"><?php echo $item->total ?></td>
                                        <td class="border-bottom-success"><?php echo $item->winTotal ?></td>
                                        <td class="border-bottom-success"><?php echo $item->drawTotal ?></td>
                                        <td class="border-bottom-success"><?php echo $item->lossTotal ?></td>
                                        <td class="border-bottom-success"><?php echo $item->goalDiffTotal ?></td>
                                        <td class="border-bottom-success text-green"><?php echo $item->pointsTotal ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
                <p class="mt-5">T: Thắng | H: Hòa | B: Bại | HS: Hiệu số | Phong độ: từ trái qua phải là trận gần đây nhất =>
                    trận xa nhất</p>
            </section>
        </div>
    </main>
<?php endif; ?>