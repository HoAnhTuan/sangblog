<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;
class Crawler_data extends Crawler_Controller
{
    protected $_client;
    protected $_match;
    protected $_post;
    protected $_tournament;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['match_model','post_model','tournament_model']);
        $this->_match = new Match_model();
        $this->_post = new Post_model();
        $this->_tournament = new Tournament_model();
        $this->_client = new Client(HttpClient::create(['timeout' => 60]));
    }
    public function hello(){
        try {
            $client = HttpClient::create();
            $response = $client->request('GET', 'https://vietnamnet.vn/jsx/loadmore/?domain=desktop&c=the-thao-hau-truong&p=1&s=15&a=5');
            $contentType = $response->getContent();
            $contentType = str_replace("retvar =", "", $contentType);
            $contentType = json_decode($contentType, true);
            dd($contentType);
//
//
//
//
//            $url = "https://vietnamnet.vn/jsx/loadmore/?domain=desktop&c=the-thao-hau-truong&p=1&s=15&a=5";
//            $crawler = $this->_client->request('GET', $url);
////            $content = $crawler->filter('body')->html();
////            $content = $crawler->getResponse()->getContent();
//            $content = json_decode($crawler->getResponse()->getContent());
////            $content = $crawler->getHeaders()['content-type'][0];
//            dd($content);
////            $content = $crawlerDetail->filter('#divcontentwrap')->html();

        }
        catch (Exception $e){
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
    public function test(){
        dd($this->callCURL("http://api.sblradar.net/api/v2/page/getPage?id=2"));
    }

    private function getCateKeonhcai($url, $category_id){
        try {
            $crawler = $this->_client->request('GET', $url);
            $listDetail = $crawler->filter('.main_content > .tintuc > h3 > a')->each(function ($node) {
                return "http://keonhacai1.net/".$node->attr("href");
            });
            $listDetail = array_reverse($listDetail);
            if(!empty($listDetail)) foreach ($listDetail as $linkDetail){
                $crawlerDetail = $this->_client->request('GET', $linkDetail);
                $data['title'] = $data['meta_title'] = $crawlerDetail->filter("h1")->text();
                $data['slug'] = $this->toSlug($data['title']);
                if(!$this->_post->checkExistByField("slug",$data['slug'])) {
                    $data['description'] = $data['meta_description'] = $crawlerDetail->filter('meta[property="og:description"]')->attr("content");
                    $content = $crawlerDetail->filter('.news-detail-content-html')->html();
                    $imgs = $crawlerDetail->filter('.news-detail-content-html img')->each(function ($node){
                        if(preg_match('/http/i', $node->attr('src'))) $imgSrc = $node->attr('src');
                        else $imgSrc = "http://keonhacai1.net/".$node->attr('src');
                        $imgSrc = strtok($imgSrc,'?');
                        $srcNew = $this->downloadImage($imgSrc,'','soi-keo');
                        return [
                          'old' => $node->attr('src'),
                          'new' => MEDIA_NAME.$srcNew
                        ];
                    });
                    if(!empty($imgs)) foreach ($imgs as $img){
                        $content = str_replace($img['old'],$img['new'],$content);
                    }
                    $content = preg_replace("/\<h1(.*)\>(.*)\<\/h1\>/", "", $content);
                    $data['content'] = preg_replace("/\<a(.*)\>(.*)\<\/a\>/", "", $content);

                    if ($crawlerDetail->filter('meta[itemprop="image"]')->count() > 0)
                        $data['thumbnail'] = $this->downloadImage($crawlerDetail->filter('meta[itemprop="image"]')->attr('content'),'','soi-keo');
                    else
                        $data['thumbnail'] = $this->downloadImage($crawlerDetail->filter('.news-detail-content-html img', 0)->attr('src'),'','soi-keo');

                    $data['type'] = "soikeo";
                    $data['user_id'] = 1;
                    if ($id = $this->_post->save($data)) {
                        $data_category = [
                          "post_id" => $id,
                          'category_id' => $category_id,
                          'is_primary' => false
                        ];
                        if ($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)) {
                            echo "Update category bài viết $id thành công ! \n";
                        }
                        echo "Thêm bài viết $id thành công ! \n";
                    }
                }else {
                    $data['description'] = $data['meta_description'] = $crawlerDetail->filter('meta[property="og:description"]')->attr("content");
                    $content = $crawlerDetail->filter('.news-detail-content-html')->html();
                    $imgs = $crawlerDetail->filter('.news-detail-content-html img')->each(function ($node){
                        if(preg_match('/http/i', $node->attr('src'))) $imgSrc = $node->attr('src');
                        else $imgSrc = "http://keonhacai1.net/".$node->attr('src');
                        $imgSrc = strtok($imgSrc,'?');
                        $srcNew = $this->downloadImage($imgSrc,'','soi-keo');
                        return [
                          'old' => $node->attr('src'),
                          'new' => MEDIA_NAME.$srcNew
                        ];
                    });
                    if(!empty($imgs)) foreach ($imgs as $img){
                        $content = str_replace($img['old'],$img['new'],$content);
                    }
                    $content = preg_replace("/\<h1(.*)\>(.*)\<\/h1\>/", "", $content);
                    $data['content'] = preg_replace("/\<a(.*)\>(.*)\<\/a\>/", "", $content);

                    if ($crawlerDetail->filter('meta[itemprop="image"]')->count() > 0)
                        $data['thumbnail'] = $this->downloadImage($crawlerDetail->filter('meta[itemprop="image"]')->attr('content'),'','soi-keo');
                    else
                        $data['thumbnail'] = $this->downloadImage($crawlerDetail->filter('.news-detail-content-html img', 0)->attr('src'),'','soi-keo');

                    $data['type'] = "soikeo";
                    $data['user_id'] = 1;
                    if ($this->_post->update(["slug" => $data['slug']],$data)) {
                        echo "Update bài viết {$data['slug']} thành công ! \n";
                    }
                    echo "Bài đã tồn tại ! \n";
                }
            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
    public function crawler_tournament(){
        try {
            $url = API_DATACENTER . '/api/v1/tournament/get_all';
            $data_json = $this->callCURL($url);
            $data = json_decode($data_json);
            $data= $data->data->data;
            if (!empty($data)) {
                foreach ($data as $key => $item) {
                    if(!$this->_tournament->checkExistByField('tournament_id',$item->tournament_id)){
                        $tmp['title'] = $item->title;
                        $tmp['country'] = $item->country;
                        $tmp['tournament_name'] = $item->tournament_name;
                        $tmp['name'] = $item->name;
                        $tmp['season_id'] = $item->season_id;
                        $tmp['tournament_id'] = $item->tournament_id;
                        $tmp['year'] = $item->year;
                        $tmp['seasontype'] = !empty($item->seasontype) ? $item->seasontype : '';
                        $tmp['data_ranks'] = !empty($item->data_ranks) ? $item->data_ranks : '';
                        $tmp['livetable'] = !empty($item->livetable) ? $item->livetable : '';
                        if($idResult = $this->_tournament->save($tmp))
                            echo "Insert tournament id success: $idResult \n";
                    }else{
                        $tmp['title'] = $item->title;
                        $tmp['country'] = $item->country;
                        $tmp['tournament_name'] = $item->tournament_name;
                        $tmp['name'] = $item->name;
                        $tmp['season_id'] = $item->season_id;
                        $tmp['tournament_id'] = $item->tournament_id;
                        $tmp['year'] = $item->year;
                        $tmp['seasontype'] = !empty($item->seasontype) ? $item->seasontype : '';
                        $tmp['data_ranks'] = !empty($item->data_ranks) ? $item->data_ranks : '';
                        $tmp['livetable'] = !empty($item->livetable) ? $item->livetable : '';
                        if($this->_tournament->update(['tournament_id' => $item->tournament_id],$tmp))
                            echo "Update tournament success ! \n";
                    }
                }
            } else {
                echo "Không có data \n";
            }
            die();
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
    public function crawler_match(){
        try {
            for ( $i = 0; $i <= 1; $i++ ) {
                $url = API_DATACENTER . 'api/v1/match/get_schedule?date=' . date('Y-m-d', strtotime("+$i days") );
                $data_json = $this->callCURL($url);
                $data = json_decode($data_json);
                $data= $data->data->data;
                if (!empty($data)) {
                    foreach ($data as $key => $item) {
                        $params = [
                            'match_id' => $item->match_id,
                            'country_id' => $item->country_id,
                            'tournament_id' => $item->tournament_id,
                            'season_id' => $item->season_id,
                            'round' => $item->round,
                            'title' => $item->name_home . ' vs ' .$item->name_away . ' ngày ' .date('d/m/Y', $item->start_timestamp) . ', ' .date('H:i:s', $item->start_timestamp),
                            'home_id' => $item->home_id,
                            'away_id' => $item->away_id,
                            'name_home' => $item->name_home,
                            'name_away' => $item->name_away,
                            'score_home' => $item->score_home,
                            'score_away' => $item->score_away,
                            'status' => $item->status,
                            'match_status' => $item->match_status,
                            'start_timestamp' => $item->start_timestamp,
                            'start_time' => $item->start_time,
                            'league_name' => $item->league_name,
                        ];
                        $exist_match = $this->_match->checkExistByField('match_id', $item->match_id, $this->_match->table);
                        if ($exist_match) {
                            echo "Update match $item->match_id \n";
                            $this->_match->update([
                                'match_id' => $item->match_id
                            ], $params, $this->_match->table);

                        } else {
                            echo "Insert match $item->match_id \n";
                            $this->_match->insert($params, $this->_match->table);
                        }

                    }
                } else {
                    echo "Không có data \n";
                }
            }
            die();
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
    public function update_data_bet (){
        $post_data = $this->_post->getData(['is_match_id' => true, 'limit' => 30, 'type' => 'soikeo','order' => ['displayed_time' => 'DESC']]);
        $listMatchId = [];
        if(!empty($post_data)) foreach ($post_data as $item){
            if(!empty($item->match_id)) $listMatchId[] = $item->match_id;
        }
        if(!empty($listMatchId)){
            try {
                $data_json = $this->callCURL(API_DATACENTER . 'api/v1/match/get_schedule_odds?in_match_id='.implode(',',$listMatchId));
                $data = json_decode($data_json);
                $data= $data->data->data;
                if(!empty($data)) foreach ($data as $index => $item) {
                    $params = [
                        'data_bets' => $item->data_top,
                        'start_time' => $item->start_time,
                        'data_match' => json_encode([
                            'match_id' => $item->match_id,
                            'name_home' => $item->name_home,
                            'name_away' => $item->name_away,
                            'home_id' => $item->home_id,
                            'away_id' => $item->away_id,
                            'league_name' => $item->league_name
                        ])
                    ];
                    echo "Update trận đấu có match id là $item->match_id \n";
                    if($this->_post->update(['match_id' => $item->match_id],$params, $this->_post->table))
                        echo "Update thành công ! \n";
                }
                die("Done !");
            } catch (Exception $e) {
                echo $e->getCode() . '\\n';
                echo $e->getMessage() . '\\n';
            }
        }

    }
    public function getKeonhacai1(){
        $url1 = 'http://keonhacai1.net/soi-keo-bong-da.htm';
        $url2 = 'http://keonhacai1.net/nhan-dinh-bong-da.htm';
        $urlVideo = 'http://keonhacai1.com/video-bong-da.htm';
        $this->getCateKeonhcai($url1, 1);
        $this->getCateKeonhcai($url2, 2);
        $this->getVideoKeonhcai($urlVideo, 17);
    }
    private function getVideoKeonhcai($url, $category_id){
        try {
            $crawler = $this->_client->request('GET', $url);
            $listDetail = $crawler->filter('.main_content > .tintuc a > img.aimg')->each(function ($node) {
                return [
                    "thumbnail" => $node->attr("src"),
                    "link" => "http://keonhacai1.net/".$node->parents()->attr("href")
                ];
            });
            $listDetail = array_reverse($listDetail);
            if(!empty($listDetail)) foreach ($listDetail as $linkDetail){
                $crawlerDetail = $this->_client->request('GET', $linkDetail["link"]);
                $data['title'] = $data['meta_title'] = $crawlerDetail->filter("h1")->text();
                $data['slug'] = $this->toSlug($data['title']);
                if(!$this->_post->checkExistByField("slug",$data['slug'])){
                    $data['description'] = $data['meta_description'] = $crawlerDetail->filter('meta[property="og:description"]')->attr("content");
//                $data['meta_keyword'] = $crawlerDetail->filter('meta[name="keywords"]')->attr("content");

                    $data['thumbnail'] = $linkDetail["thumbnail"];

                    $re = '/(http.*?\.m3u8[^&">]+)/';
                    preg_match_all($re, $crawlerDetail->html(), $matchLink, PREG_SET_ORDER, 0);
                    if(!empty($matchLink[0][0])){
                        $linkVideo = explode("?",$matchLink[0][0]);
                        $linkVideo = $linkVideo[0];
                        $data['video'] = $linkVideo;

                        $data['type'] = "video";
                        $data['user_id'] = 1;
                        if($id = $this->_post->save($data)){
                            $data_category = [
                                "post_id" => $id,
                                'category_id' => $category_id,
                                'is_primary' => false
                            ];
                            if($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)){
                                echo "Update category bài viết $id thành công ! \n";
                            }
                            echo "Thêm bài viết $id thành công ! \n";
                        }
                    }
                }else echo "Bài đã tồn tại ! \n";

            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
}