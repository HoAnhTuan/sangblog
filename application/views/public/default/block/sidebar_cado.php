<?php
$category = getByIdPage(28);
$meocuoc = getDataPost([
  'category_id' => 28,
  'is_status' => 1,
  'limit' => 5,
  'order' => ['displayed_time' => 'DESC']
]);
?>
<h3 class="title-separate mb-3">Cẩm nang cá độ</h3>
<div class="block-tips">
		<a href="<?php echo getUrlCategory($category) ?>" title="<?= $category->title ?>">
	      <?php echo getThumbnail($category, 280, 200, "w-100 img-fluid") ?>
		</a>
    <h5 class="text-danger mt-2">
	    <a href="<?php echo getUrlCategory($category) ?>"><?php echo $category->title ?? ''?></a>
    </h5>
    <p class="main-description">
        <?php echo $category->description ?? '';?>
    </p>
    <ul class="list-tips">
	    <?php foreach($meocuoc as $item) :?>
        <li class="tip-item">
	        <a href="<?php echo getUrlPost($item) ?>"><?php echo $item->title ?? ''?></a>
        </li>
	    <?php endforeach;?>

    </ul>
</div>