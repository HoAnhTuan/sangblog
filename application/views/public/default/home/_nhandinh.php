<?php if (!empty($nhandinh_post)) : ?>
	<section class="section">
		<div class="row">
			<div class="col-12">
				<a href="<?= base_url() . 'nhan-dinh-bong-da.html' ?>" class="category-title" title="nhận định bóng đá">
					<h2 class="category-name category-name-sub">
						<span>
							Nhận định bóng đá
						</span>
					</h2>
				</a>
			</div>
		</div>
		<section class="section-nhandinh">
			<div class="swiper nhandinhSwiper overflow-hidden">
				<div class="swiper-wrapper">
					<?php foreach ($nhandinh_post as $item) : ?>
						<div class="swiper-slide">
							<div class="nhandinh-block-small">
								<div class="block-small-left">
									<a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
										<?php echo getThumbnail($item, 300, 180, "w-100 img-fluid") ?>
									</a>
								</div>
								<div class="block-small-right">
									<div class="block-small-date highlight">
										<span class="post-day"><?php echo $item->category_title ?></span>
									</div>
									<div class="block-small-text">
										<a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
											<?php echo $item->title ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="nhandinh-pagination swiper-pagination"></div>
			</div>
		</section>
	</section>
<?php endif; ?>