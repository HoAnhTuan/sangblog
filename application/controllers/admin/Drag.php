<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Drag extends Admin_Controller
{
    protected $_data;
    protected $_category;
    protected $_post;
    protected $type;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['drag_model', 'category_model', 'post_model']);
        $this->_post = new Post_model();
        $this->_data = new Drag_model();
        $this->_category = new Category_model();
        $this->type = $this->uri->segment(3);
    }

    public function index($data)
    {
        $data['data'] = [];
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $this->_controller . DIRECTORY_SEPARATOR . 'index', $data, TRUE);
        $this->load->view(TEMPLATE_MAIN, $data);
    }

    public function table_ranking()
    {
        $data['heading_title'] = "Quản lý bảng xếp hạng";
        $data['heading_type'] = "table_ranking";
        $data['heading_description'] = "Danh sách bảng xếp hạng";
        $data['dragInfo'] = $this->_data->getDataDrag($this->type);
        $this->index($data);
    }

    public function keo_noi_bat()
    {
        $data['heading_title'] = "Quản lý tin kèo nỗi bật";
        $data['heading_type'] = "keo_noi_bat";
        $data['heading_description'] = "Danh sách tin trang chủ";
        $data['dragInfo'] = $this->_data->getDataDrag($this->type);
        $this->index($data);
    }

    public function nhacai()
    {
        $data['heading_title'] = "Quản lý thứ tự nhà cái";
        $data['heading_type'] = "nhacai";
        $data['heading_description'] = "Quản lý thứ tự nhà cái";
        $data['dragInfo'] = $this->_data->getDataDrag($this->type);
        $this->index($data);
    }

    public function table_schedule()
    {
        $data['heading_title'] = "Quản lý lịch thi đấu";
        $data['heading_type'] = "table_schedule";
        $data['heading_description'] = "Danh sách lịch thi đấu";
        $data['dragInfo'] = $this->_data->getDataDrag($this->type);
        $this->index($data);
    }

    public function save_drag()
    {
        $input = $this->input->post()['s'];
        $type  = $this->input->post('type');
        foreach ($input as $k => $value) {
            $data[$k]['order'] = $k;
            $data[$k]['id'] = $value['id'];
            $data[$k]['type'] = $type;
        }
        $this->_data->delete(['type' => $type], 'drag');
        if ($this->_data->insertMultiple($data, 'drag')) {
            echo 1;
        } else {
            echo 0;
        };
    }
    /*end drag*/

    public function ajax_load($type)
    {
        $term = $this->input->get("q");
        if ($type === "keo_noi_bat") {
            $data = $this->_post->getPostByTaxonomy([
                'taxonomy' => 'soikeo',
                'slug' => 'soi-keo',
                'limit' => 50,
                'is_status' => 1,
            ]);
        } elseif($type === "nhacai") {
            $params = [
                'is_status' => 1,
                'keyword' => $term,
                'limit' => 100,
                'type' => 'nhacai'
            ];
            $data = $this->_post->getData($params);
        }else{
            $params = [
              'is_status' => 1,
              'keyword' => $term,
              'limit' => 100,
              'type' => 'page'
            ];
            $data = $this->_category->getData($params);
        }

        $output = [];
        if (!empty($data)) foreach ($data as $item) {
            $output[] = ['id' => $item->id, 'text' => $item->title];
        }
        $this->returnJson($output);
    }
}
