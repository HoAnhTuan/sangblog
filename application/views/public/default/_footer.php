<?php
$domain = $this->_settings->domain;
$logo_footer = $this->_settings->logo_footer;
$meta_desc = $this->_settings->meta_description;
$content_footer = $this->_settings->content_footer ?? '';
$email = $this->_settings->email;
$phone = $this->_settings->phone;

$facebook = $this->_settings_social->facebook;
$twitter = $this->_settings_social->twitter;
$youtube = $this->_settings_social->youtube;
$instagram = $this->_settings_social->instagram;
$pinterest = $this->_settings_social->pinterest;
?>

<footer class="text-white top-footer">
	<script src="https://belife.vn/Areas/Preview/Scripts/AckGen/addon-ofs.js"></script>

	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="logo-footer">
						<a href="<?php echo base_url() ?>" rel="nofollow">
							<?php echo getThumbnailStatic((!empty($logo_footer)) ? getImageThumb($logo_footer) : TEMPLATES_ASSETS . 'assets/img/logo.svg', 240, 86, 'logo', "img-fluid logo-footer") ?>
						</a>
					</div>
					<div class="desc-footer">
						<?php echo !empty($content_footer) ? $content_footer : ''; ?>
					</div>
					<ul class="data_seo">
						<?php if (!empty($this->_settings->email)) : ?>
							<li>
								<b>Email:</b>
								<span><?php echo $this->_settings->email ?></span>
							</li>
						<?php endif; ?>

						<?php if (!empty($this->_settings->phone)) : ?>
							<li>
								<b>Phone:</b>
								<span><?php echo $this->_settings->phone ?></span>
							</li>
						<?php endif; ?>
					</ul>
					<a class="mt-2 d-block" href="https://www.dmca.com/compliance/keonhacai9.com" title="DMCA Compliance information for keonhacai9.com">
						<img width="155" height="45" src="https://www.dmca.com/img/dmca-compliant-grayscale.png" alt="DMCA.com Protection Status" />
					</a>
					<div class="mt-3 data_social">
						<div class="circle">
							<a rel="nofollow" target="_blank" href="<?php echo $facebook ?>">
								<i class="fab fa-facebook-f"></i>
							</a>
						</div>
						<div class="circle">
							<a rel="nofollow" target="_blank" href="<?php echo $youtube ?>">
								<i class="fab fa-youtube"></i>
							</a>

						</div>
						<div class="circle">
							<a rel="nofollow" target="_blank" href="<?php echo $instagram ?>">
								<i class="fab fa-instagram"></i>
							</a>

						</div>
						<div class="circle">
							<a rel="nofollow" target="_blank" href="<?php echo $pinterest ?>">
								<i class="fab fa-pinterest-p"></i>
							</a>

						</div>
						<div class="circle">
							<a rel="nofollow" target="_blank" href="<?php echo $twitter ?>">
								<i class="fab fa-twitter"></i>
							</a>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="widget widget-footer">
						<p class="widget-title">Chuyên mục</p>
						<?php echo navMenuFooter('nav-menu-footer', '', 'sub-menu'); ?>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<?php $this->load->view(TEMPLATE_PATH . '/block/widget-post-most-view') ?>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-footer text-dark text-center font-weight-bold py-3 d-block  d-lg-flex justify-content-around align-items-center">
		<div class="">
			<?php echo navMenuPageFooter('list-unstyled d-flex ml-auto mb-0 justify-content-center', '', ''); ?>
		</div>
		<div class="text-white">
			All Rights Reserved. © <?php echo $this->_settings->domain ?> 2021
		</div>

	</div>
</footer>


<?php $this->load->view(TEMPLATE_PATH . '/block/toast') ?>