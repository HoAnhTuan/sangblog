<?php if (!empty($oneItem)): ?>
    <main class=" main-detail"  data-url="<?php echo getUrlPost($oneItem) ?>">
      <div class="container">
	      <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
            <div class="col-12 col-lg-9">
                <div class="section-body">
                    <div class="d-flex flex-wrap justify-content-between">
                        <h1 class="font-23">
                            <?php echo getStringCut($oneItem->title,0,120) ?>

                        </h1>
	                    <?php $this->load->view(TEMPLATE_PATH . "block/rate") ?>
                    </div>
	                <div class="font-13 text-secondary"><?php echo date_post_vn($oneItem->updated_time) ?> - <?php echo timeAgo($oneItem->updated_time,"H:i") ?></div>
	                <div class="font-14 font-weight-bold"><?php echo $oneItem->description ?></div>
                    <?php
                    if (!empty($data_tournament) && $data_tournament->is_round != false) :
                        $data_ranking = json_decode($data_tournament->data_ranks);
                        if (!empty($data_ranking)): foreach ($data_ranking as $key) :
                            $tablerows = $key->tablerows;
                            ?>
                            <div class="ajax-content">
                                <div class="py-2 px-3 text-center bg-primary text-white text-capitalize"><?php echo $key->name ?></div>
                                <div class="overflow-auto">
                                    <table class="table table-striped table-bxh">
                                        <thead>
                                        <tr>
                                            <td class="border-bottom-success">#</td>
                                            <td class="border-bottom-success">Đội</td>
                                            <td class="border-bottom-success">Trận</td>
                                            <td class="border-bottom-success">Thắng</td>
                                            <td class="border-bottom-success">Hòa</td>
                                            <td class="border-bottom-success">Bại</td>
                                            <td class="border-bottom-success">HS</td>
                                            <td class="border-bottom-success">Điểm</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($tablerows as $index => $item) : ?>
                                            <tr>
                                                <td><?php echo $index + 1 ?></td>
                                                <td><?php echo $item->team->mediumname ?></td>
                                                <td><?php echo $item->total ?></td>
                                                <td><?php echo $item->winTotal ?></td>
                                                <td><?php echo $item->drawTotal ?></td>
                                                <td><?php echo $item->lossTotal ?></td>
                                                <td><?php echo $item->goalDiffTotal ?></td>
                                                <td><?php echo $item->pointsTotal ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        <?php endforeach; endif;
                    else : $data_ranking = json_decode($data_tournament->data_ranks);
                        if (!empty($data_ranking)):
                            $data_ranking = $data_ranking[0]->tablerows;
                            ?>

                            <div class="ajax-content">
                                <div class="py-2 px-3 text-center bg-primary text-white text-capitalize"><?php echo $oneItem->title ?></div>
                                <div class="overflow-auto">
                                    <table class="table table-striped table-bxh">
                                        <thead>
                                        <tr>
                                            <td>#</td>
                                            <td class="team">Đội</td>
                                            <td>Trận</td>
                                            <td>Thắng</td>
                                            <td>Hòa</td>
                                            <td>Bại</td>
                                            <td>HS</td>
                                            <td>Điểm</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($data_ranking as $index => $item) : ?>
                                            <tr>
                                                <td><?php echo $index + 1 ?></td>
                                                <td><?php echo $item->team->mediumname ?></td>
                                                <td><?php echo $item->total ?></td>
                                                <td><?php echo $item->winTotal ?></td>
                                                <td><?php echo $item->drawTotal ?></td>
                                                <td><?php echo $item->lossTotal ?></td>
                                                <td><?php echo $item->goalDiffTotal ?></td>
                                                <td><?php echo $item->pointsTotal ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; endif; ?>
                </div>
	            <div class="content-news text-justify">
                  <?php echo $oneItem->content ?>
	            </div>
              <?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer') ?>
            </div>
		        <div class="col-12 col-lg-3">
	              <?php $this->load->view(TEMPLATE_PATH . 'block/nav_sidebar') ?>
	              <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
		        </div>
        </div>
      </div>
    </main>
<?php endif; ?>
