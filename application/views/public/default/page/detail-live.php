<?php if(!empty($oneItem)): ?>
    <main class="container main-detail"  data-url="<?php echo getUrlPost($oneItem) ?>">
        <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
            <article class="col-12">
                <h1 class="font-23">
                    <?php echo getStringCut($oneItem->title,0,120) ?>

                </h1>
                <div class="d-flex flex-wrap justify-content-between my-3">
                    <div class="font-13 text-secondary"><?php echo date_post_vn($oneItem->updated_time) ?> - <?php echo timeAgo($oneItem->updated_time,"H:i") ?></div>
                    <?php $this->load->view(TEMPLATE_PATH . "block/rate") ?>
                </div>
                <div class="font-14 font-weight-bold mb-3">
                    <?php echo $oneItem->description ?>
                </div>
                <div class="container px-0">
                    <div class="schedule-live">
                        <?php $this->load->view(TEMPLATE_PATH . "match/_ajax_match_play") ?>
                    </div>
                </div>
	            <div id="ajax_content" class="row ajax-content">
                  <?php if(!empty($data)) foreach ($data as $item): ?>
				            <div class="col-12 col-lg-4">
					            <a href="javascript:;" title="<?php echo $item->category_title ?>" class="input-comment text-uppercase text-white py-1 px-2 d-inline-block font-11 position-absolute cate-absolute"><?php echo $item->category_title ?></a>
					            <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                          <?php echo getThumbnail($item, 350, 200, "img-fluid w-100 mb-3") ?>
					            </a>
					            <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
						            <h3 class="font-16 text-black2 max-line-2"><?php echo $item->title ?></h3>
					            </a>
					            <span class="text-secondary font-12"><?php echo timeAgo($item->displayed_time,"d/m/Y") ?></span>
					            <p class="max-line-2 text-secondary font-13"><?php echo $item->description ?></p>
				            </div>
                  <?php endforeach; ?>
	            </div>
	            <div class="col-12 text-center">
                  <?php echo !empty($pagination) ? $pagination : '' ?>
	            </div>
	            <div class="content-news text-justify">
                  <?php echo $oneItem->content ?>
	            </div>

                <div class="social-share p-3 text-center border-bottom"></div>
                <div class=""></div>
            </article>
        </div>
    </main>
<?php endif; ?>