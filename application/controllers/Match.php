<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Match extends Public_Controller {

    protected $cid = 0;
    protected $_data;
    protected $_post;
    protected $_tournament;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('match_model','tournament_model','post_model'));
        $this->_post = new Post_model();
        $this->_data = new Match_model();
        $this->_tournament = new Tournament_model();
    }

    public function detail($id)
    {
        $this->setCacheFile(5);
        $data['oneItem'] = $oneItem = $this->_data->getById($id);
        if(empty($oneItem)) redirect("truc-tiep-bong-da","location","301");
        $data['h2h'] = $this->_data->getH2H("$oneItem->home_id-$oneItem->away_id");
        $data['last10'] = $this->_data->getLast10("$oneItem->home_id-$oneItem->away_id");
        $data['onePost'] = $this->_post->getDataByField("match_id", $oneItem->match_id);
        $data['data_score'] = json_decode($oneItem->data_score);
        $data['data_info'] = $data_info = json_decode($oneItem->data_info);
        $data['data_odd_info'] = $this->_data->getMatchOdds($oneItem->match_id)[0];
        if (!empty($oneItem->data_lineup)) $data['data_lineup'] = json_decode($oneItem->data_lineup);
        if (!empty($oneItem->data_stats_form)) $data['data_stats_form'] = json_decode($oneItem->data_stats_form);
        if (!empty($oneItem->data_statistic)) $data['data_statistic'] = json_decode($oneItem->data_statistic);
        if (!empty($oneItem->data_event)) $data['data_event'] = json_decode($oneItem->data_event);
        if (!empty($oneItem->data_ranks)) $data['data_ranks'] = json_decode($oneItem->data_ranks);

        $this->breadcrumbs->push("Trang chủ", base_url());
        $this->breadcrumbs->push($oneItem->title??'', getUrlMatch($oneItem));
        $data['breadcrumb'] = $this->breadcrumbs->show();
        //SEO Meta

        $data_info_stadium = !empty($data_info->stadium) ? $data_info->stadium : null;
        $data_info_referee = !empty($data_info->referee) ? $data_info->referee : null;
        $data['stadium_name'] = $stadium_name = $data_info_stadium->name ?? '';
        $data['referee_name'] = $referee_name = $data_info_referee->name ?? '';
        $data['SEO'] = [
            'meta_title' => "Thông tin trận đấu {$oneItem->name_home} vs {$oneItem->name_away} " . date('d/m/Y', $oneItem->start_timestamp) ,
            'meta_description' => "Thông tin trận đấu {$oneItem->name_home} vs {$oneItem->name_away} sẽ diễn ra vào lúc ". date('H:i', $oneItem->start_timestamp) ." ngày ". date('d/m/Y', $oneItem->start_timestamp) ." tại sân vận động {$stadium_name}.",
            'meta_keyword' => "Thông tin trận đấu $oneItem->name_home vs $oneItem->name_away",
            'url' => getUrlMatch($oneItem),
            'is_robot' => 1,
//            'image' => getImageThumb("", 400, 200)
            'image' => ''
        ];

        $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'match/detail', $data, TRUE);
        $this->load->view(TEMPLATE_MAIN, $data);
    }


    public function ajax_load_match(){
        $this->checkRequestPostAjax();
        $date = $this->input->post('date');
        $data['list_tournament'] = $this->_tournament->getDataFeatured();
        $data['data_match'] = $this->_data->getDataMatch([
            'date' => $date
        ]);
        $response['data'] = $this->load->view(TEMPLATE_PATH . 'match/_ajax_match_today', $data, TRUE);
        $this->returnJson($response);
    }

    public function ajax_load_match_play(){
        $this->checkRequestPostAjax();
        $type = $this->input->post('type');
        $data = [];
        switch ($type){
            case "playing":
                $params = [
                    'tournament_id' => '',
                    'is_playing' => 1,
                    'is_link' => true,
//                    'type_link' => 'natra',
                    'limit' => 20
                ];
                $data['data_match'] = $this->_data->getSchedule($params);
                break;
            case "hot":
                $params = [
                    'tournament_id' => '',
                'is_link' => true,
//                'type_link' => 'natra',
                ];
                $dataMatch = $this->_data->getSchedule($params);
                if(!empty($dataMatch)) foreach ($dataMatch as $item){
                    if($item->is_hot == 1) $data['data_match'][] = $item;
                }
                break;
            case "soon":
                $params = [
                    'tournament_id' => '',
                'is_link' => true,
//                'type_link' => 'natra',
                ];
                $dataMatch = $this->_data->getSchedule($params);
                if(!empty($dataMatch)) foreach ($dataMatch as $item){
                    if(strtotime("+2 hours") >= $item->start_timestamp) $data['data_match'][] = $item;
                }
                break;

            default: $params = [
                'tournament_id' => '',
                'is_link' => true,
                'playing_and_start' => 1,
                'limit' => 100
            ];
                $data['data_match'] = $this->_data->getDataMatch($params);

        }
        $response['data'] = $this->load->view(TEMPLATE_PATH . 'match/_ajax_match_play', $data, TRUE);
        $this->returnJson($response);
    }

    public function ajax_load_match_livescore(){
        $this->checkRequestPostAjax();
        $date = $this->input->post('date');
        $data['list_match'] = $this->_data->getDataMatch([
            'date' => $date
        ]);
        $response['data'] = $this->load->view(TEMPLATE_PATH . 'match/_ajax_match_play', $data, TRUE);
        $this->returnJson($response);
    }

    public function ajax_load_score()
    {
        $this->checkRequestPostAjax();
        $key = "aj_live";
        $dataScore = $this->getCache($key);
        if(empty($dataScore)){
            $dataScore = [];
            $data = $this->_data->getMatchPlaying();
            if(!empty($data)) foreach ($data as $item){
                $dataScore[$item->id] = $item;
            }
            $this->setCache($key,$dataScore,60);
        }
        $this->returnJson($dataScore);
    }

    public function ajax_load_livescore(){
        $this->checkRequestPostAjax();
        $type = $this->input->post('type');
        $date = $this->input->post('date');

        if(empty($date)) $date = date('Y-m-d');
        $date = date('Y-m-d',strtotime(str_replace('/','-',$date)));
        if($type === 'result') $listMatch = $this->_data->getResult(false,$date);
        else $listMatch = $this->_data->getSchedule(false,$date,'');

        $listResult = [];
        if (!empty($listMatch)) foreach ($listMatch as $item){
            $tmp['league_name'] = $item->league_name;
            $listResult[] = array_merge((array)$item,$tmp);
        }
        $data['data'] = $this->array_group_by($listResult, function ($i) {
            return $i['league_name'];
        });
        $content = $this->load->view($this->template_path . "match/_ajax_schedule_result",$data,TRUE);
        dd($content);
        echo $content;exit;
    }

}