<section class="section mt-70">
	<section class="section-header category-name">
		<h2 class="category-title">Tỷ lệ kèo</h2>
	</section>
	<section class="section-body nhacai-container overflow-hidden">
		<div class="competition">
			<!--<div class="competition-top">-->
			<!--    <h3 class="competition-top-title">Tỉ lệ kèo</h3>-->
			<!--    <div class="competition-top-cate">-->
			<!--        <span>TỈ LỆ CHÂU Á</span>-->
			<!--        <span>TỔNG SỐ BÀN THẮNG</span>-->
			<!--        <span>TỈ LỆ CHÂU ÂU</span>-->
			<!--    </div>-->
			<!--</div> -->
			<div class="competition-middle">
				<table>
					<tr>
						<th>Ngày/Giờ</th>
						<th>Trận Đấu</th>
						<th>Thắng</th>
						<th>Hoà</th>
						<th>Thua</th>
						<th>Chủ</th>
						<th>Tỉ Lệ</th>
						<th>Khách</th>
						<th>Số BT</th>
						<th>Trên</th>
						<th>Dưới</th>
					</tr>
					<?php
					if (!empty($match_odds)) :
						$key = 0;
						foreach ($match_odds as $data_item_match) : ?>
							<tr data-toggle="collapse" data-target=".tlk-<?= $key ?>" aria-expanded="<?= $key == 0 ? 'true' : 'false' ?>">
								<td colspan="11"><?php echo $data_item_match[0]->league_name ?></td>
							</tr>
							<?php foreach ($data_item_match as $item) : ?>
								<tr class="tlk-<?= $key ?> collapse <?= $key == 0 ? 'show' : '' ?>">
									<td class="text-green"><?php echo timeAgo($item->start_time, "d/m H:i") ?></td>
									<td>
										<span><?php echo $item->name_home ?></span>
										vs
										<span><?php echo $item->name_away ?></span>
									</td>
									<?php
									$data_bets = json_decode($item->data_top);
									if (!empty($data_bets)) :
										$chau_au = array_values((array)$data_bets->chau_au);
										$chau_a = array_values((array)$data_bets->chau_a);
										$tai_xiu = array_values((array)$data_bets->tai_xiu);
									?>
										<td class="text-danger font-weight-bold"><?php echo $chau_au[0] ?></td>
										<td class="text-danger"><?php echo $chau_au[1] ?></td>
										<td><?php echo $chau_au[2] ?></td>
										<td><?php echo $chau_a[0] ?></td>
										<td><?php echo $chau_a[1] ?></td>
										<td><?php echo $chau_a[2] ?></td>
										<td class="text-success"><?php echo $tai_xiu[0] ?></td>
										<td><?php echo $tai_xiu[1] ?></td>
										<td><?php echo $tai_xiu[2] ?></td>
									<?php endif; ?>
								</tr>
							<?php endforeach;
							$key++ ?>
						<?php endforeach;
					else : ?>
						<tr>
							<td colspan="11">Chưa có dữ liệu mới ...</td>
						</tr>
					<?php endif; ?>
				</table>
			</div>
			<div class="competition-bottom mb-3">
				<div class="row mt-4">
					<div class="col-12 col-sm-3">
						<span class="competition-bottom-circle green"></span>
						<span>Đêm nay</span>
					</div>
					<div class="col-12 col-sm-3">
						<span class="competition-bottom-circle orange"></span>
						<span>Lựa chọn của chuyên gia</span>
					</div>
					<div class="col-12 col-sm-3">
						<span class="competition-bottom-circle success"></span>
						<span>Tỉ lệ chấp bóng thay đổi</span>
					</div>
					<div class="col-12 col-sm-3">
						<span class="competition-bottom-circle bluered"></span>
						<span>Tỷ lệ cược thay đổi</span>
					</div>
				</div>
				<div class="competition-bottom-note my-4 pb-1">
					Ghi chú: Để hiển thị tỷ lệ cược châu Á trực tuyến hãy di con trỏ lên trận đấu bạn quan
					tâm!
				</div>
			</div>
		</div>
	</section>
</section>