<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends Public_Controller
{
  protected $_post;
  protected $_all_category;
  protected $_category;
  protected $_tournament;
  protected $_match;
  protected $_reviews;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(['post_model', 'category_model', 'reviews_model', 'match_model', 'tournament_model']);
    $this->_post = new Post_model();
    $this->_category = new Category_model();
    $this->_reviews = new Reviews_model();
    $this->_tournament = new Tournament_model();
    $this->_match = new Match_model();
    $this->_all_category = $this->_category->_all_category();
  }

  public function detail($slug, $page = 1)
  {
    $oneItem = $this->_category->getBySlugCached($slug);
    if (empty($oneItem)) {
      show_404();
    }
    $data = [];
    switch ($oneItem->type) {
      case 'tournament':
        $data = $this->category_tournament($oneItem);
        $layoutView = '';
        if (!empty($oneItem->layout)) {
          $layoutView = '-' . $oneItem->layout;
        }
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/category' . $layoutView, $data, true);
        break;

      case 'soikeo':
        $data = $this->category_soikeo($oneItem, $page);
        $layoutView = '';
        if (!empty($oneItem->layout)) {
          $layoutView = '-' . $oneItem->layout;
        }
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/category' . $layoutView, $data, true);
        break;

      case 'nhacai':
        $data = $this->category_nhacai($oneItem, $page);
        $layoutView = '';
        if (!empty($oneItem->layout)) {
          $layoutView = '-' . $oneItem->layout;
        }
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/category' . $layoutView, $data, true);
        break;

      case 'gamebai':
        $data = $this->category_gamebai($oneItem, $page);
        $layoutView = '';
        if (!empty($oneItem->layout)) {
          $layoutView = '-' . $oneItem->layout;
        }
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/category' . $layoutView, $data, true);
        break;

      case 'banca':
        $data = $this->category_banca($oneItem, $page);
        $layoutView = '';
        if (!empty($oneItem->layout)) {
          $layoutView = '-' . $oneItem->layout;
        }
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/category' . $layoutView, $data, true);
        break;

      case 'schedule':

        $data = $this->category_schedule($oneItem);
        $layoutView = '-' . $oneItem->type;
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'page/detail' . $layoutView, $data, true);
        break;
      case 'result':
        $data = $this->category_result($oneItem);
        $layoutView = '-' . $oneItem->type;
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'page/detail' . $layoutView, $data, true);
        break;
      case 'rank':
        $data = $this->category_rank($oneItem);
        $layoutView = '-' . $oneItem->type;
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'page/detail' . $layoutView, $data, true);
        break;

      case 'page':
        $data = $this->category_page($oneItem, $page);
        $layoutView = '';
        if (!empty($oneItem->layout)) {
          $layoutView = '-' . $oneItem->layout;
        }
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/detail' . $layoutView, $data, true);
        break;
      default:
        $data = $this->category_post($oneItem, $page);
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'post/category', $data, true);
    }

    // iframe
    $keyword = $this->input->get('embed');
    if (isset($keyword)) {
      switch ($keyword) {
        case 'result':
          $this->load->view(TEMPLATE_PATH . 'page/embed_result', $data);
          break;

        case 'schedule':
          $this->load->view(TEMPLATE_PATH . 'page/embed_schedule', $data);
          break;
      }
    } else {


      $this->load->view(TEMPLATE_MAIN, $data);
    }
  }

  public function author($id, $page = 1)
  {
    $data['oneItem'] = $data['user'] = $oneItem = $this->_category->getById($id);
    if (empty($oneItem)) {
      show_404();
    }
    $limit = 10;
    $params = [
      'author_id' => $id,
      'page' => $page,
      'limit' => $limit,
    ];
    $data['list_post'] = $this->_post->getDataFE($params);
    $totalPost = $this->_post->getTotalFE($params);
    $data['page'] = $page = (int)round($totalPost / $limit);
    $this->load->library('pagination');
    $paging['base_url'] = getUrlAuthor($oneItem);
    $paging['first_url'] = getUrlAuthor($oneItem);
    $paging['total_rows'] = $totalPost;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Trang chủ', base_url());
    $this->breadcrumbs->push($oneItem->title, getUrlAuthor($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlAuthor($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => getImageThumb($oneItem->thumbnail, 600, 314),
    ];
    $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'post/author', $data, true);
    $this->setCacheFile(5);
    $this->load->view(TEMPLATE_MAIN, $data);
  }

  public function tags($slug, $page = 1)
  {
    $data['oneItem'] = $oneItem = $this->_category->getBySlugCached($slug);
    if (empty($oneItem)) {
      show_404();
    }
    $limit = 10;
    $params = [
      'tag_id' => $oneItem->id,
      'page' => $page,
      'limit' => $limit
    ];
    $data['data'] = $this->_post->getDataFE($params);
    $totalPost = $this->_post->getTotalFE($params);
    $this->load->library('pagination');
    $paging['base_url'] = getUrlTag($oneItem);
    $paging['total_rows'] = $totalPost;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Trang chủ', base_url());
    $this->breadcrumbs->push("Tag: " . $oneItem->title, getUrlTag($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : $oneItem->title,
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : $oneItem->description,
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlTag($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'post/tags', $data, true);
    $this->load->view(TEMPLATE_MAIN, $data);
  }

  private function category_schedule($oneItem)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "schedule") {
      show_404();
    }

    $data['data_tournament'] = $oneTournament = $this->_tournament->get_by_id_api($oneItem->tournament_id);
    $data['data_match'] = $this->_match->getDataMatch([
      'tournament_id' => $oneItem->tournament_id,
      'season_id' => $oneTournament->season_id
    ]);

    $data['reviews'] = $this->_reviews->getRate([
      'url' => getUrlPage($oneItem)
    ]);

    $this->breadcrumbs->push('Home', base_url());
    $this->breadcrumbs->push($oneItem->title, getUrlPage($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlPage($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_result($oneItem)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "result") {
      show_404();
    }
    $data['data_tournament'] = $oneTournament = $this->_tournament->get_by_id_api($oneItem->tournament_id);
    $data['data_match'] = $this->_match->getDataMatch([
      'tournament_id' => $oneItem->tournament_id,
      'season_id' => $oneTournament->season_id
    ]);
    $data['reviews'] = $this->_reviews->getRate([
      'url' => getUrlPage($oneItem)
    ]);

    $this->breadcrumbs->push('Home', base_url());
    $this->breadcrumbs->push($oneItem->title, getUrlPage($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlPage($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_rank($oneItem)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "rank") {
      show_404();
    }
    $data['list_tournament'] = $this->_tournament->getDataFeatured();
    $data['data_tournament'] = $this->_tournament->get_by_id_api($oneItem->tournament_id);
    $data['reviews'] = $this->_reviews->getRate([
      'url' => getUrlPage($oneItem)
    ]);

    $this->breadcrumbs->push('Home', base_url());
    $this->breadcrumbs->push($oneItem->title, getUrlPage($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlPage($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_page($oneItem, $page)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "page") {
      show_404();
    }
    $data['reviews'] = $this->_reviews->getRate([
      'url' => getUrlPage($oneItem)
    ]);
    switch ($oneItem->layout) {
      case "tlk":
        $data['data_ty_le_keo'] = $this->get_ty_le_keo();
        break;
      case "live":
        $data['data_match'] = $this->_match->getDataMatch([
          'tournament_id' => '',
          'is_link' => true,
          'playing_and_start' => 1,
          'limit' => 100
        ]);
        $data['data_post'] = $this->_post->getDataFE([
          'category_id' => 374,
          'limit' => 20,
          'is_status' => 1,
          'order' => ['id' => 'DESC']
        ]);
        $id = 45;
        $this->_category->_recursive_child_id($this->_all_category, $id);
        $listCateId = $this->_category->_list_category_child_id;
        $limit = 10;
        $data['data'] = $this->_post->getDataFE([
          'category_id' => $listCateId,
          'page' => $page,
          'limit' => $limit,
          'is_status' => 1,
          'order' => ['id' => 'DESC']
        ]);

        $data['recent_post'] = $this->_post->getDataFE([
          'is_status' => 1,
          'limit' => 6,
        ]);
        $data['total'] = $total = $this->_post->getTotalFE([
          'category_id' => $oneItem->id,
          'is_status' => 1
        ]);

        $data['page'] = $page;
        $this->load->library('pagination');
        $paging['base_url'] = getUrlCategory($oneItem);
        $paging['first_url'] = getUrlCategory($oneItem);
        $paging['total_rows'] = $total;
        $paging['per_page'] = $limit;
        $paging['cur_page'] = $page;
        $this->pagination->initialize($paging);
        $data['pagination'] = $this->pagination->create_links();
        // end phân Trang

        break;
      case "livescore":
        $data['data_match'] = $this->_match->getSchedule([
          'tournament_id' => ''
        ]);
        break;
      case "schedule_today":
        $list_tournament = $this->_tournament->getDataFeatured();
        if (!empty($list_tournament)) {
          foreach ($list_tournament as $item) {
            $data_tournament[$item->tournament_id] = $item->title;
          }
          $data['list_tournament'] = $data_tournament;
        }
        $data['data_match'] = $this->_match->getSchedule(['date' => date('Y-m-d')]);
        break;
      case "result_today":
        $list_tournament = $this->_tournament->getDataFeatured();
        if (!empty($list_tournament)) {
          foreach ($list_tournament as $item) {
            $data_tournament[$item->tournament_id] = $item->title;
          }
          $data['list_tournament'] = $data_tournament;
        }
        $data['data_match'] = $this->_match->getResult(['date' => date('Y-m-d')]);
        break;
      case "rank_today":
        $data['list_tournament'] = getDataCategory('rank');

        $list = [];
        foreach ($data['list_tournament'] as $item) {
          array_push($list, $this->_tournament->get_by_id_api($item->tournament_id));
        }

        $data['data_tournament'] = $list;
        //                dd($data['data_tournament']);
        break;
    }
    $this->breadcrumbs->push('Home', base_url());
    $this->breadcrumbs->push($oneItem->title, getUrlPage($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlPage($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function get_ty_le_keo()
  {
    function handleDate($match)
    {
      $match = preg_replace_callback(
        "/\d+(\.\d){0,1}/",
        function ($data) {
          return number_format((float)$data[0], 2);
        },
        $match[0]
      );
      return $match;
    }

    $pattern = '/[>]\d+(\.\d{0,1}){0,1}[-]/';
    $pattern2 = '/[-]\d+(\.\d{0,1}){0,1}[<]/';
    $pattern3 = '/[>]\d+(\.\d{0,1}){0,1}[<]/';
    $contentHtml = "";
    if (!$this->agent->is_mobile()) {
      $contentKeo = $this->_match->getTyLeKeoPC();
      $contentKeo = preg_replace_callback($pattern, "handleDate", $contentKeo);
      $contentKeo = preg_replace_callback($pattern2, "handleDate", $contentKeo);
      $contentKeo = preg_replace_callback($pattern3, "handleDate", $contentKeo);
      if (!empty($contentKeo)) {
        $contentHtml = '<div class="tb-bet-data">' . $contentKeo . '</div>';
      }
    } else {
      $contentKeo = $this->_match->getTyLeKeoMobile();;
      $contentHtml = '<div class="bet-mobile">' . $contentKeo . '</div>';
    }
    $contentHtml = str_replace('<font', '<span', $contentHtml);
    $contentHtml = preg_replace('/(?<=\D)(-\d\.\d\d?)/m', '<span class="soam">$1</span>', $contentHtml);
    return $contentHtml;
  }

  private function category_post($oneItem, $page)
  {

    $data['oneItem'] = $oneItem;
    //        if($oneItem->type !== "post") show_404();
    $id = $oneItem->id;
    $this->_category->_recursive_child_id($this->_all_category, $id);
    $listCateId = $this->_category->_list_category_child_id;
    $limit = 9;
    $data['data'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'page' => $page,
      'limit' => $limit,
      'is_status' => 1,
      'order' => ['id' => 'DESC']
    ]);

    $data['recent_post'] = $this->_post->getDataFE([
      'is_status' => 1,
      'limit' => 6,
    ]);
    $data['total'] = $total = $this->_post->getTotalFE([
      'category_id' => $oneItem->id,
      'is_status' => 1
    ]);

    $data['page'] = $page;
    $this->load->library('pagination');
    $paging['base_url'] = getUrlCategory($oneItem);
    $paging['first_url'] = getUrlCategory($oneItem);
    $paging['total_rows'] = $total;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Trang chủ', base_url());
    $this->_category->_recursive_parent($this->_all_category, $oneItem->id);
    if (!empty($this->_category->_list_category_parent)) {
      foreach (array_reverse($this->_category->_list_category_parent) as $item) {
        $this->breadcrumbs->push($item->title, getUrlCategory($item));
      }
    }
    $this->breadcrumbs->push($oneItem->title, getUrlCategory($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();
    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlCategory($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_tournament($oneItem)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "tournament") {
      show_404();
    }
    $id = $oneItem->id;
    $this->_category->_recursive_child_id($this->_all_category, $id);
    $listCateId = $this->_category->_list_category_child_id;
    $limit = 10;
    $get = $this->input->get();
    $page = isset($get['page']) ? $get['page'] : 1;
    $data['list_post'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'page' => $page,
      'limit' => $limit,
      'is_status' => 1,
      'order' => ['id' => 'DESC']
    ]);

    $data['recent_post'] = $this->_post->getDataFE([
      'is_status' => 1,
      'limit' => 6,
    ]);
    $data['total'] = $total = $this->_post->getTotalFE([
      'category_id' => $oneItem->id,
      'is_status' => 1
    ]);

    $data['page'] = $page;
    $this->load->library('pagination');
    $paging['base_url'] = getUrlCategory($oneItem);
    $paging['first_url'] = getUrlCategory($oneItem);
    $paging['total_rows'] = $total;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Trang chủ', base_url());
    $this->_category->_recursive_parent($this->_all_category, $oneItem->id);
    if (!empty($this->_category->_list_category_parent)) {
      foreach (array_reverse($this->_category->_list_category_parent) as $item) {
        $this->breadcrumbs->push($item->title, getUrlCategory($item));
      }
    }
    $this->breadcrumbs->push($oneItem->title, getUrlCategory($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();
    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlCategory($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_soikeo($oneItem, $page)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "soikeo") {
      show_404();
    }
    $id = $oneItem->id;
    $data['list_child'] = $this->_category->getListChild("soikeo");
    $this->_category->_recursive_child_id($this->_all_category, $id);
    // $listCateId = $this->_category->_list_category_child_id;
    $limit = 36;
    $data['data'] = $this->_post->getDataFE([
      'category_id' => $id,
      'page' => $page,
      'limit' => $limit,
      'is_status' => 1,
      'order' => ['displayed_time' => 'DESC'],
      'type' => 'soikeo'
    ]);

    $data['recent_post'] = $this->_post->getDataFE([
      'is_status' => 1,
      'limit' => 6,
      'type' => 'soikeo'
    ]);
    $data['total'] = $total = $this->_post->getTotalFE([
      'category_id' => $oneItem->id,
      'is_status' => 1,
      'type' => 'soikeo'
    ]);


    $data['page'] = $page;
    $this->load->library('pagination');
    $paging['base_url'] = getUrlCategory($oneItem);
    $paging['total_rows'] = $total;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Home', base_url());
    $this->_category->_recursive_parent($this->_all_category, $oneItem->id);
    if (!empty($this->_category->_list_category_parent)) {
      foreach (array_reverse($this->_category->_list_category_parent) as $item) {
        $this->breadcrumbs->push($item->title, getUrlCategory($item));
      }
    }
    $this->breadcrumbs->push($oneItem->title, getUrlCategory($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();
    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlCategory($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_nhacai($oneItem, $page)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "nhacai") {
      show_404();
    }

    $id = $oneItem->id;
    $this->_category->_recursive_child_id($this->_all_category, $id);
    $listCateId = $this->_category->_list_category_child_id;
    $limit = 9;


    $drag = getDrag('nhacai');
    $temp = $this->_post->getDataFE([
      'type' => 'nhacai',
      'in' => array_map(function ($item) {
        return (int)$item->id;
      }, $drag),
      'limit' => 5,
      'is_primary_category' => true,
      'order' => ['order' => 'ASC']
    ]);
    $data['top_dealer'] = [];
    foreach ($drag as $d) {
      foreach ($temp as $list) {
        if ($d->id == $list->id) {
          array_push($data['top_dealer'], $list);
        }
      }
    }
    $data['data'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'page' => $page,
      'limit' => $limit,
      'is_status' => 1,
      'order' => ['id' => 'DESC'],
      'post__not_in' => array_map(function ($item) {
        return (int) $item->id;
      }, $data['top_dealer'])
    ]);


    $data['recent_post'] = $this->_post->getDataFE([
      'is_status' => 1,
      'limit' => 6,
      'type' => 'nhacai'
    ]);
    $data['total'] = $total = $this->_post->getTotalFE([
      'category_id' => $oneItem->id,
      'is_status' => 1,
      'type' => 'nhacai'
    ]);

    $data['page'] = $page;
    $this->load->library('pagination');
    $paging['base_url'] = getUrlCategory($oneItem);
    $paging['first_url'] = getUrlCategory($oneItem);
    $paging['total_rows'] = $total;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Home', base_url());
    $this->_category->_recursive_parent($this->_all_category, $oneItem->id);
    if (!empty($this->_category->_list_category_parent)) {
      foreach (array_reverse($this->_category->_list_category_parent) as $item) {
        $this->breadcrumbs->push($item->title, getUrlCategory($item));
      }
    }
    $this->breadcrumbs->push($oneItem->title, getUrlCategory($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();
    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlCategory($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_banca($oneItem, $page)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "banca") {
      show_404();
    }
    $id = $oneItem->id;
    $this->_category->_recursive_child_id($this->_all_category, $id);
    $listCateId = $this->_category->_list_category_child_id;
    $limit = 10;
    $data['top_dealer'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'limit' => 6,
      'order' => ['order' => 'ASC'],
      'type' => 'banca'
    ]);

    $data['data'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'page' => $page,
      'limit' => $limit,
      'is_status' => 1,
      'order' => ['id' => 'DESC'],
      'type' => 'banca',
      /*'post__not_in' => array_map(function ($item){
                return (int) $item->id;
            },$data['top_dealer'])*/
    ]);


    $data['recent_post'] = $this->_post->getDataFE([
      'is_status' => 1,
      'limit' => 6,
      'type' => 'banca'
    ]);
    $data['total'] = $total = $this->_post->getTotalFE([
      'category_id' => $oneItem->id,
      'is_status' => 1,
      'type' => 'banca'
    ]);

    $data['page'] = $page;
    $this->load->library('pagination');
    $paging['base_url'] = getUrlCategory($oneItem);
    $paging['first_url'] = getUrlCategory($oneItem);
    $paging['total_rows'] = $total;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Home', base_url());
    $this->_category->_recursive_parent($this->_all_category, $oneItem->id);
    if (!empty($this->_category->_list_category_parent)) {
      foreach (array_reverse($this->_category->_list_category_parent) as $item) {
        $this->breadcrumbs->push($item->title, getUrlCategory($item));
      }
    }
    $this->breadcrumbs->push($oneItem->title, getUrlCategory($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();
    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlCategory($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  private function category_gamebai($oneItem, $page)
  {
    $data['oneItem'] = $oneItem;
    if ($oneItem->type !== "gamebai") {
      show_404();
    }
    $id = $oneItem->id;
    $this->_category->_recursive_child_id($this->_all_category, $id);
    $listCateId = $this->_category->_list_category_child_id;
    $limit = 10;
    $data['top_dealer'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'order' => ['order' => 'ASC'],
      'limit' => 6,
      'type' => 'gamebai'
    ]);

    $data['data'] = $this->_post->getDataFE([
      'category_id' => $listCateId,
      'page' => $page,
      'limit' => $limit,
      'is_status' => 1,
      'order' => ['id' => 'DESC'],
      'type' => 'gamebai',
      /*'post__not_in' => array_map(function ($item){
                return (int) $item->id;
            },$data['top_dealer'])*/
    ]);


    $data['recent_post'] = $this->_post->getDataFE([
      'is_status' => 1,
      'limit' => 6,
      'type' => 'gamebai'
    ]);
    $data['total'] = $total = $this->_post->getTotalFE([
      'category_id' => $oneItem->id,
      'is_status' => 1,
      'type' => 'gamebai'
    ]);

    $data['page'] = $page;
    $this->load->library('pagination');
    $paging['base_url'] = getUrlCategory($oneItem);
    $paging['first_url'] = getUrlCategory($oneItem);
    $paging['total_rows'] = $total;
    $paging['per_page'] = $limit;
    $paging['cur_page'] = $page;
    $this->pagination->initialize($paging);
    $data['pagination'] = $this->pagination->create_links();
    // end phân Trang

    $this->breadcrumbs->push('Home', base_url());
    $this->_category->_recursive_parent($this->_all_category, $oneItem->id);
    if (!empty($this->_category->_list_category_parent)) {
      foreach (array_reverse($this->_category->_list_category_parent) as $item) {
        $this->breadcrumbs->push($item->title, getUrlCategory($item));
      }
    }
    $this->breadcrumbs->push($oneItem->title, getUrlCategory($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();
    $data['SEO'] = [
      'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : '',
      'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
      'url' => getUrlCategory($oneItem),
      'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
      'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
    ];
    return $data;
  }

  public function ajax_load_data_today()
  {
    $this->checkRequestPostAjax();
    $data['date'] = $date = $this->input->post('date');
    $layout = $this->input->post('layout');
    if (!empty($date)) {
      $list_tournament = $this->_tournament->getDataFeatured();
      if (!empty($list_tournament)) {
        foreach ($list_tournament as $item) {
          $data_tournament[$item->tournament_id] = $item->title;
        }
        $data['list_tournament'] = $data_tournament;
      }
      if ($layout === 'result_today') {
        $data['data_match'] = $this->_match->getResult(['date' => date('Y-m-d', strtotime($date))]);
      } else {
        $data['data_match'] = $this->_match->getSchedule(['date' => date('Y-m-d', strtotime($date))]);
      }
      $message['type'] = 'Success';
      $message['message'] = 'Tải dữ liệu thành công';
      $message['data'] = $this->load->view(TEMPLATE_PATH . 'match/_table_schedule', $data, true);
    } else {
      $message['type'] = 'Error';
      $message['message'] = 'Có lỗi xảy ra';
    }

    $this->returnJson($message);
  }

  public function testexcel(){
      $data = $this->_post->getDataFe([
          'select' => 'id,title, slug, type',
          'displayed_time_less' => '2021-10-2',
          'limit' => 30000
      ]);
      if(!empty($data)){
          $this->load->library('PHPExcel');
          $path = APPPATH . 'controllers' . DIRECTORY_SEPARATOR . 'temp.xlsx';
          $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
          $excel2 = $excel2->load($path); // Empty Sheet
          $excel2->setActiveSheetIndex(0);

          $excel2->getActiveSheet()->getRowDimension("1")->setRowHeight(30);
          $excel2->getActiveSheet()->setCellValue("A1","Title");
          $excel2->getActiveSheet()->setCellValue("B1","Link");
          $excel2->getActiveSheet()->setCellValue("C1","Displayed Time");
          $excel2->getActiveSheet()->setCellValue("D1","Type");
          foreach ($data as $key => $item) {
              $j = $key + 2;
              $excel2->getActiveSheet()->getRowDimension("$j")->setRowHeight(30);
              $excel2->getActiveSheet()->setCellValue("A$j",$item->title);
              $excel2->getActiveSheet()->setCellValue("B$j",getUrlPost($item));
              $excel2->getActiveSheet()->setCellValue("C$j",$item->displayed_time);
              $excel2->getActiveSheet()->setCellValue("D$j",$item->type);
          }
          $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
          $filename = 'posts' . date("Ymdhi");// set filename for excel file to be exported
          header('Content-Type: application/vnd.ms-excel'); // generate excel file
          header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
          header('Cache-Control: max-age=0');
          $objWriter->save('php://output');
      }else{
          die('Không có dữ liệu');
      }

  }
}
