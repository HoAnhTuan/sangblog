<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['cms_banner'] = [
  'header_left' => "Banner header left",
  'header_right' => "Banner header right",
  'top_left' => "Banner top left",
  'top_right' => "Banner top right",
  'sidebar_top' => "Banner sidebar top",
  'sidebar_middle' => "Banner sidebar middle",
  'sidebar_bottom' => "Banner sidebar bottom",
  'home_middle' => "Banner middle home page",
  'home_footer_left' => "Banner footer left",
  'home_footer_right' => "Banner footer right",
  'catfish' => 'Banner catfish',
  'banner_catfish_mobile' => "Banner catfish mobile",
  'footer' => "Banner footer",
  'fixed_left' => "Banner fixed left",
  'fixed_right' => "Banner fixed right",
  'home_post' => "Banner home post",
  'home_post_bottom' => "Banner home post bottom",
  'popup' => 'Banner popup'
];