<!--        sidebar-->
<aside class="col-12 col-lg-4 pl-lg-2 mt-3 mt-lg-0">
    <!--    --><?php //$this->load->view(TEMPLATE_PATH . '/block/widget_tab_data') 
                ?>

    <?php $listSoikeo = getDataPost([
        'type' => 'soikeo',
        'limit' => 5,
        'order' => ['displayed_time' => 'DESC']
    ]) ?>
    <div class="shadow">
        <p class="title-bg-danger text-white mb-0">Bài Mới Cập Nhật</p>
        <div class="d-flex flex-wrap">
            <?php if (!empty($listSoikeo)) foreach ($listSoikeo as $item) : ?>
                <div class="col-5 p-2">
                    <?php echo getThumbnail($item, 135, 75, "img-fluid") ?>
                </div>
                <div class="col-7 p-2">
                    <div class="font-15">
                        <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>" class="text-black2 max-line-2"><?php echo $item->title ?></a>
                    </div>
                    <i class="ic-clock"></i>
                    <span class="text-secondary font-12"><?php echo timeAgo($item->displayed_time, "d/m/Y") ?></span>
                </div>
            <?php endforeach; ?>
        </div>
    </div>


    <?php if ($this->_controller != 'home') : $listMostViewed = getDataPost([
            'limit' => 5,
            'order' => ['displayed_time' => 'DESC']
        ]) ?>
        <div class="my-3 pb-3">
            <div class="title-mix text-uppercase font-weight-bold font-16 py-2">tin xem nhiều</div>

            <div id="demo" class="carousel slide tin-xem-nhieu-slider" data-ride="carousel">

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <?php if (!empty($listSoikeo)) foreach ($listSoikeo as $key => $item) : ?>
                            <div class="my-3 py-3 border-bottom-dashed text-center">
                                <!--                        <a href="--><?php //echo getUrlPost($item) 
                                                                        ?>
                                <!--" title="--><?php //echo $item->title 
                                                ?>
                                <!--" class="input-comment text-uppercase text-white py-2 px-3 d-inline-block">--><?php //echo $item->title 
                                                                                                                    ?>
                                <!--</a>-->
                                <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                    <h3 class="font-weight-bold font-15 max-line-2 my-3"><?php echo $item->title ?></h3>
                                </a>
                                <p class="font-14 text-secondary mb-0"><?php echo timeAgo($item->displayed_time, "d/m/Y") ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>

                <!--<a class="carousel-control-prev" href="#demo" data-slide="prev">
                <i class="fas fa-chevron-left text-danger"></i>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
                <i class="fas fa-chevron-right text-danger"></i>
            </a>-->
            </div>
        </div>
    <?php endif ?>
    <div class="position-sticky" style="top: 45px">
        <?php echo showContainerBanner('sidebar_middle', 0, '') ?>
        <iframe id="chatbox" src="https://www5.cbox.ws/box/?boxid=927201&boxtag=3jw66g" width="100%" height="500" allowtransparency="yes" allow="autoplay" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto"></iframe>
        <?php echo showContainerBanner('sidebar_bottom', 0, '') ?>
    </div>

</aside>
<!--        sidebar-->