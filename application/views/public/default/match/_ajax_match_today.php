<?php
if(!empty($data_match)){
    $data_match_group = array_group_by($data_match, function ($i) {
        return $i['tournament_id'];
    });
}

if (!empty($list_tournament)) foreach ($list_tournament as $item){
    $data_tournament[$item->tournament_id] = $item->title;
}
?>

<?php if(!empty($data_match_group)) foreach ($data_match_group as $tournament_id => $matches):?>
<div class="<?php echo $tournament_id == 17 ? "d-block" : "d-none" ?>">
    <div class="bg-main py-2 px-3 text-center text-white text-capitalize">
        <?php echo !empty($data_tournament[$tournament_id]) ? $data_tournament[$tournament_id] : '' ?>
    </div>
    <div class="overflow-auto">
        <table class="table text-nowrap">
            <tbody>
            <?php $this->load->view(TEMPLATE_PATH . "match/_data_match_item", ['data_match' => $matches]) ?>
            </tbody>
        </table>
    </div>
</div>
<?php endforeach; ?>