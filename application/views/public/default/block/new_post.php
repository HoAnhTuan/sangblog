<?php
$new_post = getDataPost([
    'type' => 'post',
    'is_status' => 1,
    'limit' => 10,
    'order' => ['displayed_time' => 'DESC']
]);
?>
<?php if (!empty($new_post)) : ?>
    <div class="nhandinh-soikeo-right">
        <a href="javascript:void(0);">Mới Cập Nhật</a>
    </div>
    <div class="widget-posts-list-container timeline-widget">
        <ul class="posts-list-items widget-posts-wrapper">
            <?php foreach ($new_post as $key => $item) : ?>
                <li class="widget-single-post-item">
                    <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                        <span class="date meta-item tie-icon"><?php echo date('d/m/Y', strtotime($item->displayed_time)) ?></span>
                        <h3 class="max-line-1"><?php echo $item->title ?></h3>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif ?>