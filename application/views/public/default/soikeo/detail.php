<?php if (!empty($oneItem)) :; ?>
	<main class="main-detail" data-url="<?php echo getUrlPost($oneItem) ?>">
		<div class="container">
			<div class="row">
				<?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
				<article class="col-12 col-lg-9 border-radius">
					<div class="bg-gray post-content p-2">
						<h1 class="font-23 normal-title">
							<?php echo $oneItem->title; ?>
						</h1>
						<div class="d-flex flex-wrap justify-content-between my-3">
							<div class="font-13 text-secondary">
								<span class="mr-3">
									<i class="far fa-clock mr-1"></i>
									<?php echo timeAgo($oneItem->displayed_time, "H:i") ?> <?php echo date_post_vn($oneItem->displayed_time) ?>
								</span>

								<?php if (!empty($category)) : ?>
									<a class="text-secondary" href="<?= base_url("$category->slug.html") ?>"><i class="far fa-folder-open mr-1"></i><?= $category->title ?></a>
								<?php endif; ?>
							</div>
						</div>
						<?php if (!empty($data_match)) : ?>
							<div class="list-link">
								<a class="vip" href="<?php echo getUrlMatch($data_match) ?>">
									<i class="fas fa-angle-double-right mr-1"></i>
									Thông tin trận đấu <?php echo $data_match->name_home . ' và ' . $data_match->name_away ?></a>
							</div>
						<?php endif; ?>
						<div class="font-14 font-weight-bold">
							<?php echo seoCustom($oneItem->description) ?>
						</div>
						<div class="my-3 border-top border-bottom border-radius">
							<?php
							if (!empty($data_match)) :
							?>
								<div class="d-flex block-keo flex-wrap text-center bg-white">
									<strong class="w-100 time mt-1 mb-2">
										<?php echo timeAgo($oneItem->start_time, "d/m") . ' - ' . timeAgo($oneItem->start_time, "H:i") ?>
										-
										<a href="javascript:;" title="<?php echo $data_match->league_name ?>" class="name-keo text-danger text-uppercase"><?php echo $data_match->league_name ?></a>
									</strong>

									<div class="w-25 d-flex flex-wrap align-content-center p-2 p-md-0">
										<?php echo getLogoClub($data_match->home_id, $data_match->name_home, "img-fluid d-block mx-auto", 80, 80) ?>
										<span class="text-black2 line-2 w-100 bold"><?php echo $data_match->name_home ?></span>
									</div>
									<div class="w-50">
										<?php
										if (!empty($data_bets) && (is_object($data_bets))) :
											$chau_au = array_values((array)$data_bets->chau_au);
											$chau_a = array_values((array)$data_bets->chau_a);
											$tai_xiu = array_values((array)$data_bets->tai_xiu);
										?>
											<div class="rate-wrap">
												<p class="mb-0 d-flex">
													<span>
														Châu Âu
													</span>
													<span>
														<?php echo $chau_au[0] ?>/<?php echo $chau_au[1] ?>/<?php echo $chau_au[2] ?>
													</span>
												</p>
												<p class="mb-0 d-flex">
													<span>
														Châu Á
													</span>
													<span>
														<?php echo $chau_a[0] ?>*<?php echo $chau_a[1] ?>*<?php echo $chau_a[2] ?>
													</span>
												</p>
												<p class="mb-0 d-flex">
													<span>
														Tài xỉu
													</span>
													<span>
														<?php echo $tai_xiu[0] ?>*<?php echo $tai_xiu[1] ?>*<?php echo $tai_xiu[2] ?>
													</span>
												</p>
											</div>
										<?php else : ?>
											<div>
												<span class="font-weight-bold">Đang cập nhật!</span>
											</div>
										<?php endif; ?>
									</div>
									<div class="w-25 d-flex flex-wrap align-content-center p-2 p-md-0">
										<?php echo getLogoClub($data_match->away_id, $data_match->name_away, "img-fluid d-block mx-auto", 80, 80) ?>
										<span class="text-black2 line-2 w-100 bold"><?php echo $data_match->name_away ?></span>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<!-- <div class="banner-post my-2">
						</div> -->
						<div class="list-link">
							<?php if (!empty($related_post)) {
								foreach ($related_post as $item) : ?>
									<a class="d-block" href="<?php echo getUrlPost($item) ?>">
										<i class="far fa-check-circle mr-1"></i><?php echo $item->title ?></a>
							<?php endforeach;
							} ?>
						</div>
						<div class="content-news text-justify">
							<?php
							echo getTableOfContent($oneItem->content, $oneItem->title);
							?>
						</div>
						<?php if (!empty($data_info->data_statistic)) : ?>
							<div class="tab-content tab_static">
								<div role="tabpanel" class="tab-pane active post mb-0 pb-0 " id="content">
									<ul class="nav nav-tabs no-border">
										<li class="nav-item"><a class="nav-link no-border max-height no-radius active show" data-toggle="tab" href="#menu" rel="nofollow">So sánh kèo</a></li>
										<li class="nav-item"><a class="nav-link no-border max-height no-radius" data-toggle="tab" href="#menu1" rel="nofollow">Thống kê kèo</a></li>
										<li class="nav-item"><a class="nav-link no-border max-height no-radius" data-toggle="tab" href="#menu2" rel="nofollow">Thành tích đối đầu</a></li>
										<li class="nav-item"><a class="nav-link no-border max-height no-radius" data-toggle="tab" href="#menu3" rel="nofollow">Phong độ gần đây</a></li>
									</ul>
								</div>
							</div>
							<div class="tab-content tab_static_content">
								<div id="menu" class="tab-pane fade in active show">
									<div class="thong_ke_gd">
										<?php if (!empty($data_info->data_compare)) echo $data_info->data_compare; ?>
									</div>
								</div>
								<div id="menu1" class="tab-pane fade">
									<div class="ttddd dragScroll_sk font-12">
										<?php if (!empty($data_info->data_statistic)) echo $data_info->data_statistic; ?>
									</div>
								</div>
								<div id="menu2" class="tab-pane fade">
									<div class="rank dragScrol2_sk font-12">
										<?php if (!empty($data_info->data_h2h)) echo $data_info->data_h2h; ?>
									</div>
								</div>
								<div id="menu3" class="tab-pane fade">
									<?php if (!empty($data_info->data_form)) echo $data_info->data_form; ?>
								</div>
							</div>
						<?php endif; ?>

						<?php if (!empty($data_tag)) : ?>
							<div class="list-tag ">
								<span class="tag-title">
									<i class="fas fa-tags mr-1 p-0"></i>
								</span>
								<?php foreach ($data_tag as $item) : ?>
									<a href="<?php echo getUrlTag($item) ?>" title="<?php echo $item->title ?>" class=""><?php echo $item->title ?></a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>


						<div class="btns">
							<a title="<?php echo $prevPost->title ?? ''; ?>" href="<?php echo !empty($prevPost) ? getUrlPost($prevPost) : ''; ?>" class="prev">
								<i class="mr-2 fas fa-chevron-left"></i>
								Bài trước
							</a>
							<a title="<?php echo $nextPost->title ?? ''; ?>" href="<?php echo !empty($nextPost) ? getUrlPost($nextPost) : ''; ?>" class="next">Xem tiếp
								<i class="fas fa-chevron-right ml-2"></i>
							</a>
						</div>
						<?php $this->load->view(TEMPLATE_PATH . '/block/author') ?>
					</div>
					<?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer') ?>

				</article>
				<div class="col-12 col-lg-3">
					<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_news') ?>
					<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
				</div>
			</div>
		</div>
	</main>
<?php endif; ?>