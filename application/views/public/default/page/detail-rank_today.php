<?php if (!empty($oneItem)): ?>
    <main class=" main-detail"  data-url="<?php echo getUrlPost($oneItem) ?>">
      <div class="container">
	      <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
            <div class="col-12 col-lg-9">
                <?php $this->load->view(TEMPLATE_PATH . "block/table-bxh") ?>
	              <?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer.php') ?>
            </div>
	        <div class="col-12 mt-70 col-lg-3">
              <?php $this->load->view(TEMPLATE_PATH . 'block/nav_sidebar') ?>
              <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
	        </div>
        </div>
      </div>
        <?php if(!empty($oneItem->content)) $this->load->view(TEMPLATE_PATH . 'block/_content_footer', ['content' => $oneItem->content, 'des' => $oneItem->description]) ?>
    </main>
<?php endif; ?>
