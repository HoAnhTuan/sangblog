<?php
$keoxien = getDataPost([
    'category_id' => 31,
    'is_status' => 1,
    'limit' => 1,
    'order' => ['displayed_time' => 'DESC']
]);
$keophatgoc = getDataPost([
    'category_id' => 34,
    'is_status' => 1,
    'limit' => 1,
    'order' => ['displayed_time' => 'DESC']
]);
$keotaixiu = getDataPost([
    'category_id' => 35,
    'is_status' => 1,
    'limit' => 1,
    'order' => ['displayed_time' => 'DESC']
]);

$new_post = array_merge($keophatgoc, $keoxien, $keotaixiu);

$nhan_dinh = getDataPost([
    'category_id' => 1,
    'is_status' => 1,
    'limit' => 3,
    'is_today' => true,
    'show_user' => true,
    'order' => ['displayed_time' => 'DESC']
]);
if (empty($nhan_dinh)) {
    $nhan_dinh = getDataPost([
        'category_id' => 1,
        'is_status' => 1,
        'limit' => 3,
        'show_user' => true,
        'order' => ['displayed_time' => 'DESC']
    ]);
}

$not_id = array();
foreach ($nhan_dinh as $item) {
    array_push($not_id, $item->id);
}

$phan_tich_keo_temp = getDataPost([
    'category_id' => 1,
    'is_status' => 1,
    'post__not_in' => $not_id,
    'limit' => 6,
    'is_today' => true,
    'order' => ['displayed_time' => 'DESC']
]);
if (empty($phan_tich_keo_temp)) {
    $phan_tich_keo_temp = getDataPost([
        'category_id' => 1,
        'is_status' => 1,
        'post__not_in' => $not_id,
        'limit' => 6,
        'order' => ['displayed_time' => 'DESC']
    ]);
}
$phan_tich_keo = array_chunk($phan_tich_keo_temp, 2);
?>
<!-- Kèo Xiên -->
<div class="sidebar-soikeo">
    <?php foreach ($new_post as $item) :  ?>
        <div class="sidebar-soikeo-block">
            <a href="<?php echo getUrlPost($item) ?>" title="<?= $item->title ?>">
                <?php echo getThumbnail($item, 320, 180, "w-100 img-fluid") ?>
            </a>
            <div class="home-block-post swiper-bottom">
                <?php if (!empty($item->category_id)) : ?>
                    <div class="home-block-post-top">
                        <a href="<?php echo BASE_URL . "$item->category_slug" . '.html' ?>" class="post-category"><?php echo $item->category_title ?></a>
                    </div>
                <?php endif; ?>
                <div class="home-block-post-bottom">
                    <a href="<?php echo getUrlPost($item) ?>" title="<?= $item->title ?>">
                        <?= $item->title ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?php if ($this->_controller === 'home') : ?>

    <div class="sidebar-swiper">
        <?php if (!empty($nhan_dinh)) : ?>
            <div class="swiper mySwiper1">
                <h4 class="sidebar-swiper-title">CHUYÊN GIA KEONHACAI NHẬN ĐỊNH</h4>
                <hr>
                <div class="swiper-wrapper">
                    <?php foreach ($nhan_dinh as $item) : ?>
                        <div class="swiper-slide">
                            <a href="<?php echo getUrlPost($item) ?>">
                                <p class="font-weight-normal font-16"><?php echo $item->title ?? '' ?></p>
                                <p class="text-secondary font-weight-normal">Chuyên gia: <?php echo $item->username ?? '' ?></p>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination-1"></div>
            </div>
        <?php endif; ?>
    </div>

    <div class="sidebar-swiper">
        <?php if (!empty($phan_tich_keo)) : $stt = 1 ?>
            <div class="swiper mySwiper2">
                <h4 class="sidebar-swiper-title">PHÂN TÍCH KEO NHA CAI HÔM NAY</h4>
                <hr>
                <div class="swiper-wrapper">
                    <?php foreach ($phan_tich_keo as $item) : ?>
                        <div class="swiper-slide">
                            <?php foreach ($item as $itemchild) : ?>
                                <div class="d-flex">
                                    <span class="p-3 font-16 font-weight-bold"><?php echo '0' . $stt;
                                                                                $stt++; ?></span>
                                    <a href="<?php echo getUrlPost($itemchild) ?>">
                                        <p class="text-left font-weight-normal font-16 max-line-2"><?php echo  $itemchild->title ?? ''; ?></p>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination-2"></div>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>