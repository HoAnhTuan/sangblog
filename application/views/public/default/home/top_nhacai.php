<?php if (!empty($nha_cai)) : ?>

	<section class="section mt-70">
		<section class="section-header category-name">
			<h2 class="category-title">Top 10 nhà cái 2022</h2>
		</section>
		<section class="section-body nhacai-container">
			<?php foreach ($nha_cai as $key => $item) :
				$data_dealer = json_decode($item->data_dealer);
				if (!empty($data_dealer)) : ?>
					<div class="home-match-item-nhacai">
						<h3 class="title"><?php echo $data_dealer->title ?></h3>
						<div class="rank"><?php echo $key + 1 ?></div>
						<div class="row header no-gutters border-white-bottom">
							<div class="col-6 order-1 border-white-right-lg col-md-4">
								<div class=" pl-5 logo-broker w-auto">
									<?php echo getThumbnail($data_dealer, '100', '70', 'img-fluid') ?>
								</div>
							</div>
							<div class="col-6 order-2 border-white-right-lg col-md-2 text-center ">
								<p class="bonus mb-0">KHUYẾN MÃI</p>
								<p class="des mb-0">Thưởng <strong><?php echo $data_dealer->odds ?? 100 ?>%</strong> lên tới:</p>
								<p class="prize mb-0"><?php echo number_format((int)$data_dealer->bonus ?? 0, 0, '', '.') ?>&nbsp;đ</p>
							</div>
							<div class="col-6 border-white-top-lg  order-3  order-md-7 row  no-gutters">
								<div class="col-md-6 mark-wrap border-white-right-lg flex-column flex-md-row text-center">
									<p class="title">Điểm</p>
									<span class="mark"><?php echo $data_dealer->reliability ?? 0 ?></span>
								</div>
								<div class="col-md-6 vote flex-column flex-md-row text-center">
									<p class="mb-0 title">Bình chọn</p>
									<div class="stars">
										<?php echo renderStarRating($data_dealer->reliability ?? 0) ?>
									</div>
								</div>
							</div>
							<div class="col-6 order-4 border-white-right-lg col-md-3 feature ">
								<?php
								$danhgia = explode("|", $data_dealer->promote);
								for ($i = 0; $i < 3; $i++) : ?>
									<p>
										<i class="fas fa-check-circle"></i>
										<span><?php echo $danhgia[$i] ?? 'Đang cập nhật'; ?></span>
									</p>
								<?php endfor; ?>
							</div>
							<div class="col-12 order-5 col-md-3 btns">
								<p>
									<a rel="nofollow" class="btn-view" href="<?php echo $data_dealer->link_bet ?? '#' ?>">Đăng ký</a>
								</p>
								<p>
									<a class="btn-bet" href="<?php echo getUrlPost($item) ?>" rel="nofollow" target="_blank">Đánh giá</a>
								</p>
							</div>
							<div class="col-12 order-6 col-md-6 border-white-top-lg   payment flex-column flex-md-row text-center">
								<p class="title">Cổng thanh toán</p>
								<div class="logos-group">
									<div class="logo momo"></div>
									<div class="logo viettel"></div>
									<div class="logo bank"></div>
									<div class="logo zalo"></div>
									<div class="logo go"></div>
								</div>
							</div>

						</div>
					</div>
			<?php endif;
			endforeach; ?>
		</section>
	</section>
<?php endif; ?>