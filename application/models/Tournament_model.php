<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tournament_model extends STEVEN_Model {
    public $table;

    public function __construct()
    {
        parent::__construct();
        $this->table = "tournament";
        $this->column_order = array("$this->table.id", "$this->table.id", "title", "$this->table.is_status", "$this->table.created_time", "$this->table.updated_time");
        $this->column_search = array('title','country');
        $this->order_default = array("$this->table.created_time" => 'desc');
    }

    public function _where_custom($args = array())
    {
        parent::_where_custom();
        extract($args);
    }
    public function getByTourId($id,$updateCache = false){
        $key = "getByTourId{$id}";
        $data = $this->getCache($key);
        if($data === false || $updateCache == true){
            $this->db->select('season_id, name, title, tournament_id');
            $this->db->where('tournament_id',$id);
            $this->db->from($this->table);
            $data = $this->db->get()->row();
            $this->setCache($key,$data,60*60);
        }
        return $data;
    }

    public function getSelect2Tourament($id){
        $this->db->select("CONCAT(st_$this->table.title,' - ',st_$this->table.country) AS text, $this->table.tournament_id AS id");
        $this->db->from($this->table);
        $this->db->join("category", "$this->table.tournament_id = category.tournament_id");
        $this->db->where($this->table . ".tournament_id", $id);
        return $this->db->get()->result();

    }

    public function getDataFeatured($updateCache = false){
        $key = "{$this->table}_getDataFeatured";
        $data = $this->getCache($key);
        if($data === false || $updateCache == true){
            $this->db->where('is_featured',true);
            $this->db->from($this->table);
            $this->db->order_by('order',"ASC");
            $data = $this->db->get()->result();
            $this->setCache($key,$data,60*60);
        }
        return $data;
    }

    public function getByCountryId($id) {
        $params = [
            'country_id' => $id,
        ];
        $result = $this->getDataApi('country/get_country',$params);
        return $result;
    }


    //Lịch thi đấu các trận sắp đá, chỉ tính từ thời gian hiện tại
    public function getScheduleToday (string $tournament_id = '', string $date_time = '', $timeCache = 120, $update_cache = false){
        $keyCache = $this->table . "_getScheduleToday_" . md5(json_encode($tournament_id).$date_time);
        $data = $this->getCache($keyCache);
        if ($data === false || $update_cache == true) {
            $data_json = callCURL(API_DATACENTER . "api/v1/match/get_schedule?tournament_id=". $tournament_id .'&date=' .$date_time);
            $data_json = json_decode($data_json);

            if(!empty($data_json->data))
                $listMatch = $data_json->data->data;
            if (!empty($listMatch)) foreach ($listMatch as $item){
                $listResult[] = (array)$item;
            }
            if(!empty($listResult))
                $data = array_group_by($listResult, function ($i) {
                    return $i['tournament_id'];
                });
            $this->setCache($keyCache, $data,$timeCache);
        }
        return $data;
    }

    //Lịch thi đấu của cả mùa giải
    public function getMatch (string $tournamentId, string $dateTime= '', $timeCache = 30, $updateCache = false) {

        $keyCache = $this->table . "_getMatch_" . md5(json_encode($tournamentId).$dateTime);
        $data = $this->getCache($keyCache);
        if ($data === false || $updateCache == true) {
            $data_json = callCURL(API_DATACENTER . "api/v1/match/get_match?tournament_id=". $tournamentId .'&date=' .$dateTime);
            $data_json = json_decode($data_json);
            if(!empty($data_json->data))
                $listMatch = $data_json->data->data;
            if (!empty($listMatch)) foreach ($listMatch as $item){
                $listResult[] = (array)$item;
            }
            if(!empty($listResult))
                $data = array_group_by($listResult, function ($i) {
                    return $i['tournament_id'];
                });
            $this->setCache($keyCache, $data,$timeCache);
        }
        return $data;
    }

    public function get_schedule_tournament($tournament_id, $season_id = null,$updateCache = false){
        $key = "get_schedule_tournament_{$tournament_id}_{$season_id}";
        $data = $this->getCache($key);
        if($data === false || $updateCache == true){
            $param = [
                'tournament_id' => $tournament_id,
                'season_id' => $season_id
            ];
            $data = $this->getDataApi('match/get_schedule_tournament', $param);
            $this->setCache($key,$data,60*60);
        }
        return $data;
    }

    public function get_by_id_api($tournament_id, $updateCache = false){
        $key = "get_by_id_api_{$tournament_id}";
        $data = $this->getCache($key);
        if($data === false || $updateCache == true){
            $param = [
                'tournament_id' => $tournament_id,
            ];
            $data = $this->getDataApi('tournament/get_by_id', $param);
            $data = !empty($data->data) ? $data->data : NULL;
            $this->setCache($key,$data,60*60);
        }
        return $data;
    }

}