<?php if (!empty($oneItem)) :
    $data_match_group = [];
    $current_round = 1;
    if (!empty($data_tournament)) {
        $data_rank = json_decode($data_tournament->data_ranks);
        if (!empty($data_rank)) $current_round = $data_rank[0]->currentround;
        if (!empty($data_match)) foreach ($data_match as $key => $item) {
            $data_match_group[$item->round][date('Y-m-d', $item->start_timestamp)][$key] = $item;
        }
    }
?>
    <main class=" main-detail" data-url="<?php echo getUrlPost($oneItem) ?>">
        <div class="container">
            <div class="row">
                <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
                <div class="col-12 col-lg-9">
                    <div class="section-body">
                        <div class="d-flex flex-wrap justify-content-between">
                            <h1 class="font-23"><?php echo $oneItem->title ?></h1>

                            <?php $this->load->view(TEMPLATE_PATH . "block/rate") ?>
                        </div>
                        <div class="font-13 text-secondary"><?php echo date_post_vn($oneItem->updated_time) ?> - <?php echo timeAgo($oneItem->updated_time, "H:i") ?></div>

                        <div class="font-14 font-weight-bold"><?php echo $oneItem->description ?></div>

                        <?php if (!empty($data_match_group)) : ?>
                            <div class="d-flex my-3 align-items-center bg-dark text-white text-center slide-ltd" id="tbl-result" data-current-round="<?php echo $current_round ?>">
                                <div class="swiper-button-prev ml-3 rounded-circle"></div>
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        <?php
                                        foreach ($data_match_group as $key => $item) :
                                        ?>
                                            <div class="swiper-slide p-3">
                                                <p class="mb-0">Vòng</p>
                                                <span data-click-round="<?php echo (int)$key; ?>"><?php echo (int)$key ?></span>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="swiper-button-next mr-3 rounded-circle "></div>
                            </div>

                            <div class="ajax-content">
                                <?php
                                foreach ($data_match_group as $key => $item) :
                                    foreach ($item as $time => $matches) :
                                ?>
                                        <div class="item-round table-result-bongda" data-round="<?php echo (int)$key; ?>">
                                            <div class="mb-0 table-heading">
                                                <?php echo getTitleRoundTournament($time) ?>
                                            </div>
                                            <div class="overflow-auto">
                                                <table class="table table-striped">
                                                    <tbody>
                                                        <?php $this->load->view(TEMPLATE_PATH . "match/_data_match_item", ['data_match' => $matches]) ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                <?php endforeach;
                                endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="content-news text-justify">
                        <?php echo $oneItem->content ?>
                    </div>
                    <?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer') ?>
                </div>
                <div class="col-12 col-lg-3">
                    <?php $this->load->view(TEMPLATE_PATH . 'block/nav_sidebar') ?>
                    <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
                </div>
            </div>
        </div>
    </main>
<?php endif; ?>