<?php
    $post_most_view = getDataPost([
        'limit' => 5,
    ]);
    if (!empty($post_most_view)) : ?>
    <div class="widget widget-post__most__viewed">
        <p class="widget-title">
            Mẹo chơi kèo
        </p>
        <ul class="post-most__viewed_style_1">
            <?php foreach ($post_most_view as $key => $post_item) : ?>
            <li class="post-item">
                <div class="post-info">
                    <h4>
                        <a class="text-white" href="<?php echo getUrlPost($post_item) ?>">
                            <?php echo $post_item->title ?>
                        </a>
                        <span> - <i class="far fa-clock"></i>  <?php echo date('d/m/Y', strtotime($post_item->displayed_time)) ?></span>
                    </h4>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>