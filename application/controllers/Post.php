<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post extends Public_Controller
{
    protected $_post;
    protected $_all_category;
    protected $_category;
    protected $_user;
    protected $_reviews;
    protected $_match;
    public function __construct()
    {
        parent::__construct();

        $this->load->model(['post_model', 'category_model', 'users_model', 'reviews_model', 'match_model']);
        $this->_post = new Post_model();
        $this->_category = new Category_model();
        $this->_user = new Users_model();
        $this->_reviews = new Reviews_model();
        $this->_match = new Match_model();
        $this->_all_category = $this->_category->_all_category();
    }

    public function detail($slug, $id)
    {
        $this->setCacheFile(15);
        $oneItem = $this->_post->getByIdCached($id);
        if (empty($oneItem)) show_404();
        $urlNormal = getUrlPost($oneItem);
        if ($oneItem->slug !== $slug) redirect($urlNormal, "location", "301");

        switch ($oneItem->type) {
            case 'soikeo':
                $data = $this->detail_soikeo($oneItem);
                $layoutView = '';
                if (!empty($oneItem->layout)) $layoutView = '-' . $oneItem->layout;
                $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/detail' . $layoutView, $data, TRUE);
                break;

            case 'gamebai':
            case 'nhacai':
            case 'banca':
                $data = $this->detail_toplist($oneItem);
                $layoutView = '';
                if (!empty($oneItem->layout)) $layoutView = '-' . $oneItem->layout;
                $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/detail' . $layoutView, $data, TRUE);
                break;

            default:
                $data = $this->detail_post($oneItem);
                $data['main_content'] = $this->load->view(TEMPLATE_PATH . $oneItem->type . '/detail', $data, TRUE);
        }
        $this->load->view(TEMPLATE_MAIN, $data);
    }

    private function detail_soikeo($oneItem)
    {
        if ($oneItem->is_status == 0) show_404();
        $id = $oneItem->id;
        $data['oneItem'] = $oneItem;
        $data['category'] = $oneCategory = $this->_category->getCatByPostId($id);
        $this->_category->_recursive_child_id($this->_all_category, $oneCategory->id);
        $listChildId = $this->_category->_list_category_child_id;
        $data['related_post'] = $this->_post->getDataFE([
            'select' => 'st_post.title, st_post.slug, st_post.id',
            'is_status' => 1,
            'in_date' => $oneItem->displayed_time,
            'category_id' => $listChildId,
            'post__not_in' => [$id],
            'limit' => 4
        ]);

        $data['prevPost'] = $this->_post->getPrevById($id, 'id,slug,title');
        $data['nextPost'] = $this->_post->getNextById($id, 'id,slug,title');

        $data['reviews'] = $this->_reviews->getRate([
            'url' => getUrlPost($oneItem)
        ]);
        $data['data_tag'] = $this->_category->getTagByPostId($id);
        $data['data_bets'] = json_decode($oneItem->data_bets);
        $data['data_match'] = !empty($oneItem->data_match) ? json_decode($oneItem->data_match) : '';
        $data['data_info'] = $this->_match->getMatchOdds($oneItem->match_id)[0];

        $this->breadcrumbs->push('Home', base_url());
        $this->breadcrumbs->push($oneCategory->title, getUrlCategory($oneCategory));
        $this->breadcrumbs->push($oneItem->title, getUrlPost($oneItem));
        $data['breadcrumb'] = $this->breadcrumbs->show();
        if (!empty($oneItem->author_id)) $data['user'] = $this->_category->getById($oneItem->author_id);

        $data['SEO'] = [
            'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : $oneItem->title,
            'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : $oneItem->description,
            'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
            'url' => getUrlPost($oneItem),
            'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
            'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
        ];
        return $data;
    }

    private function detail_toplist($oneItem)
    {
        if ($oneItem->is_status == 0) show_404();
        $id = $oneItem->id;
        $data['category'] = $oneCategory = $this->_category->getCatByPostId($id);
        $this->_category->_recursive_child_id($this->_all_category, $oneCategory->id);
        $listChildId = $this->_category->_list_category_child_id;
        $data['recent_post'] = $this->_post->getDataFE([
            'is_status' => 1,
            'limit' => 6,
        ]);
        $data['related_post'] = $this->_post->getDataFE([
            'is_status' => 1,
            'post__not_in' => [$id],
            'category_id' => $listChildId,
            'limit' => 6
        ]);
        $data['reviews'] = $this->_reviews->getRate([
            'url' => getUrlPost($oneItem)
        ]);

        if (!empty($oneItem->author_id)) $data['user'] = $this->_category->getById($oneItem->author_id);


        $this->breadcrumbs->push('Home', base_url());
        $this->breadcrumbs->push($oneCategory->title, getUrlCategory($oneCategory));
        $this->breadcrumbs->push($oneItem->title, getUrlPost($oneItem));
        $data['breadcrumb'] = $this->breadcrumbs->show();

        $data['SEO'] = [
            'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : $oneItem->title,
            'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : $oneItem->description,
            'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
            'url' => getUrlPost($oneItem),
            'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
            'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
        ];

        $data['oneItem'] = $oneItem;
        return $data;
    }

    private function detail_post($oneItem)
    {
        if ($oneItem->is_status == 0) show_404();
        $id = $oneItem->id;
        $data['category'] = $oneCategory = $this->_category->getCatByPostId($id);
        if (!empty($oneCategory)) {
            $this->_category->_recursive_child_id($this->_all_category, $oneCategory->id);
            $listChildId = $this->_category->_list_category_child_id;
        }

        $data['recent_post'] = $this->_post->getDataFE([
            'is_status' => 1,
            'limit' => 6,
        ]);
        $data['related_post'] = $this->_post->getDataFE([
            'is_status' => 1,
            // 'in_date' => $oneItem->displayed_time,
            'category_id' => $listChildId ?? '',
            'post__not_in' => [$id],
            'limit' => 4,
        ]);
        $data['prevPost'] = $this->_post->getPrevById($id, 'id,slug,title');
        $data['nextPost'] = $this->_post->getNextById($id, 'id,slug,title');
        $data['reviews'] = $this->_reviews->getRate([
            'url' => getUrlPost($oneItem)
        ]);
        $data['data_tag'] = $this->_category->getTagByPostId($id);
        if (!empty($oneItem->author_id)) $data['user'] = $this->_category->getById($oneItem->author_id);

        $this->breadcrumbs->push('Home', base_url());
        if (!empty($oneCategory)) {
            $this->breadcrumbs->push($oneCategory->title, getUrlCategory($oneCategory));
        }
        $this->breadcrumbs->push($oneItem->title, getUrlPost($oneItem));
        $data['breadcrumb'] = $this->breadcrumbs->show();

        $data['SEO'] = [
            'meta_title' => !empty($oneItem->meta_title) ? $oneItem->meta_title : $oneItem->title,
            'meta_description' => !empty($oneItem->meta_description) ? $oneItem->meta_description : $oneItem->description,
            'meta_keyword' => !empty($oneItem->meta_keyword) ? $oneItem->meta_keyword : '',
            'url' => getUrlPost($oneItem),
            'is_robot' => !empty($oneItem->is_robot) ? $oneItem->is_robot : '',
            'image' => !empty($oneItem->thumbnail) ? getImageThumb($oneItem->thumbnail, 470, 246) : '',
        ];

        $data['oneItem'] = $oneItem;
        return $data;
    }
}
