<?php
/**
 * Developer: linhth
 * Controller bài viết
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Player extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function watch($hash, $matchId, $linkIndex){
        if($hash !== md5(date('Y-m-d H'))) show_404();
        $this->load->model(['match_model']);
        $matchModel = new Match_model();
        $oneItem = $matchModel->getById($matchId);
        if(empty($oneItem)) show_404();
        $data_link = explode("\n",$oneItem->data_link);
        if(!empty($data_link[$linkIndex])){
            $oneLink = $data_link[$linkIndex];
            $this->jwplayer($oneLink);
        }else{
            $this->load->view(TEMPLATE_PATH . 'player/index', ['link' => "http://intro.steamaz.co/clip.mp4"]);
        }
    }

    public function watch_other($hash, $matchId, $linkIndex){
        if($hash !== md5(date('Y-m-d H'))) show_404();
        $this->load->model(['match_model']);
        $matchModel = new Match_model();
        $oneItem = $matchModel->getById($matchId);
        if(empty($oneItem)) show_404();
        $data_link = explode("\n",$oneItem->data_link_wb);
        if(!empty($data_link[$linkIndex])){
            $oneLink = $data_link[$linkIndex];
            $this->load->view(TEMPLATE_PATH . 'player/index', ['data_link' => (object) ['url_video' => $oneLink]]);
        }else{
            $this->load->view(TEMPLATE_PATH . 'player/index', ['link' => "http://intro.steamaz.co/clip.mp4"]);
        }
    }

    private function watch_vcdn($channel){
        if(!empty($channel)){
            $domain = "oulh3luu2f.vcdn.com.vn";
            $epoc_timestamp = round(microtime(true) * 1000);
            $md5_digest = md5("vuivetivi@123/{$channel}{$epoc_timestamp}{$domain}");
            $link_full = "//oulh3luu2f.vcdn.com.vn/hls/$md5_digest/$epoc_timestamp/$channel/index.m3u8";
        } else{
            $link_full = "";
        }
        return $this->jwplayer($link_full);
    }

    private function watch_vega($channel){
        if(!empty($channel)){
            $channel = trim($channel);
            $live_secret = '0915224935'; // khóa bí mật cấu hình trên VegaCDN Portal
            $live_time = time() + 12 * 60 * 60; // khoảng thời gian link hết hạn
            $live_uri = "/live/_definst_/$channel/playlist.m3u8";
            $live_mini_uri = "/live/_definst_/$channel";
            $live_host = 'https://86305d128a.vws.vegacdn.vn';
            $live_ip = $_SERVER['REMOTE_ADDR'];
            $live_str="$live_secret$live_mini_uri$live_time";
            $live_str_ip="$live_secret$live_ip$live_mini_uri$live_time";
            $live_hotkey_noip = sha1($live_str);
            $live_hotkey = sha1($live_str_ip);
            $link_full = $live_host . $live_uri . '?hotkey=' . $live_hotkey_noip . '&time=' . $live_time;
        } else{
            $link_full = "";
        }
        return $this->jwplayer($link_full);
    }

    public function watch_video($hash, $postId){
        if($hash !== md5(date('Y-m-d H'))) show_404();
        $this->load->model('post_model');
        $postModel = new Post_model();
        $oneItem = $postModel->getById($postId);
        if(empty($oneItem)) show_404();
        if(!empty($oneItem->video))
            $this->jwplayer($oneItem->video);
    }

    public function ads_vast(){
        // create document
        $factory = new \Sokil\Vast\Factory();
        $document = $factory->create('2.0');
// insert Ad section
        $ad1 = $document
            ->createInLineAdSection()
            ->setId('ad1')
            ->setAdSystem('Ad Server Name')
            ->setAdTitle('Ad Title')
            ->addImpression('http://ad.server.com/impression', 'imp1');

// create creative for ad section
        $linearCreative = $ad1
            ->createLinearCreative()
            ->setDuration(128)
            ->setVideoClicksClickThrough('http://entertainmentserver.com/landing')
            ->addVideoClicksClickTracking('http://ad.server.com/videoclicks/clicktracking')
            ->addVideoClicksCustomClick('http://ad.server.com/videoclicks/customclick')
            ->addTrackingEvent('start', 'http://ad.server.com/trackingevent/start')
            ->addTrackingEvent('pause', 'http://ad.server.com/trackingevent/stop');

// add 100x100 media file
        $linearCreative
            ->createMediaFile()
            ->setProgressiveDelivery()
            ->setType('video/mp4')
            ->setHeight(100)
            ->setWidth(100)
            ->setBitrate(2500)
            ->setUrl('http://server.com/media1.mp4');

// add 200x200 media file
        $linearCreative
            ->createMediaFile()
            ->setProgressiveDelivery()
            ->setType('video/mp4')
            ->setHeight(200)
            ->setWidth(200)
            ->setBitrate(2500)
            ->setUrl('http://server.com/media2.mp4');

// get dom document
        $domDocument = $document->toDomDocument();

// get XML string
        echo $document;exit;
    }

    public function test_player(){
        $url = $this->input->get('url');
        if(!empty($url)){
            $this->jwplayer($url);
        }
    }
    private function jwplayer($link){
        $data['link'] = urldecode($link);
        $this->load->view(TEMPLATE_PATH . 'player/jwplayer', $data);
    }

    private function videojs(){
        $data['link'] = '';
        $this->load->view(TEMPLATE_PATH . 'player/videojs', $data);
    }
}
