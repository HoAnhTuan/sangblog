<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Club_model extends STEVEN_Model {
    public $table;

    public function __construct()
    {
        parent::__construct();
        $this->table = "club";
        $this->column_order = array("$this->table.id", "title", "country", "gender", "$this->table.is_status", "$this->table.created_time", "$this->table.updated_time");
        $this->column_search = array('title');
        $this->order_default = array("$this->table.created_time" => 'desc');
    }

    public function _where_custom($args = array()) {
        parent::_where_custom();
        extract($args);
    }

}