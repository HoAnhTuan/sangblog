<?php if (!empty($oneItem)):
?>
    <main class="main-detail"  data-url="<?php echo getUrlPost($oneItem) ?>">
        <div class="container">
	        <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
		        <div class="col-12 col-lg-9">
			        <div class="">
				        <!--<div class="d-flex flex-wrap justify-content-between">-->
					      <!--  <h1 class="font-23">-->
                <!--      --><?php //echo getStringCut($oneItem->title,0,120) ?>
					      <!--  </h1>-->
				        <!--</div>-->
				        <!--<div class="font-13 text-secondary">--><?php //echo date_post_vn($oneItem->updated_time) ?><!-- - --><?php //echo timeAgo($oneItem->updated_time,"H:i") ?><!--</div>-->
			        </div>
              <?php $this->load->view(TEMPLATE_PATH . "block/table-collapse", ['data_match_group'=>$data_match??[]]) ?>


            <?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer') ?>
		        </div>
			        <div class="col-12 col-lg-3">
<!--                  --><?php //$this->load->view(TEMPLATE_PATH . 'block/widget_tab_data') ?>
<!--                  --><?php //$this->load->view(TEMPLATE_PATH . 'block/nav_sidebar') ?>
                  <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
			        </div>
	        </div>
        </div>
        <?php if(!empty($oneItem->content)) $this->load->view(TEMPLATE_PATH . 'block/_content_footer', ['content' => $oneItem->content, 'des' => $oneItem->description]) ?>

    </main>
<?php endif; ?>
