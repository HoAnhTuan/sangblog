<!DOCTYPE html>
<html>
<head>
    <title>Player</title>
    <meta name="robots" content="noindex,nofollow">
    <style>
        body{
            overflow: hidden;
            background-color: #000000;
            font-family: Arial, Helvetica, Sans-serif, Verdana;
        }
        p{
            color: #ffffff;
        }
        #player{
            overflow: hidden;
            z-index: 1000;
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            width: 100vw !important;
            /*height: 85vh !important;*/
        }
        #container_wrapper {
            width: 100% !important;
        }

        .change_player {
            position: absolute;
            z-index: 99999;
            background-color: rgba(255, 255, 255, .5);
            padding: 5px 10px;
            border-radius: 4px;
            border: 2px solid #fff;
            top: 20px;
            right: 20px;
        }

        .boxBot, .boxTop {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            background-color: #0006;
            color: #fff;
            font-size: 12px;
            text-align: center;
            white-space: nowrap;
            z-index: 9999;
        }
        .boxBot {
            bottom: 0;
            top: auto;
        }
        .a-v9 {
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .a-v9 a {
            color: #fff;
        }
        .a-v9 img {
            margin-right: 2.1875vw;
            max-width: 8.75vw;
        }
        .a-v9 p {
            font-size: 1.25vw;
            line-height: 2.6875vw;
            color: #ffffff;
            font-weight: normal;
        }
        .a-v9 a.btn {
            background-color: #f8d000;
            width: 14.75vw;
            height: 2.625vw;
            border-radius: 0.5625vw;
            text-transform: uppercase;
            border: 1px solid #000000;
            font-size: 1.125vw !important;
            color: #040404;
            font-weight: 700;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            margin-left: 2.6875vw;
        }
        .a-v9 span {
            font-size: 1.5vw;
            color: #f8d000;
            font-weight: bold;
        }
        @media screen and (max-width: 991px){
            .a-v9 img {
                max-width: 9.75vw;
            }
            .a-v9 p {
                font-size: 2vw;
            }
            .a-v9 a.btn {
                width: 14.75vw;
                height: 3.625vw;
                font-size: 1.425vw !important;
            }
            .a-v9 span {
                font-size: 2vw;
            }
        }

    </style>

</head>
<body>
<div class="livestream" style="width: 100%">
    <div id="player">Loading the video...</div>
    <div class="boxTop a-v9">
        <a rel="nofollow" target="_blank" href="https://www.1126bet.com/?uagt=natra"><img src="https://i.imgur.com/zG5oaCY.png" alt="REG"></a>
        <p> ĐĂNG KÝ NHẬN NGAY 66K TRẢI NGHIỆM GAME HAY</p>
        <a rel="nofollow" target="_blank" href="https://www.1126bet.com/?uagt=natra" style="background: #fa0a0d;color: #fff;width:auto;padding:0px 5px !important;" class="btn">[THAM GIA NGAY]</a>
    </div>
    <div class="boxBot a-v9">
        <a rel="nofollow" target="_blank" href="https://k8vip011.cc/"><img src="https://i.imgur.com/KMb8vrl.png" alt="REG"></a>
        <p>ĐĂNG KÝ TẢI APP NHẬN NGAY 168K</p>
        <a rel="nofollow" target="_blank" href="https://k8vip011.cc/" class="btn">[THAM GIA NGAY]</a>
    </div>
</div>
<?php $settings = getSetting("data_seo"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
<script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'plugins/jwplayer/jwplayer.js' ?>"></script>
<!--<script type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'js/player.custom.js' ?><!--"></script>-->
<script type='text/javascript'>
    $(document).ready(function() {
        jwplayer.key="ITWMv7t88JGzI0xPwW8I0+LveiXX9SWbfdmt0ArUSyc=";
        var playerInstance = jwplayer("player");
        var jwConfig = {
            logo: {
                file: "<?php echo MEDIA_URL . $settings->logo ?>",
                link: "<?php echo base_url() ?>",
                position:'top-left',
                margin: '10'
            },
            file: "<?php echo(!empty($link)) ? $link : '' ; ?>",
            width: "100%",
            height: "100%",
            autostart: true,
            primary: "html5",
            /*image: '<?php echo $settings->thumbnail ?>',*/
            advertising: {
                client: 'vast',
                tag: '<?php echo base_url('ads_vast_tvc.xml') ?>',
                skipoffset: 5,
                skipmessage: 'Skip this ad in 5s'
            }
        };
        playerInstance.setup(jwConfig);
    });
</script>
</body>
</html>