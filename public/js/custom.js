$(document).ready(function () {
    const GUI = {
        search_button: function () {
            $(document).on('click', '.header-search button', function () {
                let _this = $(this);
                let input = _this.find('+ input');
                if (input.val() !== '') {
                    _this.attr('type', 'submit');
                } else {
                    input.toggleClass('show');
                }
            });
            $(document).on('click', '.main-menu-mobile button', function () {
                let _this = $(this);
                let input = _this.parent().find('input');
                if (input.val() !== '') {
                    _this.attr('type', 'submit');
                }
            });
        },
        slide_tournament: function () {
            let current_round = parseInt($('[data-current-round]').data('current-round')) - 1;
            let elDataRound = $('[data-round]');
            $('[data-round="' + (current_round + 1) + '"]').removeClass('d-none');
            let swiper = new Swiper('[data-current-round] > .swiper-container', {
                spaceBetween: 10,
                centeredSlides: true,
                loop: true,
                slideToClickedSlide: true,
                initialSlide: current_round,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.bxh-next',
                    prevEl: '.bxh-prev',
                },
                breakpoints: {
                    0: {
                        slidesPerView: 1,
                    },
                    400: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 5,
                    },
                },
            });

            swiper.on('slideChange', function () {
                let date = $(
                    '.swiper-slide:not(.swiper-slide-duplicate)[data-swiper-slide-index="' + swiper.realIndex + '"] a'
                ).data('time');
                GUI.ajax_load_schedule_today(date);
            });
        },
        slide_today: function () {
            let current_date = $('[data-date]').data('date');
            let swiper = new Swiper('[data-date] > .swiper-container', {
                spaceBetween: 15,
                centeredSlides: true,
                loop: true,
                slideToClickedSlide: true,
                initialSlide: 3,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    0: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 5,
                    },
                },
            });
            if ($('[data-swiper-slide-index="' + swiper.realIndex + '"]').data('date') !== current_date) {
                swiper.slideTo(4);
                swiper.update();
            }
            swiper.on('click', function () {
                let date = $(
                    '.swiper-slide:not(.swiper-slide-duplicate)[data-swiper-slide-index="' + swiper.realIndex + '"]'
                ).data('date');
                if ($('.ajax-content-livescore').length > 0)
                    this.ajax_load_content(base_url + 'match/ajax_load_match_livescore', { date: date });
                else this.ajax_load_content(base_url + 'match/ajax_load_match', { date: date });
            });
        },
        swiper_home: function () {
            let swiper_home = new Swiper('.swiper-container.home-swiper', {
                loop: true,
                pagination: {
                    el: '.swiper-pagination.post-top',
                    clickable: true,
                },
            });
            var swiper = new Swiper('.nhandinhSwiper', {
                slidesPerView: 1,
                spaceBetween: 30,
                autoplay: {
                    delay: 2000,
                },
                pagination: {
                    el: '.swiper-pagination.nhandinh-pagination',
                    clickable: true,
                },
                breakpoints: {
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 50,
                    },
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 60,
                    },
                    1024: {
                        slidesPerView: 4,
                        spaceBetween: 80,
                    },
                    1360: {
                        slidesPerView: 5,
                        spaceBetween: 80,
                    },
                },
            });
        },
        swiper_home_bottom: function () {
            let swiper_home_bottom = new Swiper('.swiper-container.home-swiper-bottom', {
                slidesPerView: 1,
                spaceBetween: 30,
                loop: true,
                pagination: {
                    el: '.swiper-pagination.post-bottom',
                },
                breakpoints: {
                    500: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 40,
                    },
                },
            });
        },
        menu_mobile: function () {
            $('[data-toggle="offcanvas"]').on('click', function () {
                $('.offcanvas-collapse').toggleClass('open');
                if ($('.offcanvas-collapse').hasClass('open')) {
                    $('body>header').hide();
                } else {
                    $('body>header').show();
                }
            });
        },
        ajax_load_content: function (url, params) {
            let element = '#ajax_content';
            let _container = $(element);
            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                dataType: 'JSON',
                beforeSend: function () {
                    _container.html(
                        '<div class=\'m-auto text-center text-danger\'><i class="fa fa-spinner fa-3x fa-spin"></i>Na Tra đang loading ...</div>'
                    );
                },
                success: function (response) {
                    if (response.data.length > 0) {
                        _container.html(response.data);
                    } else {
                        _container.html("<div class='m-auto text-center text-danger'>Không có trận đấu nào ..</div>");
                    }
                },
                error: function (jxhr, msg, err) {
                    console.log(jxhr);
                    console.log(msg);
                    console.error(err);
                },
            });
        },
        event_filter_match: function () {
            $(document).on('click', '.btnFilterMatch', function () {
                let type = $(this).data('type');
                this.ajax_load_content(base_url + 'match/ajax_load_match_play', { type: type });
            });
        },
        event_filter_tournament: function () {
            $(document).on('click', '.btnChooseTournament', function () {
                let tournament_id = $(this).data('tournament-id');
                $('a[data-tournament-id]:not(".btn-secondary")').removeClass('btn-danger').addClass('btn-secondary');
                $(this).addClass('btn-danger').removeClass('btn-secondary');
                $('#ajax_content > [data-tournament-id]:not(.d-none)').addClass('d-none');
                $('#ajax_content > [data-tournament-id="' + tournament_id + '"]').removeClass('d-none');
            });
        },
        filterLivescore: function () {
            $(document).on('change', 'select[name="filter_livescore"]', function () {
                let val = $(this).val();
                $('.ajax-content-livescore .item-match').show();
                $('.ajax-content-livescore table tr').show();
                if (val !== 'all') {
                    $('.ajax-content-livescore table tr:not(.' + val + ')')
                        .hide()
                        .closest('.item-match')
                        .hide();
                    // $('.ajax-content-livescore table tr:not(.'+val+')').closest(".item-match").hide();
                    $('.ajax-content-livescore .item-match')
                        .find('.' + val)
                        .closest('.item-match')
                        .show();
                }
            });
            $('select[name="filter_livescore"]').val('select-live').trigger('change');
        },
        social_sharte: function () {
            if ($('.social-share').length > 0) {
                $('.social-share').jsSocials({
                    shares: ['twitter', 'facebook', 'email'],
                });
            }
        },
        rate: function () {
            let selector = $('.rateit');
            if (selector.length > 0) {
                selector.bind('rated', function (e) {
                    e.preventDefault();
                    let ri = $(this);
                    let value = ri.rateit('value');
                    let urlC = window.location.href;
                    let url = base_url + 'reviews/ajax_vote';
                    let request = {
                        url: urlC,
                        rate: value,
                    };
                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'JSON',
                        data: request,
                        success: function (data) {
                            if (data.vote) {
                                ri.rateit(data.vote.avg);
                            }
                            if (data.type == 'success') {
                                let _avg = $('.avg-rate');
                                let _count = $('.count-rate');
                                let avg = parseFloat(_avg.text());
                                let count = parseInt(_count.text());
                                _avg.text(((avg * count + value) / (count + 1)).toFixed(1));
                                _count.text(count + 1);
                                $.ajax({
                                    url: base_url + 'debug/delete_cache_file?url=' + urlC,
                                    type: 'post',
                                });
                            }
                            if (data.type == 'warning') ri.rateit('readonly', true);
                            else ri.rateit('readonly', false);
                            $('.toast-body').html(
                                '<span style="font-weight: 600; font-size: 13px">' + data.message + '</span>'
                            );
                            $('.toast').toast('show');
                        },
                        error: function (jxhr, msg, err) {
                            console.log(jxhr);
                            console.log(msg);
                            console.error(err);
                        },
                    });
                });
            }
        },
        loadMorePost: function () {
            if ($('button.btnLoadMore').length > 0) {
                $(document).on('click', 'button.btnLoadMore', function () {
                    let el = $(this);
                    let url = el.data('url');
                    let page = parseInt(el.attr('data-page'));
                    el.attr('disable', true);
                    $.ajax({
                        url: url + '/' + page,
                        type: 'POST',
                        beforeSend: function () {
                            el.append('<i class="fa fa-spinner fa-spin"></i>');
                        },
                        success: function (html) {
                            if (html.length > 0) {
                                page++;
                                let contentPage = $(html).find('#ajax_content').html();
                                $('#ajax_content').append(contentPage);
                                el.attr('data-page', page);
                                el.children('i').remove();
                            } else {
                                $('.btn-load-more').html('Hết dữ liệu !');
                            }
                            el.attr('disable', false);
                        },

                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus + ': ' + errorThrown);
                        },
                    });
                });
            }
        },
        renderBanner: function () {
            setTimeout(function () {
                /* get data banner */
                var data = JSON.parse(JSON.stringify(data_banner));
                /* dom banners */
                var banners = $('.bannerElement');
                if (banners.length > 0 && data.length > 0) {
                    banners.each(function () {
                        let location = $(this).data('location');
                        let device = $(this).data('device');

                        /* filter data banner and location */
                        var filteredBanner = $(data).filter(function (n) {
                            return data[n].location == location && data[n].is_device == device;
                        });

                        /* sort after filter */
                        if (filteredBanner.length > 1) {
                            filteredBanner.sort(function (a, b) {
                                return a.sort - b.sort;
                            });
                        }

                        /* append banner for each location */
                        for (let $k = 0; $k < filteredBanner.length; $k++) {
                            if (location == filteredBanner[$k].location && device == filteredBanner[$k].is_device) {
                                let info = JSON.parse(filteredBanner[$k].data_info);
                                let link =
                                    `<a href="` +
                                    info.link +
                                    `" class="d-block text-center ` +
                                    `" rel="nofollow" target="_blank" title="` +
                                    info.title +
                                    `"><img class="img-fluid" src="` +
                                    media_url +
                                    filteredBanner[$k].thumbnail +
                                    `" alt="` +
                                    info.alt +
                                    `" title="` +
                                    info.title +
                                    `"></a>`;
                                $(this).append(link);

                                if (location === 'popup') {
                                    let first = sessionStorage.getItem('first');

                                    if (first != 'false') {
                                        $('#adpopup').modal('show');
                                        sessionStorage.setItem('first', 'false');
                                        $(document).on('click', function (event) {
                                            event.preventDefault();
                                            window.open('https://tai.go88vn.shop/?dl=seoasoikeo', '_blank');
                                            $(document).unbind();
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            }, 4000);
        },
        sticky_banner: function () {
            let window_width = $(window).width();
            $('.BannerStickyLeft_content, .BannerStickyRight_content').trigger('sticky_kit:detach');
            if (window_width > 980) {
                $('.BannerStickyLeft_content , .BannerStickyRight_content').stick_in_parent({
                    parent: 'body',
                    offset_top: 30,
                });
            }
        },
        Popup: function () {
            setTimeout(function () {
                // banner popup
                let banner = $('#ads_popup');
                let keyPopup = 'checkPopup';

                if (!sessionStorage.getItem(keyPopup) && $('#ads_popup .modal-body img').length > 0) {
                    sessionStorage.setItem(keyPopup, 1);
                    banner.modal('show');
                } else {
                    // console.log('ton tai keyPopup');
                }
                // 30 phut
                setInterval(function () {
                    if (sessionStorage.getItem(keyPopup)) {
                        sessionStorage.removeItem(keyPopup);
                    }
                    console.log('xoa keyPopup');
                }, 18000000);
            }, 3000);
        },
        update_tylekeo() {
            let ele = $('.tylekeo');
            if (ele.length !== 0) {
                setInterval(() => {
                    $.get(window.location.href, function (html, status) {
                        console.log(window.location.href);
                        let content = $(html).find('.tylekeo').html();
                        ele.html(content);
                    });
                }, 10000);
            }
        },

        update_tylekeoHome() {
            let ele = $('.competition-middle');
            if (ele.length !== 0) {
                setInterval(() => {
                    $.get(window.location.href, function (html, status) {
                        console.log(window.location.href);
                        let content = $(html).find('.competition-middle').html();
                        ele.html(content);
                    });
                }, 10000);
            }
        },

        sidebar_swiper: function () {
            let swiper1 = new Swiper('.mySwiper1', {
                spaceBetween: 15,
                autoplay: {
                    delay: 2000,
                },
                pagination: {
                    el: '.swiper-pagination-1',
                    clickable: true,
                },
            });
            let swiper2 = new Swiper('.mySwiper2', {
                spaceBetween: 15,
                autoplay: {
                    delay: 2000,
                },
                pagination: {
                    el: '.swiper-pagination-2',
                    clickable: true,
                },
            });
        },
        collapseTable: function () {
            $('.table-collapse .sub-heading').on('click', function () {
                $(this).closest('.table').toggleClass('active');
            });
        },
        activeCurrentLink: function () {
            $('ul>li a[href="' + window.location.origin + window.location.pathname + '"]')
                .parent()
                .addClass('active');
        },
        ajax_load_schedule_today: function (date) {
            console.log(date);
            $.ajax({
                url: base_url + 'category/ajax_load_data_today',
                data: {
                    date: date,
                    layout: layout,
                },
                beforeSend: function () {
                    $('#ajax-table').html(
                        '<div class="text-center text-white bg-info"><i class="fas fa-spinner fa-spin fa-3x"></i></div>'
                    );
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (data) {
                    if (data.type == 'Success') {
                        $('#ajax-table').html(data.data);
                    }
                },
                error: function (jxhr, msg, err) {
                    console.log(jxhr);
                    console.log(msg);
                    console.error(err);
                },
            });
        },
        countdown_match: function () {
            if (
                typeof start_time != 'undefined' &&
                start_time != null &&
                start_time !== '' &&
                $('.league-count-down').length > 0
            ) {
                var countDownDate = new Date(start_time).getTime();
                var x = setInterval(function () {
                    // Get today's date and time
                    var now = new Date().getTime();

                    // Find the distance between now and the count down date
                    var distance = countDownDate - now;

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    // Output the result in an element with id="demo"
                    $('.league-count-down #days').html(days);
                    $('.league-count-down #hours').html(hours);
                    $('.league-count-down #minutes').html(minutes);
                    $('.league-count-down #seconds').html(seconds);

                    // If the count down is over, write some text
                    if (distance < 0) {
                        clearInterval(x);
                        $('#countdown').remove();
                    }
                }, 1000);
            }
        },
        init: function () {
            // GUI.update_tylekeoHome;
            // GUI.update_tylekeo();
            GUI.collapseTable();
            GUI.swiper_home();
            GUI.sidebar_swiper();
            GUI.search_button();
            GUI.swiper_home_bottom();
            GUI.menu_mobile();
            GUI.slide_tournament();
            GUI.slide_today();
            GUI.event_filter_match();
            GUI.event_filter_tournament();
            GUI.filterLivescore();
            GUI.rate();
            GUI.social_sharte();
            GUI.loadMorePost();
            GUI.renderBanner();
            // GUI.ajax_load_schedule_today();
            GUI.activeCurrentLink();
            GUI.countdown_match();
            // GUI.sticky_banner();
            GUI.Popup();
        },
    };

    GUI.init();
});
function openTySo(id, isAjaxCall) {
    if (isAjaxCall === 'undefined') isAjaxCall = false;
    let thisElement = document.getElementById('tiso' + id);
    let tiso = localStorage.getItem('keo_tiso_open');
    tiso = JSON.parse(tiso);
    let tiso_final = tiso;
    if (Array.isArray(tiso)) {
        tiso_final = tiso_final.filter(function (open_id) {
            return open_id !== id;
        });
    }
    thisElement.classList.toggle('active');
    thisElement.nextElementSibling.classList.toggle('active');
    if (Array.isArray(tiso)) {
        tiso_final.push(id);
    } else {
        tiso_final = [id];
    }
    localStorage.setItem('keo_tiso_open', JSON.stringify(tiso_final));
}

function isViewTLH1(isView, id) {
    let elBut = $('#showHiep' + id);
    let hiep1 = localStorage.getItem('keo_hiep_1_open');
    hiep1 = JSON.parse(hiep1);
    let hiep1_final = hiep1;
    if (Array.isArray(hiep1)) {
        hiep1_final = hiep1_final.filter(function (open_id) {
            return open_id !== id;
        });
    }
    if (isView === 0) {
        elementShow('viewTyLeHiep' + id);
        elBut.css('opacity', '0');
        if (Array.isArray(hiep1)) {
            hiep1_final.push(id);
        } else {
            hiep1_final = [id];
        }
    } else {
        elementHideShow('viewTyLeHiep' + id);
        elBut.css('opacity', '1');
    }
    localStorage.setItem('keo_hiep_1_open', JSON.stringify(hiep1_final));
}

function elementShow(elementToHideOrShow) {
    let el = $('#' + elementToHideOrShow);
    el.show();
}

function elementHideShow(elementToHideOrShow) {
    let el = $('#' + elementToHideOrShow);
    el.hide();
}
