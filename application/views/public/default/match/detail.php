<script>
	var start_time = "<?php echo $oneItem->start_time ?? '' ?>";
</script>
<?php if (!empty($oneItem)) :
	$data_info_manager = !empty($data_info->manager) ? $data_info->manager : null;
	$data_info_season = !empty($data_info->season) ? $data_info->season : null;
	$data_info_jerseys = !empty($data_info->jerseys) ? $data_info->jerseys : null;
	//    $data_odds = json_decode($oneItem->data_bets, true);
?>
	<main class="bg-light">
		<div class="container">
			<div class="match-detail">
				<div class="match-count-down  mb-5">
					<h1><?php echo "Thông tin trận đấu $oneItem->name_home vs $oneItem->name_away $oneItem->league_name "  . date('d/m/Y', $oneItem->start_timestamp) ?></h1>
					<div class="count-down-block">
						<img class="match-img-left" src="<?php echo TEMPLATES_ASSETS . 'images/Group 68.png' ?>" alt="match group 68">
						<img class="match-img-right" src="<?php echo TEMPLATES_ASSETS . 'images/Group 67.png' ?>" alt="match group 67">
						<?php echo getLogoClub($oneItem->home_id, $oneItem->name_home, "img-fluid", 200, 200) ?>
						<div class="d-flex flex-column text-center league-info">
							<div class="league-name"><?php echo $oneItem->league_name ?? ''; ?></div>
							<div class="league-time"><?php echo date('d/m/Y - H:i', $oneItem->start_timestamp); ?></div>
							<?php if ($oneItem->match_status === 'finished') : ?>
								<div class="text-secondary font-30 font-weight-bold text-uppercase font-italic"><?= $oneItem->score_home . ' - ' . $oneItem->score_away ?></div>
							<?php elseif ($oneItem->match_status === "live") : ?>
								<div class="font-16 font-weight-bold position-relative">
									<span class="text-warning live-match"><?php echo !empty($oneItem->elapsed) ? $oneItem->elapsed . "'" : $oneItem->status ?></span>
									<div class="text-danger font-30 font-weight-bold text-uppercase font-italic"><?php echo $oneItem->score_home . " - " . $oneItem->score_away ?></div>
								</div>
							<?php else : ?>
								<div class="league-count-down">
									<div class="">
										<span id="days" class="text-danger">00</span>
										<span>ngày</span>
									</div>
									<div class="">
										<span id="hours" class="text-success">00</span>
										<span>giờ</span>
									</div>
									<div class="">
										<span id="minutes" class="text-info">00</span>
										<span>phút</span>
									</div>
									<div class="">
										<span id="seconds" class="text-warning">00</span>
										<span>giây</span>
									</div>
								</div>
							<?php endif; ?>
							<?php if (!empty($referee_name)) : ?>
								<div>
									<span class="text-secondary">Trọng tài: </span><?php echo $referee_name; ?>
								</div>
							<?php endif; ?>
							<?php if (!empty($stadium_name)) : ?>
								<div>
									<span class="text-secondary">Sân vận động: </span><?php echo $stadium_name; ?>
								</div>
							<?php endif; ?>
						</div>
						<?php echo getLogoClub($oneItem->away_id, $oneItem->name_away, "img-fluid", 200, 200) ?>
					</div>
				</div>
				<div class="match-info  mb-5">
					<h2>Thông tin trận đấu</h2>
					<div class="match-info-block">
						Thông tin trận đấu <b><?= $oneItem->name_home ?> vs <?= $oneItem->name_away ?>
						</b> sẽ diễn ra vào lúc <b><?php echo date('H:i', $oneItem->start_timestamp); ?></b> ngày <b><?php echo date('d/m/Y', $oneItem->start_timestamp); ?></b><?php echo !empty($stadium_name) ? "
						, trên sân vận động " . "<b>$stadium_name</b>" : '' ?><?php echo !empty($referee_name) ? " , điều khiển trận đấu này là trọng tài " . "<b>$referee_name</b>" : '' ?>
						<br>
						<?php if (!empty($onePost)) : ?>
							Để được cung cấp các phân tích chi tiết về tỷ lệ kèo cùng dự đoán về trận đấu của đội ngũ chuyên gia uy tín, nhiều năm kinh nghiệm, xin đọc bài
							<a class="font-weight-bold" href="<?= getUrlPost($onePost) ?>"><?= $oneItem->name_home ?> vs <?= $oneItem->name_away ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="match-predict  pb-5">
					<h2>dự đoán tỉ lệ</h2>
					<div class="match-predict-block">
						<div class="color-success">
							<div class="progress-bar position" data-percent="<?php echo rand(20, 95) ?>" data-duration="1000" data-color="#D8D8D8,#18BD27"></div>
							<p>Đội nhà thắng</p>
						</div>
						<div class="color-info">
							<div class="progress-bar position" data-percent="<?php echo rand(20, 95) ?>" data-duration="1000" data-color="#D8D8D8,#1DA3F8"></div>
							<p>Hòa</p>
						</div>
						<div class="color-warning">
							<div class="progress-bar position" data-percent="<?php echo rand(20, 95) ?>" data-duration="1000" data-color="#D8D8D8,#FCAA0C"></div>
							<p>Đội khách thắng</p>
						</div>
					</div>
				</div>

				<?php if (!empty($h2h)) :  ?>
					<div class="match-h2h mb-5">
						<h4>thành tích đối đầu</h4>
						<div class="match-h2h-block table-responsive overflow-auto">
							<table class="table table-striped">
								<thead class="">
									<tr>
										<th>Thời gian</th>
										<th>Đội nhà</th>
										<th>Tỷ số</th>
										<th>Đội khách</th>
										<th>Giải đấu</th>
										<th>Sân vận động</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($h2h as $key => $item) :
										$childInfo = json_decode($item->data_info);
									?>
										<tr>
											<td><?= date('H:i d/m/Y', $item->start_timestamp) ?></td>
											<td><?= $item->name_home ?></td>
											<td>
												<span class="<?php echo ($item->score_home > $item->score_away) ? 'badge-danger' : 'badge-secondary'; ?>"><?= $item->score_home ?></span>
												<span class="<?php echo ($item->score_away > $item->score_home) ? 'badge-danger' : 'badge-secondary'; ?>"><?= $item->score_away ?></span>
											</td>
											<td><?= $item->name_away ?></td>
											<td><?= !empty($item->league_name) ? $item->league_name : "Đang cập nhật"; ?></td>
											<td><?= !empty($item->stadium_name) ? $item->stadium_name : "Đang cập nhật"; ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				<?php endif; ?>

				<?php if (!empty($last10)) : ?>
					<div class="match-recent  pb-5">
						<h4>thành tích gần đây</h4>
						<div class="match-recent-block table-responsive overflow-auto">
							<table class="table table-striped">
								<thead class="">
									<tr>
										<th colspan="3"><?php echo $oneItem->name_home ?></th>
									</tr>
									<tr>
										<th>Đội nhà</th>
										<th>Tỷ số</th>
										<th>Đội khách</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($last10->{$oneItem->home_id} as $key => $item) : ?>
										<tr>
											<td><?= $item->name_home ?></td>
											<td>
												<span class="<?php echo ($item->score_home > $item->score_away) ? 'badge-danger' : 'badge-secondary'; ?>"><?= $item->score_home ?></span>
												<span class="<?php echo ($item->score_away > $item->score_home) ? 'badge-danger' : 'badge-secondary'; ?>"><?= $item->score_away ?></span>
											</td>
											<td><?= $item->name_away ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							<table class="table table-striped">
								<thead class="">
									<tr>
										<th colspan="3"><?php echo $oneItem->name_away ?></th>
									</tr>
									<tr>
										<th>Đội nhà</th>
										<th>Tỷ số</th>
										<th>Đội khách</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($last10->{$oneItem->away_id} as $key => $item) : ?>
										<tr>
											<td><?= $item->name_home ?></td>
											<td>
												<span class="<?php echo ($item->score_home > $item->score_away) ? 'badge-danger' : 'badge-secondary'; ?>"><?= $item->score_home ?></span>
												<span class="<?php echo ($item->score_away > $item->score_home) ? 'badge-danger' : 'badge-secondary'; ?>"><?= $item->score_away ?></span>
											</td>
											<td><?= $item->name_away ?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							<?php if (!empty($data_odd_info->data_form)) echo $data_odd_info->data_form; ?>
						</div>
					</div>
				<?php endif; ?>

			</div>
		</div>
	</main>
<?php endif; ?>