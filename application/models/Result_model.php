<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result_model extends STEVEN_Model
{
    protected $_domain_api = "http://apinew.xsradar.com/";
    protected $_data_category;
    public function __construct(){
        parent::__construct();
        $this->table            = "category";
        $this->load->model(['cateresult_model']);
        $this->_data_category = new Cateresult_model();
    }

    public function getDataByCategory($params,$timeCache = 3600, $updateCache = false){
        $keyCache = "getDataByCategory".md5(json_encode($params));
        $data = $this->getCache($keyCache);
        if (empty($data) || $updateCache){
            $data = $this->getDataAPIXoso($this->_domain_api."result/getDataByCategory",$params);
            $this->setCache($keyCache, $data, $timeCache);
        }
        return $data;
    }
    public function getDataByCategoryDayOfWeek($params, $timeCache = 3600, $updateCache = false){
        $keyCache = "getDataByCategoryDayOfWeek".md5(json_encode($params));
        $data = $this->getCache($keyCache);
        if (empty($data) || $updateCache){
            $data = $this->getDataAPIXoso($this->_domain_api."result/getDataByCategoryDayOfWeek",$params);
            $this->setCache($keyCache, $data, $timeCache);
        }
        return $data;
    }
    public function getFromDayToDay($params, $timeCache = 3600, $updateCache = false){
        $keyCache = "getFromDayToDay".md5(json_encode($params));
        $data = $this->getCache($keyCache);
        if (empty($data) || $updateCache){
            $data = $this->getDataAPIXoso($this->_domain_api."result/getFromDayToDay",$params);
            $this->setCache($keyCache, $data, $timeCache);
        }
        return $data;
    }
    public function getDataNearestByDay($params, $timeCache = 3600, $updateCache = false){
        $keyCache = "getDataNearestByDay".md5(json_encode($params));
        $data = $this->getCache($keyCache);
        if (empty($data) || $updateCache){
            $data = $this->getDataAPIXoso($this->_domain_api."result/getDataNearestByDay",$params);
            $this->setCache($keyCache, $data, $timeCache);
        }
        return $data;
    }

}