<?php
	$trends = getDataPost([
    'category_id' => 2,
    'is_status' => 1,
    'limit' => 5,
    'is_today' => true,
    'order' => ['displayed_time' => 'DESC']
	]);
	if(empty($trends)) {
      $trends = getDataPost([
        'select' => 'st_post.title, st_post.id, st_post.slug',
        'is_status' => 1,
        'limit' => 5,
        'category_id' => 2,
        'order' => ['displayed_time' => 'DESC']
      ]);
	}
?>
<header>
		<div class="container">
			<?php
				if(!empty($trends)) :
			?>
			<div class="top-header">
				<div class="d-flex align-items-center h-100">
					<b class="trending text-danger">TRENDING | </b>

					<div id="slide-top-header" class="carousel slide" data-ride="carousel">
						<!-- The slideshow -->
						<div class="carousel-inner">
                <?php foreach ($trends as $key => $item) : ?>
									<div class="carousel-item <?php echo $key==0? 'active' : ''?>">
										<a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title;?>"><?php echo $item->title;?></a>
									</div>
                <?php endforeach; ?>
						</div>
						<!-- Left and right controls -->
						<a class="carousel-control-prev header-control" href="#slide-top-header" data-slide="prev">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<g clip-path="url(#clip0_227:1672)">
									<path d="M25 12H5" stroke="#E92D34" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M12 19L5 12L12 5" stroke="#E92D34" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								</g>
								<defs>
									<clipPath id="clip0_227:1672">
										<rect width="24" height="24" fill="white"/>
									</clipPath>
								</defs>
							</svg>

						</a>
						<a class="carousel-control-next header-control" href="#slide-top-header" data-slide="next">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<g clip-path="url(#clip0_227:1669)">
									<path d="M-1 12L19 12" stroke="#E92D34" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
									<path d="M14 5L21 12L14 19" stroke="#E92D34" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								</g>
								<defs>
									<clipPath id="clip0_227:1669">
										<rect width="24" height="24" fill="white"/>
									</clipPath>
								</defs>
							</svg>

						</a>
					</div>

				</div>
			</div>
        <?php endif; ?>
		</div>
</header>
<nav class="middle-header bg-menu navbar navbar-expand-lg disabled py-0 menu-top sticky-top  navbar-dark">
	<div class="container">
		<a href="<?php echo base_url() ?>" title="<?php echo getSetting("data_seo","meta_title") ?>">
        <?php echo getThumbnailStatic((!empty(getSetting("data_seo","logo"))) ? getImageThumb(getSetting("data_seo","logo")) : TEMPLATES_ASSETS . 'assets/img/logo.svg','250',74,'logo',"img-fluid logo-top") ?>
		</a>

		<button class="navbar-toggler" type="button" data-toggle="offcanvas">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="offcanvas-collapse d-block d-lg-none main-menu-mobile">
			<div class="d-flex align-items-center my-3 my-lg-0">
				<form action="<?php echo site_url("tim-kiem") ?>" method="get" class="position-relative">
					<input type="text" name="q" value="<?php echo $this->input->get('q') ?>" placeholder="Tìm kiếm...">
					<button type="button" class="btn btn-search">
						<i class="fas fa-search"></i>
					</button>
				</form>
			</div>
      <?php echo navMenuMainMobile("navbar-nav mr-auto text-uppercase menu-pc","","dropdown-menu m-0 disabled") ?>
			<div class="footer-offcanvas">
				<p>Liên hệ: <?= $this->_settings->email ?? 'quangcao@keonhacai9.com'?></p>
				<p>Copyright by <?= $this->_settings->domain ?? ''?> @<?php echo date('Y')?></p>
			</div>
		</div>
		<div class="navbar-collapse offcanvas-collapse d-none d-lg-block">
        <?php echo navMenuMiddle("navbar-nav mr-auto text-uppercase menu-pc menu-main ml-auto","","dropdown-menu m-0 disabled") ?>
		</div>

		<div class="container bottom-header">
			<div class="bottom-header_1">
					<a class="btn-lg btn py-2 px-4 text-white" href="<?php echo base_url() ?>" title="<?php echo getSetting("data_seo","meta_title") ?>">
						<i class="fas fa-home"></i>
					</a>
            <?php echo navMenuMain("navbar-nav mr-auto text-uppercase menu-pc menu-space flex-row","","dropdown-menu m-0 disabled") ?>
					<div class="d-flex align-items-center header-search my-3 my-lg-0">
						<form action="<?php echo site_url("tim-kiem") ?>" method="get" class="position-relative">
							<button type="button" class="btn btn-search">
								<i class="fas fa-search"></i>
							</button>
							<input type="text" class="form-control position-absolute" name="q" value="<?php echo $this->input->get('q') ?>" placeholder="Tìm kiếm">
						</form>
					</div>
				</div>
			</div>
	</div>

	<div class="mobile-fixed-bottom d-lg-none">
      <?php echo navMenuStickyFooter('nav', '', ''); ?>
	</div>
</nav>

