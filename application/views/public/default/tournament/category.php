<main class="main-content">
    <?php $this->load->view(TEMPLATE_PATH . '/block/main_banner') ?>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <?php echo showContainerBanner('top_left', 0, '') ?>
            </div>
            <div class="col-12 col-md-6">
                <?php echo showContainerBanner('top_right', 0, '') ?>
            </div>
            <div class="col-12">
                <h2 class="cat-title"><?php echo $oneItem->title ?></h2>
                <?php if (!empty($breadcrumb)) echo $breadcrumb; ?>
            </div>
            <main class="col-12 col-md-9">
                <?php if (!empty($list_post)) : ?>
                    <ul class="blog-post__layout_vertical_style_1">
                        <?php foreach ($list_post as $key => $item) : ?>
                            <li class="blog-post__post-item">
                                <div class="blog-post__thumbnail">
                                    <a class="thumbnail" href="<?php echo getUrlPost($item) ?>" title="title" style="background-image: url('<?php echo getImageThumb($item->thumbnail) ?>'); ">
                                    </a>
                                </div>
                                <div class="blog-post__meta">
                                    <a class="category-primary category-label" href="<?php echo getUrlCategory($oneItem) ?>">
                                        <?php echo $oneItem->title ?>
                                    </a>
                                    <div class="time">
                                        <i class="far fa-clock"></i>
                                        <span><?php echo date('d/m/Y', strtotime($item->displayed_time)) ?></span>
                                    </div>
                                    <h4 class="title">
                                        <a href="<?php echo getUrlPost($item) ?>">
                                            <?php echo $item->title ?>
                                        </a>
                                    </h4>
                                    <div class="excerpt"><?php echo $item->description ?></div>
                                    <a class="btn-readmore" href="<?php echo getUrlPost($item) ?>">Xem thêm</a>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php echo !empty($pagination) ? $pagination : '' ?>
                <?php else : ?>
                    <h2 class="text-center">Danh mục này chưa có bài viết</h2>
                <?php endif; ?>
                <div class="category-content">
                    <?php echo getTableOfContent($oneItem->content, $oneItem->title) ?>
                </div>
            </main>
            <aside class="col-12 col-md-3">
                <?php $this->load->view(TEMPLATE_PATH . '/block/sidebar') ?>
            </aside>
        </div>
    </div>
</main>