<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;
class Keowin365 extends Crawler_Controller
{
    private $_category;
    private $_post;
    private $_client;
    private $_base_url;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['category_model', 'post_model']);
        $this->_category = new Category_model();
        $this->_post = new Post_model();
        $this->_client = new Client(HttpClient::create(['timeout' => 60]));
        $this->_base_url = 'https://keowin365.vip';
    }
    public function get_soi_keo($id1, $id2){
        try {
            $data_json = $this->callCURL('https://keowin365.vip/debug/get_data_soikeo/'.$id1);
            $data = json_decode($data_json);
            $data_category = $id2;
            if(!empty($data)) foreach ($data as $index => $item) {
                if (!$this->_post->checkExistByField("slug",$item->slug)) {
                    $post['title'] = $item->title;
                    $post['meta_title'] = $item->meta_title;
                    $post['description'] = $item->description;
                    $post['meta_description'] = $item->meta_description;
                    $post['slug'] = $item->slug;
                    $post['meta_keyword'] = $item->meta_keyword;
                    $post['is_status'] = $item->is_status;
                    $post['start_time'] = $item->start_time;
                    $post['is_featured'] = $item->is_featured;
                    $post['displayed_time'] = $item->displayed_time;
                    $post['data_bets'] = $item->data_bets;
                    $post['type'] = $item->type;
                    $post['is_robot'] = $item->is_robot;
                    $post['match_id'] = $item->match_id;
                    $post['user_id'] = 1;  
                    $bot_crawler_detail = $this->_client->request('GET', $this->_base_url . '/' . $item->slug . '-p' . $item->id . '.html');
                    $content = $item->content;
                    $imgs = $bot_crawler_detail->filter('.content img')->each(function ($node) {
                        if (preg_match('/http/i', $node->attr('src'))) $imgSrc = $node->attr('src');
                        else $imgSrc = $this->_base_url . $node->attr('src');
                        $srcNew = $this->downloadImage($imgSrc, '', 'soi-keo');
                        return [
                          'old' => $node->attr('src'),
                          'new' => "/" . MEDIA_NAME . $srcNew
                        ];
                    });
                    if (!empty($imgs)) foreach ($imgs as $img) {
                        $content = str_replace($img['old'], $img['new'], $content);
                    }
                    $post['content'] = $content;
                    $post['thumbnail'] = $this->downloadImage('https://keowin365.vip/uploads/'. $item->thumbnail, '', 'soi-keo');
                    $primary_category = $item->is_primary;
                    if($id = $this->_post->save($post)){
                        $this->save_category($id, $data_category, $primary_category);
                        echo $message   = 'Thêm post có id là : '.$id . '\\n';
                    }else{
                        echo $message   = 'Lỗi khi thêm post có id là : '.$id . '\\n';
                    }
                }
            }else{
                echo "No data \n";
            }
            echo "Done {$id1} - {$id2} \n";
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
    /* keowin
     * Anh -> 33
     * C1 -> 34
     * C2 -> 35
     * TBN -> 37
     * Y -> 38
     * Duc -> 39
     * DTQG -> 40
     * Phap -> 41
     * WorldCup -> 42
    */
    /*keonhacai9
     * Anh -> 18
     * C1 -> 11
     * C2 -> 12
     * TBN -> 13
     * Y -> 19
     * Duc -> 17
     * DTQG -> 20
     * Phap -> 14
     * WorldCup -> 21
     */
    public function start(){
        $arr = [
          '33' => 18,
          '34' => 11,
          '35' => 12,
          '37' => 13,
          '38' => 19,
          '39' => 17,
          '40' => 20,
          '41' => 14,
          '42' => 21,
        ];
        foreach ($arr as $keowinId => $nhacai9Id){
            $this->get_soi_keo($keowinId, $nhacai9Id);
        }
    }
    private function save_category($id, $data,$primary_category_id){
        $this->_post->delete([$this->_post->table.'_id'=>$id],$this->_post->table_post_category);
//        dd($data,$primary_category_id);
//        if(!empty($data)) foreach ($data as $category_id){
//            $tmp = [
//              "{$this->_post->table}_id" => $id,
//              'category_id' => $category_id,
//              'is_primary' => $category_id == $primary_category_id ? true : false
//            ];
//            $data_category[] = $tmp;
//        }
        $data_category = [
          "{$this->_post->table}_id" => $id,
            'category_id' => $data,
            'is_primary' => $data == $primary_category_id ? true : false
        ];

        if(!$this->_post->insert($data_category, $this->_post->table_post_category)){
            $message['type'] = 'error';
            $message['message'] = "Thêm {$this->_post->table_category} thất bại !";
            log_message('error', $message['message'] . '=>' . json_encode($data_category));
//            $this->returnJson($message);
            echo $message['message'];
        }
    }

}

