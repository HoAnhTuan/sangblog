<?php if (!empty($data_match)) foreach ($data_match as $item) : ?>
    <tr class="team text-right">
        <td style="width: 40%;">
            <?php echo $item->name_home ?>
            <?php echo getLogoClub($item->home_id, $item->name_home, "ml-2 logo") ?>
        </td>
        <td class="text-center">
            <div class="d-inline-block score">
                <?php if ($item->match_status === 'finished') : ?>
                    <?php echo $item->score_home . " - " . $item->score_away; ?>
                <?php elseif ($item->match_status === "notstarted") : ?>
                    <?php echo timeAgo($item->start_time, "H:i d/m") ?>
                <?php else : ?>
                    <?php echo $item->score_home . " - " . $item->score_away . ($item->match_status === "live" ? "({$item->elapsed})" : '') ?>
                <?php endif; ?>
            </div>
        </td>
        <td class="team text-left" style="width: 40%;">
            <?php echo getLogoClub($item->away_id, $item->name_away, "mr-2 logo") ?>
            <?php echo $item->name_away ?>
        </td>
    </tr>
<?php endforeach; ?>