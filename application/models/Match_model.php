<?php
class Match_model extends STEVEN_Model
{
    public $table;
    public $table_tournament;
    public function __construct()
    {
        parent::__construct();
        $this->table = "match";
        $this->table_tournament = "tournament";
        $this->column_order = array("$this->table.id", "$this->table.id", "$this->table.is_status", "$this->table.created_time", "$this->table.updated_time");
        $this->column_search = array('name_home', 'name_away');
        $this->order_default = array("$this->table.created_time" => 'desc');
    }

    public function _where_custom($args = array())
    {
        parent::_where_custom();
        extract($args);

        if (empty($select)) {
            $this->db->select("*");
        }
        $this->db->select("CONCAT('Link xem Trực Tiếp ',name_home,' vs ',name_away, ' lúc ',DATE_FORMAT(start_time,'%H:%i %d/%m/%Y')) AS title");

        if (!empty($tournament_id)) {
            $this->db->where_in("tournament_id", $tournament_id);
        }

        if (!empty($sport_id)) {
            $this->db->where("sport_id", $sport_id);
        }

        if (!empty($is_link)) {
            $this->db->where("$this->table.data_link !=", '', TRUE);
        }

        if (!empty($search_match)) {
            $this->db->group_start();
            $this->db->like("name_home", $search_match);
            $this->db->or_like("name_away", $search_match);
            $this->db->group_end();
        }

        if (!empty($start_time_elder)) {
            $this->db->where('unix_timestamp(start_time) <= ', strtotime("+$start_time_elder days"));
        }

        if (!empty($start_time)) {
            $this->db->where('date(start_time)', $start_time);
        }

        if (!empty($start_time_new)) {
            //$this->db->where('status','-');
            $this->db->where('match_status', "notstarted");
            $this->db->where('start_time >= ', date('Y-m-d H:i'), true);
        }

        if (!empty($playing)) {
            $this->db->where('match_status', "inprogress");
            //$this->db->where('elapsed',0);
            $this->db->where("unix_timestamp(start_time) >=", strtotime('-3 hour'));
        }

        if (!empty($playing_and_start)) {
            $this->db->where_in('match_status', ['notstarted', 'inprogress']);
            //$this->db->where('elapsed',0);
            $this->db->where("unix_timestamp(start_time) >=", strtotime('-3 hour'));
        }

        if (!empty($start_time_hot)) {
            $this->db->where_not_in('status', ['AET', 'FT', 'Canceled']);
            //$this->db->where('elapsed',0);
            //$this->db->where('start_time <= ',date('Y-m-d H:i:s',strtotime('+1 hour')),true);
            //$this->db->where('start_time >= ',date('Y-m-d H:i:s',strtotime('-2 hour')),true);
        }

        if (!empty($finished)) {
            $this->db->where('match_status', "finished");
            //$this->db->where('start_time < ',date('Y-m-d H:i:s'),true);
        }

        if (!empty($is_display)) {
            $this->db->where('is_display', $is_display);
        }
    }

    public function getTournamentIdFeatured($updateCache = false)
    {
        $key = $this->table . "_getTournamentIdFeatured";
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $this->db->select('tournament_id');
            $this->db->from($this->table_tournament);
            $this->db->where([
                'is_featured' => 1
            ]);
            $this->db->order_by("order", "ASC");
            $this->db->order_by("tournament_id", "ASC");
            $result = $this->db->get()->result();
            $data = '';
            if (!empty($result)) foreach ($result as $key => $item) {
                if ($key != 0) $data .= ',';
                $data .= $item->tournament_id;
            }
            $this->setCache($key, $data);
        }
        return $data;
    }
    public function getSelect2Match($id)
    {
        $this->db->select("match_id AS id, title AS text");
        $this->db->from($this->table);
        $this->db->where($this->table . ".match_id", $id);
        $data = $this->db->get()->result();
        return $data;
    }

    public function getDataMatch($params = [], $updateCache = false)
    {
        $key = "match_data_" . md5(serialize($params));
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            if (!isset($params['tournament_id'])) $params['tournament_id'] = $this->getTournamentIdFeatured();
            $result = $this->getDataApi('match/get_match', $params);
            $data = !empty($result->data) ? $result->data : NULL;
            $this->setCache($key, $data, 30 * 60);
        }
        return $data;
    }

    public function getDataMatchOdds($params = [], $updateCache = false)
    {
        $key = "match_data_odds_" . md5(serialize($params));
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            if (!isset($params['tournament_id'])) $params['tournament_id'] = $this->getTournamentIdFeatured();
            $result = $this->getDataApi('match/get_schedule_odds', $params);
            $data = !empty($result->data) ? $result->data : NULL;
            $this->setCache($key, $data, 10);
        }
        return $data;
    }

    public function getHot($params = [], $updateCache = false)
    {
        $key = "match_hot_" . md5(serialize($params));
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            if (empty($params['tournament_id'])) $params['tournament_id'] = $this->getTournamentIdFeatured();
            $result = $this->getDataApi('match/get_schedule_hot', $params);
            $data = !empty($result->data) ? $result->data : NULL;
            $this->setCache($key, $data, 30 * 60);
        }
        return $data;
    }

    public function getSchedule($params = [], $updateCache = false)
    {

        $key = "match_schedule_" . md5(serialize($params));
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            if (!isset($params['tournament_id'])) $params['tournament_id'] = $this->getTournamentIdFeatured();
            if ($params['tournament_id'] === '') unset($params['tournament_id']);
            $result = $this->getDataApi('match/get_schedule', $params);
            $listMatch = !empty($result->data) ? $result->data : NULL;
            if (!empty($listMatch)) {
                $data = array_group_by($listMatch, function ($i) {
                    return $i['tournament_id'];
                });
            }
            $this->setCache($key, $data, 5 * 60);
        }
        return $data;
    }

    public function getScheduleOdds($params = [], $updateCache = false)
    {
        $key = "match_schedule_odds_" . md5(serialize($params));
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $result = $this->getDataApi('match/get_schedule_odds', $params);
            $data = !empty($result->data) ? $result->data : NULL;
            $this->setCache($key, $data, 5 * 60);
        }
        return $data;
    }

    public function getResult($params = [], $updateCache = false)
    {
        $key = "match_result_" . md5(serialize($params));
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            if (!isset($params['tournament_id'])) $params['tournament_id'] = $this->getTournamentIdFeatured();
            if ($params['tournament_id'] === '') unset($params['tournament_id']);
            $result = $this->getDataApi('match/get_result', $params);
            $listMatch = !empty($result->data) ? $result->data : NULL;
            if (!empty($listMatch)) {
                $data = array_group_by($listMatch, function ($i) {
                    return $i['tournament_id'];
                });
            }
            $this->setCache($key, $data, 5 * 60);
        }
        return $data;
    }

    public function getLive($limit = false, $is_play = false, $updateCache = false)
    {
        $key = "match_live_{$limit}_{$is_play}";
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $params = [
                'limit' => $limit,
                'is_play' => $is_play
            ];
            $result = $this->getDataApi('match/get_schedule_live', $params);
            $data = !empty($result->data) ? $result->data : NULL;
            $this->setCache($key, $data, 5 * 60);
        }
        return $data;
    }

    public function getById($id, $updateCache = false)
    {
        $key = "match_byid_{$id}";
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $params = [
                'id' => $id,
            ];
            $result = $this->getDataApi('match/get_by_id', $params);
            $data = !empty($result->data) ? $result->data : null;
            $this->setCache($key, $data, 60 * 60);
        }
        return $data;
    }

    public function getH2H($ids, $updateCache = false)
    {
        $key = "get_h2h_{$ids}";
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $params = [
                'ids' => $ids,
            ];
            $result = $this->getDataApi('match/get_h2h', $params);
            $data = !empty($result) ? $result : null;
            $this->setCache($key, $data, 60 * 60);
        }
        return $data;
    }

    public function getLast10($ids, $updateCache = false)
    {
        $key = "get_last10_{$ids}";
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $params = [
                'ids' => $ids,
            ];
            $result = $this->getDataApi('match/get_last10', $params);
            $data = !empty($result) ? $result : null;
            $this->setCache($key, $data, 60 * 60);
        }
        return $data;
    }


    public function getMatchOdds($id, $updateCache = false)
    {
        $key = "match_odds_byid_{$id}";
        $data = $this->getCache($key);
        if ($data === false || $updateCache == true) {
            $params = [
                'in_match_id' => $id,
            ];
            $result = $this->getDataApi('match/get_match_odds', $params);
            $data = !empty($result->data) ? $result->data : null;
            $this->setCache($key, $data, 1 * 60);
        }
        return $data;
    }

    public function updateDataLink($id, $data_link)
    {
        $params = [
            'id' => $id,
            'type_link' => 'knc',
            'data_link_natra' => urlencode($data_link)
        ];
        $data = $this->getDataApi(API_DATACENTER . "/api/v1/match/save_match_link", $params);
        return $data;
    }

    public function getTyLeKeoPC($update_cache = false)
    {
        $keyCache = '_getTyLeKeoPC_';
        $data = $this->getCache($keyCache);
        if ($data === false || $update_cache == true) {
            $params['meta_key'] = "tylekeo_pc";
            $result = $this->getDataApi('page/get_by_key', $params);
            $data = !empty($result->data) ? $result->data->meta_value : null;
            $this->setCache($keyCache, $data, 10);
        }

        return $data;
    }

    public function getTyLeKeoMobile($update_cache = false)
    {
        $keyCache = '_getTyLeKeoMobile_';
        $data = $this->getCache($keyCache);
        if ($data === false || $update_cache == true) {
            $params['meta_key'] = "tylekeo_mobile";
            $result = $this->getDataApi('page/get_by_key', $params);
            $data = !empty($result->data) ? $result->data->meta_value : null;
            $this->setCache($keyCache, $data, 10);
        }

        return $data;
    }
}
