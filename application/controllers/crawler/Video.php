<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

class Video extends Crawler_Controller
{
    private $_category;
    private $_post;
    private $_base_url;
    private $_client;
    private $_list_cate;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['category_model', 'post_model']);
        $this->_category = new Category_model();
        $this->_post = new Post_model();
        $this->_client = new Client(HttpClient::create(['timeout' => 60]));
        $this->_base_url = 'https://video.thethaovanhoa.vn';
        $this->_list_cate = [
          'https://video.thethaovanhoa.vn/ajax/video-list-v2/0-' => 6
        ];
    }

    public function start()
    {
        foreach ($this->_list_cate as $slug => $cate_id) {
            for ($i = 160; $i > 0; $i--) {
                $this->getVideo($slug . $i . '.htm', $cate_id, "video");
            }
        }
        die("END - Crawler from site {$this->_base_url}\n");
    }

    public function getVideo($url, $category_id, $type = 'video')
    {
        try {
            $bot_crawler = $this->_client->request('GET', $url);
            echo "Start Crawler article from category => {$bot_crawler->getUri()} \n";
            $listDetail = $bot_crawler->filter('li > a')->each(function ($node) {
                return $this->_base_url . $node->attr("href");
            });
            $listDetail = array_reverse($listDetail);
            if (!empty($listDetail)) {
                foreach ($listDetail as $linkDetail) {
                    $bot_crawler_detail = $this->_client->request('GET', $linkDetail);
                    $data['title'] = $data['meta_title'] = $bot_crawler_detail->filter("h2")->text();
                    $data['slug'] = $this->toSlug($data['title']);
                    if (!$this->_post->checkExistByField("slug", $data['slug'])) {
                        echo "------Crawl link video from => $linkDetail ------- \n";
                        $data['description'] = $data['meta_description'] = $bot_crawler_detail->filter('meta[property="og:description"]')->attr("content");

                        if ($bot_crawler_detail->filter('#video-frame iframe')->count() > 0) {
                            $data['video'] = $bot_crawler_detail->filter('#video-frame iframe')->attr('src');
                        } else {
                            $data['video'] = 'https://hls.mediacdn.vn/'. $bot_crawler_detail->filter('#video-frame .VCSortableInPreviewMode')->attr('data-vid');
                        }

                        if ($bot_crawler_detail->filter('meta[property="og:image"]')->count() > 0) {
                            $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'), '', 'video');
                        } else {
                            $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('#divcontentwrap img', 0)->attr('src'), '', 'video');
                        }

                        $data['type'] = $type;
                        $data['is_robot'] = 1;
                        $data['user_id'] = 1;

                        if ($id = $this->_post->save($data)) {
                            $data_category = [
                              "post_id" => $id,
                              'category_id' => $category_id,
                              'is_primary' => false
                            ];
                            if ($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)) {
                                echo "Update category bài viết $id thành công ! \n";
                            }
                            echo "Thêm bài viết $id thành công ! \n";
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }

    private function getCategory($url, $category_id, $type = 'video')
    {
        try {
            $bot_crawler = $this->_client->request('GET', $url);
            echo "Start Crawler article from category => {$bot_crawler->getUri()} \n";
            $listDetail = $bot_crawler->filter('.ovh.lst > ul > li > a')->each(function ($node) {
                return $this->_base_url . $node->attr("href");
            });
            $listDetail = array_reverse($listDetail);
            if (!empty($listDetail)) {
                foreach ($listDetail as $linkDetail) {
                    $bot_crawler_detail = $this->_client->request('GET', $linkDetail);
                    $data['title'] = $data['meta_title'] = $bot_crawler_detail->filter("h1")->text();
                    $data['slug'] = $this->toSlug($data['title']);
                    if (!$this->_post->checkExistByField("slug", $data['slug'])) {
                        echo "---------Crawl content from => $linkDetail ----------------- \n";
                        $data['description'] = $data['meta_description'] = $bot_crawler_detail->filter('meta[property="og:description"]')->attr("content");
                        $content = $bot_crawler_detail->filter('#divcontentwrap')->html();
                        if (!empty($content)) {
                            $content = $this->cleanContent($content);
                            $content = str_replace("(adsbygoogle = window.adsbygoogle || []).push({});", '', $content);
                            $imgs = $bot_crawler_detail->filter('#divcontentwrap img')->each(function ($node) {
                                if (preg_match('/http/i', $node->attr('src'))) {
                                    $imgSrc = $node->attr('src');
                                } else {
                                    $imgSrc = $this->_base_url . $node->attr('src');
                                }
                                $imgSrc = strtok($imgSrc, '?');
                                $srcNew = $this->downloadImage($imgSrc, '', 'video');
                                return [
                                  'old' => $node->attr('src'),
                                  'new' => "/" . MEDIA_NAME . $srcNew
                                ];
                            });
                            if (!empty($imgs)) {
                                foreach ($imgs as $img) {
                                    $content = str_replace($img['old'], $img['new'], $content);
                                }
                            }

                            $data['content'] = $content;

                            if ($bot_crawler_detail->filter('meta[property="og:image"]')->count() > 0) {
                                $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'), '', 'video');
                            } else {
                                $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('#divcontentwrap img', 0)->attr('src'), '', 'video');
                            }

                            $data['type'] = $type;
                            $data['is_robot'] = 1;
                            $data['user_id'] = 1;


                            if ($id = $this->_post->save($data)) {
                                $data_category = [
                                  "post_id" => $id,
                                  'category_id' => $category_id,
                                  'is_primary' => false
                                ];
                                if ($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)) {
                                    echo "Update category bài viết $id thành công ! \n";
                                }
                                echo "Thêm bài viết $id thành công ! \n";
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
}

