<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Match extends Admin_Controller
{
    protected $_data;
    protected $_tournament;

    public function __construct()
    {
        parent::__construct();
        //tải thư viện
        $this->load->model(['match_model','tournament_model']);
        $this->_data = new Match_model();
        $this->_tournament = new Tournament_model();
    }

    public function get_list($data){
        $data['main_content'] = $this->load->view(TEMPLATE_PATH . $this->_controller . '/' . 'index', $data, TRUE);
        $this->load->view(TEMPLATE_MAIN, $data);
    }

    public function index(){
        $data['heading_title'] = "Danh sách trận đấu";
        $data['heading_description'] = "Danh sách trận đấu";
        $this->get_list($data);
    }

    public function ajax_list(){
        $this->checkRequestPostAjax();
        $data = array();
        $pagination = $this->input->post('pagination');
        $page = $pagination['page'];
        $total_page = isset($pagination['pages']) ? $pagination['pages'] : 1;
        $limit = !empty($pagination['perpage']) && $pagination['perpage'] > 0 ? $pagination['perpage'] : 1;

        $queryFilter = $this->input->post('query');
        $params = [
            'search_match'     => !empty($queryFilter['generalSearch']) ? $queryFilter['generalSearch'] : '',
            'tournament_id'     => !empty($queryFilter['tournament_id']) ? $queryFilter['tournament_id'] : '',
            'page'          => $page,
            'limit'         => $limit,
        ];
        $status = !empty($queryFilter['is_status']) ? $queryFilter['is_status'] : '';
        switch ($status){
            case 1: $params = array_merge($params,[
                'start_time_new' => true,
                'order'         => ['start_time' => 'ASC']
            ]); break;
            case 2: $params = array_merge($params,[
                'playing' => true,
                'order'         => ['start_time' => 'ASC']
            ]); break;
            case 3: $params = array_merge($params,[
                'start_time' => date('Y-m-d'),
                'order'         => ['start_time' => 'ASC']
            ]); break;
            case 4: $params = array_merge($params,[
                'finished' => true,
                'order'         => ['start_time' => 'DESC']
            ]); break;
            default: $params = array_merge($params,[
                'playing_and_start' => true,
                'order'         => ['start_time' => 'ASC']
            ]);
        }
        if(isset($queryFilter['filter_date']) && $queryFilter['filter_date'] !== ''){
            $params = array_merge($params,[
                'start_time' => $queryFilter['filter_date'],
                'order'         => ['start_time' => 'ASC']
            ]);
        }

        $listData = $this->_data->getData($params);

        if(!empty($listData)) foreach ($listData as $item) {
            $tournament = $this->_tournament->getByTourId($item->tournament_id);
            $row = array();
            $row['checkID'] = $item->id;
            $row['id'] = $item->id;
            $tournament_name = !empty($tournament->title)? $tournament->title : '';
            $row['title'] = "(<strong>$tournament_name</strong>) <br> $item->title";
            $row['score_home'] = $item->score_home;
            $row['score_away'] = $item->score_away;
            $row['elapsed'] = $item->elapsed;
            $row['status'] = $item->status;
            $row['match_status'] = $item->match_status;
            $row['start_date'] = date('H:i d/m',strtotime($item->start_time));
            $link = json_decode($item->data_link);
            if (gettype($link) == 'array') $link = implode("\n",$link);
            $row['link'] = !empty($link) ? $link : '';
            $data[] = $row;
        }

        $output = [
            "meta" => [
                "page"      => $page,
                "pages"     => $total_page,
                "perpage"   => $limit,
                "total"     => $this->_data->getTotal($params),
                "sort"      => "asc",
                "field"     => "id"
            ],
            "data" =>  $data
        ];

        $this->returnJson($output);
    }

    public function ajax_load($type = ''){
        $term = $this->input->get("q");
        $id = $this->input->get('id')?$this->input->get('id'):0;
        if(empty($type)) $this->session->userdata('type');
        $params = [
            'type' => !(empty($type)) ? $type : null,
            'is_status'=> 1,
            'limit'=> 2000
        ];
        $list = $this->_data->getData($params);
        if($type === 'brand'){
            $listTree = $list;
        }else{
            $this->_queue_select($list);
            $listTree = $this->category_tree;
            if(!empty($term)){
                $searchword = $term;
                $matches = array();
                foreach($listTree as $k=>$v) {
                    if(preg_match("/\b$searchword\b/i", $v['title'])) {
                        $matches[$k] = $v;
                    }
                }
                $listTree = $matches;
            }
        }
        $output = [];
        if(!empty($listTree)) foreach ($listTree as $item) {
            $item = (object) $item;
            $output[] = ['id'=>$item->id, 'text'=>$item->title];
        }
        $this->returnJson($output);
    }
    
    public function ajax_add(){
        $this->checkRequestPostAjax();
        $data = $this->_convertData();
        if($id = $this->_data->save($data)){
            $note   = 'Thêm category có id là : '.$id;
            $this->addLogaction('category',$data,$id,$note,'Add');
            $message['type'] = 'success';
            $message['message'] = "Thêm mới thành công !";
        }else{
            $message['type'] = 'error';
            $message['message'] = "Thêm mới thất bại !";
        }
        $this->returnJson($message);
    }
    
    public function ajax_edit(){
        $this->checkRequestPostAjax();
        $id = $this->input->post('id');
        if(!empty($id)){
            $output['data_info'] = $this->_data->single(['id' => $id]);
//            $output['data_category'] = $this->_data->getSelect2($output['data_info']->parent_id);
            $this->returnJson($output);
        }
    }

    public function ajax_update(){
        $this->checkRequestPostAjax();
        $data = $this->_convertData();
        $id = $data['id'];
        $data_old = $this->_data->single(['id' => $id],$this->_data->table);
        if($this->_data->update(['id' => $id],$data, $this->_data->table)){
            $note   = 'Update category có id là : '.$id;
            $this->addLogaction('category',$data_old,$id,$note,'Update');
            $message['type'] = 'success';
            $message['message'] = "Cập nhật thành công !";
        }else{
            $message['type'] = 'error';
            $message['message'] = "Cập nhật thất bại !";
        }
        $this->returnJson($message);
    }

    public function ajax_update_field(){
        $this->checkRequestPostAjax();
        $id = $this->input->post('id');
        $field = $this->input->post('field');
        $value = $this->input->post('value');
        if ($field == 'order') {
            $check_order = $this->_data->check_order($value);
            if (!empty($check_order)) {
                $message['type'] = 'error';
                $message['message'] = "Thứ tự đã tồn tại, vui lòng thử thứ tự khác";
                $this->returnJson($message);
            }
        }
        
        $response = $this->_data->update(['id' => $id], [$field => $value]);
        if($response != false){
            if($field === 'data_link'){
                $oneMatch = $this->_data->single(['id' => $id]);
                $resApi = $this->_data->updateDataLink($oneMatch->match_id,$value);
            }
            $message['type'] = 'success';
            $message['message'] = "Cập nhật thành công !";
            $message['resApi'] = $resApi;
        }else{
            $message['type'] = 'error';
            $message['message'] = "Cập nhật thất bại !";
        }
        $this->returnJson($message);
    }

    public function ajax_delete(){
        $this->checkRequestPostAjax();
        $ids = (int)$this->input->post('id');
        $response = $this->_data->deleteArray('id',$ids);
        if($response != false){
            $message['type'] = 'success';
            $message['message'] = "Xóa thành công !";
        }else{
            $message['type'] = 'error';
            $message['message'] = "Xóa thất bại !";
            log_message('error',$response);
        }
        $this->returnJson($message);
    }

    private function _validation(){
        $this->checkRequestPostAjax();
        $rules = [
            [
                'field' => "title",
                'label' => "Tiêu đề",
                'rules' => "trim|required"
            ],[
                'field' => "slug",
                'label' => "Đường dẫn",
                'rules' => "trim|required"
            ]
        ];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $message['type'] = "warning";
            $message['message'] = "Vui lòng kiểm tra lại thông tin vừa nhập.";
            $valid = array();
            if(!empty($rules)) foreach ($rules as $item){
                if(!empty(form_error($item['field']))) $valid[$item['field']] = form_error($item['field']);
            }
            $message['validation'] = $valid;
            $this->returnJson($message);
        }
    }

    private function _convertData(){
        $this->_validation();
        $data = $this->input->post();
        if(!empty($data['is_status'])) $data['is_status'] = 1;else $data['is_status'] = 0;
        return $data;
    }
}