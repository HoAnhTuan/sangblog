<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <?php $this->load->view(TEMPLATE_PATH . 'seo/meta_seo') ?>
    <link rel="shortcut icon" type="image/png" href="<?php echo $this->_settings->favicon ? getImageThumb($this->_settings->favicon, 32, 32) : site_url('public/favicon.ico') ?>">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.2.0/css/all.css">
    <!--<link rel="stylesheet" href="--><?php //echo TEMPLATE_ASSET . 'css/all_minify.min.css?v='.ASSET_VERSION 
                                        ?>
    <!--" >-->
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'css/swiper.min.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'css/bootstrap.min.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'css/custom.css?v=2' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'plugins/jssocials/jssocials.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'plugins/jssocials/jssocials-theme-flat.css?v=' . ASSET_VERSION ?>">
    <?php echo getSetting("data_seo", "style") ?>
    <?php echo getSetting("data_seo", "script") ?>

    <?php if ($this->_method === 'detail' && ($this->_controller === 'match' || $this->_controller === 'post')) : ?>
        <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/custom_match.js?v=' . ASSET_VERSION ?>"></script>
    <?php endif; ?>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="<?php echo $this->_controller; ?>">
    <div id="fb-root"></div>
    <script type='text/javascript'>
        const base_url = '<?php echo base_url(); ?>',
            media_url = '<?php echo MEDIA_URL . '/'; ?>',
            data_banner = <?php echo showImageBanner() ?>;
    </script>
    <!--Popup Ads-->
    <div class="modal fade" data-backdrop="static" id="ads_popup" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content  text-center bg-transparent">
                <div class="modal-body p-0">
                    <button type="button" class="close" data-dismiss="modal" style="
                        position: absolute;
                        top: -17px;
                        background-color: white;
                        padding: 5px 10px;
                        left: 50%;
                        transform: translateX(-50%);
                        opacity: 1;
                        border-radius: 50%;">&times;
                    </button>
                    <?php echo showContainerBanner('popup', 2, '') ?>
                    <?php echo showContainerBanner('popup', 1, '') ?>
                </div>
            </div>
        </div>
    </div>
    <!--Popup Ads-->
    <?php $this->load->view(TEMPLATE_PATH . '_header') ?>
    <!--banner-->
    <aside class="container">
        <div class="row">
            <?php echo showContainerBanner('catfish', 1, 'col-12 my-2 my-md-0') ?>
        </div>
    </aside>
    <?php $this->load->view(TEMPLATE_PATH . 'block/banner_fixed') ?>

    <?php echo !empty($main_content) ? $main_content : '' ?>

    <div id="ackIframe"> </div>

    <!-- Banner Sticky bottom -->
    <?php echo showContainerBanner('footer', 0, 'sticky-banner-bottom text-center') ?>

    <?php echo $this->load->view(TEMPLATE_PATH . '_footer', null, TRUE); ?>

    <!-- <div class="BannerFixed BannerFixedLeft d-none d-md-block">
        <div class="BannerFixedLeft_content">
            <div class="d-block mb-10">
                <?php //echo showContainerBanner('fixed_left_bottom', 2, '') ?>
            </div>
            <div class="close-banner" onclick="this.closest('.BannerFixed').remove();"><i class="fa fa-times-circle"></i></div>
        </div>
    </div>
    <div class="BannerFixed BannerFixedRight d-none d-md-block">
        <div class="BannerFixedRight_content">
            <div class="d-block mb-10">
                <?php //echo showContainerBanner('fixed_right_bottom', 2, '') ?>
            </div>
            <div class="close-banner" onclick="this.closest('.BannerFixed').remove();"><i class="fa fa-times-circle"></i></div>
        </div>
    </div> -->

<!--    <div class="BannerFixed BannerFixedCenter d-none d-md-block">-->
<!--        <div class="BannerFixedCenter_content">-->
<!--            <div class="d-block mb-10">-->
<!--                --><?php //echo showContainerBanner('home_post_bottom', 2, '') ?>
<!--            </div>-->
<!--            <div class="close-banner" onclick="this.closest('.BannerFixed').remove();"><i class="fa fa-times-circle"></i></div>-->
<!--        </div>-->
<!--    </div>-->
    <!-- catfish show for PC and MB -->
    <div class="banner_mobile ads text-center fixed-bottom">
        <?php echo showContainerBanner('banner_catfish_mobile', 0, '') ?>
        <div class="close-banner-mobile" onclick="this.closest('.banner_mobile').remove();"><i class="fa fa-times-circle"></i></div>
    </div>

    <!--Wrapper End-->
    <!--<script defer type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'js/all_minify.min.js?v='.ASSET_VERSION 
                                                        ?>
    <!--"></script>-->
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/jquery.js?v=' . ASSET_VERSION ?>"></script>
    <!--<script defer type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'js/jquery.sticky-kit.min.js?v='.ASSET_VERSION 
                                                        ?>
    <!--"></script>-->
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/bootstrap.js?v=' . ASSET_VERSION ?>"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/swiper.min.js?v=' . ASSET_VERSION ?>"></script>
    <!--<script defer type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'plugins/jssocials/jssocials.min.js?v='.ASSET_VERSION 
                                                        ?>
    <!--"></script>-->
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/custom.js?v=1' . ASSET_VERSION ?>"></script>
    <?php if ($this->_controller === 'match' && $this->_method === 'detail') : ?>
        <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/circle_process.js?v=' . ASSET_VERSION ?>"></script>
    <?php endif; ?>
</body>

</html>