<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Public_Controller
{
  protected $_category;
  protected $_post;
  protected $_match;
  protected $_tournament;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(['category_model', 'post_model', 'match_model', 'tournament_model']);
    $this->_match = new Match_model();
    $this->_post = new Post_model();
    $this->_category = new Category_model();
    $this->_tournament = new Tournament_model();
  }

  public function index()
  {
    $this->setCacheFile(8);

    $drag = getDrag('keo_noi_bat');
    $listId = [];
    foreach ($drag as $item) {
      array_push($listId, $item->id);
    }
    $temp = $this->_post->getDataFE([
      'type' => 'soikeo',
      'in' => $listId,
      'limit' => 50,
      'is_primary_category' => true,
      'order' => ['start_time' => 'ASC']
    ]);

    $data['keo_noi_bat'] = [];
    foreach ($drag as $d) {
      foreach ($temp as $list) {
        if ($d->id == $list->id) {
          array_push($data['keo_noi_bat'], $list);
        }
      }
    }

    $data['tin_soi_keo'] = $this->_post->getDataFE([
      'category_id' => 2,
      'type' => 'soikeo',
      // 'slug' => 'soi-keo',
      // 'is_start_time' => 1,
      'limit' => 15,
      'order' => ['id' => 'ASC']
    ]);

    $drag = getDrag('nhacai');
    $temp = $this->_post->getDataFE([
      'type' => 'nhacai',
      'in' => array_map(function ($item) {
        return (int)$item->id;
      }, $drag),
      'limit' => 5,
      'is_primary_category' => true,
      'order' => ['order' => 'ASC']
    ]);
    $data['nha_cai'] = [];
    foreach ($drag as $d) {
      foreach ($temp as $list) {
        if ($d->id == $list->id) {
          array_push($data['nha_cai'], $list);
        }
      }
    }

    $data['soikeo_post'] = $this->_post->getDataFE([
      'category_id' => 2,
      'limit' => 6,
      'order' => ['displayed_time' => 'DESC']
    ]);

    $data['nhandinh_post'] = $this->_post->getDataFE([
      'category_id' => 1,
      'order' => ['displayed_time' => 'DESC']
    ]);

    //    $data['nha_cai'] = $this->_post->getDataFE([
    //      'type' => 'nhacai',
    //      'limit' => 6,
    //      'order' => ['order' => 'ASC']
    //    ]);

    $odds = $this->_match->getDataMatchOdds();
    if (!empty($odds)) {
      $data_match_group = array_group_by($odds, function ($i) {
        return $i['tournament_id'];
      });
      $data['match_odds'] = $data_match_group;
    } else {
      $data['match_odds'] = [];
    }
    $data['SEO'] = [
      'meta_title' => !empty($this->_settings->meta_title) ? $this->_settings->meta_title : '',
      'meta_description' => !empty($this->_settings->meta_description) ? $this->_settings->meta_description : '',
      'meta_keyword' => !empty($this->_settings->meta_keyword) ? $this->_settings->meta_keyword : '',
      'url' => base_url(),
      'is_robot' => true,
      'image' => !empty($this->_settings->thumbnail) ? getImageThumb($this->_settings->thumbnail, 600, 314) : '',
    ];
    $data['main_content'] = $this->load->view(TEMPLATE_PATH . 'home/index', $data, TRUE);
    $this->load->view(TEMPLATE_MAIN, $data);
  }
}
