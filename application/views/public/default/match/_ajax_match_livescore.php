<?php
if(!empty($data_match)){
    $data_match_group = array_group_by($data_match, function ($i) {
        return $i['tournament_id'];
    });
}

?>

<?php if(!empty($data_match_group)) foreach ($data_match_group as $tournament_name => $matches):?>
    <div class="bg-main py-2 px-3 text-center text-white text-capitalize">
        <?php echo $tournament_name ?>
    </div>
    <div class="overflow-auto">
        <table class="table table-striped">
            <tbody>
            <?php $this->load->view(TEMPLATE_PATH . "match/_data_match_livescore", ['data_match' => $matches]) ?>
            </tbody>
        </table>
    </div>
<?php endforeach; ?>