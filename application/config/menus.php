<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config['cms_menu'] = [
    'Main menu',
    'Menu top',
    'Menu footer',
    'Widget liên kết hữu ích',
    'Widget giải đấu',
    'Menu page footer',
    'Menu middle',
    'Mobile sticky bottom',
    'Main menu mobile'
];
