<?php if (!empty($oneItem)) :; ?>
	<main class="main-content main-detail" data-url="<?php echo getUrlPost($oneItem) ?>">
		<div class="container">
			<div class="row">
				<?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
				<article class="col-12 col-lg-9 border-radius">
					<div class="bg-gray post-content p-2">
						<h1 class="font-23 normal-title">
							<?php echo $oneItem->title; ?>
						</h1>
						<div class="d-flex flex-wrap justify-content-between my-3">
							<div class="font-13 text-secondary">
								<i class="far fa-clock mr-1"></i>
								<?php echo timeAgo($oneItem->displayed_time, "H:i") ?> <?php echo date_post_vn($oneItem->displayed_time) ?>

								<?php if (!empty($category)) : ?>
									<a class="text-secondary" href="<?= base_url("$category->slug.html") ?>"><i class="far fa-folder-open mr-1 ml-3"></i><?= $category->title ?></a>
								<?php endif; ?>
							</div>
						</div>
						<div class="font-14 font-weight-bold">
							<?php echo $oneItem->description ?>
						</div>
						<div class="content-news text-justify">
							<?php
							echo getTableOfContent($oneItem->content, $oneItem->title);
							?>
						</div>
					</div>
					<?php $this->load->view(TEMPLATE_PATH . '/block/author') ?>
					<?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer', ['title' => 'nhà cái khác', 'news_soccer' => $related_post ?? '']) ?>
				</article>
				<div class="col-12 col-lg-3">
					<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_news') ?>
					<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
				</div>
			</div>
		</div>
	</main>
<?php endif; ?>