<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Result extends Public_Controller
{
    protected $_data_result;
    protected $_data_category;
    public function __construct(){
        parent::__construct();
        $this->load->model(["Result_model","Cateresult_model"]);
        $this->_data_result = new Result_model();
        $this->_data_category = new Cateresult_model();
    }

	public function category($id) {
        $timeCache = !in_array(date('G'),[15,16,17,18])? 60*60: 60;
        $data['oneItem'] = $oneItem = $this->_data_category->getByIdCached($id);
        if (empty($oneItem) || $oneItem->type != 'result' || !in_array(strtoupper($oneItem->code),['XSMB','XSMT','XSMN'])) show_404();

        if ($oneItem->id != $oneItem->parent_id) {
            $oneParent = $this->_data_category->getByIdCached($oneItem->parent_id);
        } else {
            $oneParent = $oneItem;
        }
        $limit = 7;
        $data['oneParent'] = $oneParent;
        if (empty($page = $this->input->get('page'))) $page = 1;
        $api = $this->_data_result->getDataByCategory([
            'api_id' => $oneItem->api_id,
            'limit' => $limit,
            'page' => $page
        ], $timeCache);
        if (in_array(strtoupper($oneItem->code),['XSMT','XSMN']))
            $data['data'] = $this->array_group_by($api['data'],function ($i){
                return $i['displayed_time'];
            });
        else
            $data['data'] = $api['data'];

        if (strtoupper($oneItem->code) == 'XSMB') $view = '_result_mb';
        elseif (in_array(strtoupper($oneItem->code),['XSMT','XSMN'])) $view = '_result_mtmn';
        else $view = "_result_tinh";
        $data['result'] = $this->load->view(TEMPLATE_PATH . 'result/'.$view, $data, TRUE);
        $data['limit'] = $limit;

        $this->breadcrumbs->push('Trang chủ', base_url());
        $this->breadcrumbs->push($oneItem->title, getUrlCateResult($oneItem));
        $data['breadcrumb'] = $this->breadcrumbs->show();

        $data['SEO'] = [
            'meta_title' => $oneItem->title,
            'meta_description' => $oneItem->description,
            'meta_keyword' => $oneItem->meta_keyword,
            'image' => !empty($oneItem->thumbnail)? getImageThumb($oneItem->thumbnail, 470, 246): '',
            'url' => getUrlCateResult($oneItem)
        ];

        $data['main_content']  = $this->load->view(TEMPLATE_PATH . 'result/category', $data, TRUE);
        $this->load->view(TEMPLATE_MAIN, $data);
	}
	public function categorydow($dow,$id) {
        $timeCache = !in_array(date('G'),[15,16,17,18])? 60*60: 60;
        $data['oneItem'] = $oneItem = $this->_data_category->getByIdCached($id);
        if (empty($oneItem) || $oneItem->type != 'result' || !in_array(strtoupper($oneItem->code),['XSMB','XSMT','XSMN'])) show_404();

        if ($oneItem->id != $oneItem->parent_id) {
            $oneParent = $this->_data_category->getByIdCached($oneItem->parent_id);
        } else {
            $oneParent = $oneItem;
        }
        $data['oneParent'] = $oneParent;
        $limit = 7;
        switch ($dow){
            case 'thu-2': $dayofweek = 1; $titleSeo = "thứ 2"; break;
            case 'thu-3': $dayofweek = 2; $titleSeo = "thứ 3"; break;
            case 'thu-4': $dayofweek = 3; $titleSeo = "thứ 4"; break;
            case 'thu-5': $dayofweek = 4; $titleSeo = "thứ 5"; break;
            case 'thu-6': $dayofweek = 5; $titleSeo = "thứ 6"; break;
            case 'thu-7': $dayofweek = 6; $titleSeo = "thứ 7"; break;
            case 'chu-nhat': $dayofweek = 0; $titleSeo = "chủ nhật"; break;
        }
        if (!isset($dayofweek)) show_404();

        if (empty($page = $this->input->get('page'))) $page = 1;
        $api = $this->_data_result->getDataByCategoryDayOfWeek([
            'api_id' => $oneItem->api_id,
            'limit' => $limit,
            'page' => $page,
            'dayofweek' => $dayofweek
        ], $timeCache);

        if (in_array(strtoupper($oneItem->code),['XSMT','XSMN']))
            $data['data'] = $this->array_group_by($api['data'],function ($i){
                return $i['displayed_time'];
            });
        else
            $data['data'] = $api['data'];

        if (strtoupper($oneItem->code) == 'XSMB') $view = '_result_mb';
        elseif (in_array(strtoupper($oneItem->code),['XSMT','XSMN'])) $view = '_result_mtmn';
        else $view = "_result_tinh";
        $data['result'] = $this->load->view(TEMPLATE_PATH . 'result/'.$view, $data, TRUE);
        $data['limit'] = $limit;

        $this->breadcrumbs->push('Trang chủ', base_url());
        $this->breadcrumbs->push($oneItem->title, getUrlCateResult($oneItem));
        $this->breadcrumbs->push($oneItem->title." $titleSeo", getUrlCateResultDow($oneItem,$dayofweek));
        $data['breadcrumb'] = $this->breadcrumbs->show();

        $data['SEO'] = [
            'meta_title' => $oneItem->title,
            'meta_description' => $oneItem->description,
            'meta_keyword' => $oneItem->meta_keyword,
            'image' => !empty($oneItem->thumbnail)? getImageThumb($oneItem->thumbnail, 470, 246): '',
            'url' => getUrlCateResult($oneItem)
        ];

        $data['main_content']  = $this->load->view(TEMPLATE_PATH . 'result/category', $data, TRUE);
        $this->load->view(TEMPLATE_MAIN, $data);
	}
	public function detail($code,$date) {
        if (strtotime($date) > time()) show_404();
        $timeCache = !in_array(date('G'),[15,16,17,18])? 60*60: 60;
        $oneItem = $this->_data_category->getDataFE(['code' => $code]);
        if (empty($oneItem) || $oneItem[0]->type != 'result') show_404();
        $data['oneItem'] = $oneItem = $oneItem[0];
        if ($oneItem->id != $oneItem->parent_id) {
            $oneParent = $this->_data_category->getByIdCached($oneItem->parent_id);
        } else {
            $oneParent = $oneItem;
        }
        $data['oneParent'] = $oneParent;

        $api = $this->_data_result->getFromDayToDay([
            'api_id' => $oneItem->api_id,
            'date_begin' => $date,
            'date_end' => $date
        ], $timeCache);

        if (!empty($api['data'])){
            if (in_array(strtoupper($oneItem->code),['XSMT','XSMN']))
                $data['data'] = $this->array_group_by($api['data'],function ($i){
                    return $i['displayed_time'];
                });
            else
                $data['data'] = $api['data'];
        } else {
            $data['data'] = null;
        }


        if (strtoupper($oneItem->code) == 'XSMB') $view = '_result_mb';
        elseif (in_array(strtoupper($oneItem->code),['XSMT','XSMN'])) $view = '_result_mtmn';
        else $view = "_result_tinh";
        $data['result'] = $this->load->view(TEMPLATE_PATH . 'result/'.$view, $data, TRUE);

        $this->breadcrumbs->push('Trang chủ', base_url());
        $this->breadcrumbs->push($oneItem->title, getUrlCateResult($oneItem));
        $this->breadcrumbs->push($oneItem->title." ngày $date", getUrlDetailResult($oneItem,$date));
        $data['breadcrumb'] = $this->breadcrumbs->show();

        $data['SEO'] = [
            'meta_title' => $oneItem->title,
            'meta_description' => $oneItem->description,
            'meta_keyword' => $oneItem->meta_keyword,
            'image' => !empty($oneItem->thumbnail)? getImageThumb($oneItem->thumbnail, 470, 246): '',
            'url' => getUrlCateResult($oneItem)
        ];

        $data['main_content']  = $this->load->view(TEMPLATE_PATH . 'result/detail', $data, TRUE);
        $this->load->view(TEMPLATE_MAIN, $data);
	}
}

