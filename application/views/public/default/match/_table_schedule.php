<?php if (!empty($data_match)) :  ?>
	<?php foreach ($data_match as $tournament_id => $matches) :   ?>
		<h5 class="mb-0 table-heading">
			<?php echo $list_tournament[$tournament_id] ?? 'Đang cập nhật' ?>
		</h5>
		<table class="table table-striped">
			<tbody>
				<?php $this->load->view(TEMPLATE_PATH . "match/_data_match_item", ['data_match' => $matches]) ?>
			</tbody>
		</table>
	<?php endforeach; ?>
<?php else : ?>
	<h5 class="mb-3 p-3 text-center text-white text-uppercase bg-info">
		Không có dữ liệu
	</h5>
<?php endif; ?>