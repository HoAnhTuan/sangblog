<h3 class="font-heading border-left-green pl-2 mt-3 text-primary">Bảng Xếp Hạng</h3>
<div class="block-bangxephang">
    <?php $data_rank = getDataCategory('rank');if(!empty($data_rank)) foreach ($data_rank as $item): ?>
    <div class="d-flex justify-content-start py-2">
        <div class="px-3"> <?php echo getLogoTournament($item->tournament_id,$item->title,"logo-tournament") ?></div>
        <span><a href="<?php echo getUrlPage($item) ?>" title="<?php echo $item->title ?>"><?php echo $item->title ?></a></span>
    </div>
    <?php endforeach; ?>
</div>