<?php
if (!empty($user)) :
    ?>
	<div class="author">
		<div class="avatar">
			<img src="<?php echo getImageThumb($user->thumbnail, 60, 60, true) ?>" alt="<?php echo $user->title ?>" title="<?php echo $user->title ?>">
		</div>
		<div class="bio">
			<div class="float-md-right text-center mb-2">
				<a class="btn-readmore <?php echo $this->_method ==='author' ? 'd-none' : '' ?>" href="<?php echo getUrlAuthor($user) ?>">Xem chi tiết</a>
			</div>
			<ul>
				<li><b>Tác giả: </b><?php echo $user->title ?></li>
				<li><b>Tham gia: </b><?php echo date('d/m/Y', strtotime($user->created_time)) ?></li>
				<li><b>Bút danh: </b><?php echo $user->title ?></li>
				<li style="margin: 10px 0"><?php echo $user->description ?></li>
			</ul>
		</div>
	</div>

<?php endif; ?>
