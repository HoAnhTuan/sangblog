<?php if (!empty($tin_soi_keo)) : ?>
	<section class="section">
		<section class="section-header category-name">
			<h2 class="category-title">Soi kèo bóng đá</h2>
		</section>
		<section class="section-body">
			<?php
			foreach ($tin_soi_keo as $key => $item) :
				$data_match = !empty($item->data_match) ? json_decode($item->data_match) : '';
				$data_bets = !empty($item->data_bets) ? json_decode($item->data_bets) : '';
				if (!empty($data_match)) : ?>
					<div class="post-soikeo">
						<div class="header">
							<span><?php echo !empty($data_match->league_name) ? $data_match->league_name : '' ?></span>
							<span><?php echo date('d/m/Y H:i:s', strtotime($item->start_time)) ?></span>
						</div>
						<div class="body">
							<div class="team-home">
								<div class="logo">
									<a href="<?php echo getUrlPost($item) ?>" title="<?php echo $data_match->name_home ?>">
										<img width="38" height="38" loading="lazy" class="img-fluid" src="<?php echo getLogoClub($data_match->home_id, $item->title, '', false) ?>" alt="logo-<?php echo $data_match->name_home ?>">
									</a>
								</div>
								<h3 class="name"><?php echo $data_match->name_home ?></h3>
							</div>
							<div class="data-bet">
								<?php if(!empty($data_bets)) { ?>
									<?php
										$chau_au = array_values((array)$data_bets->chau_au);
										$chau_a = array_values((array)$data_bets->chau_a);
										$tai_xiu = array_values((array)$data_bets->tai_xiu);
									?>
									<div class="d-flex justify-content-around flex-column flex-lg-row">
										<p class="mb-2 justify-content-center flex-row flex-lg-column">
											<span><?php echo $chau_a[0] ?> * <?php echo $chau_a[1] ?> * <?php echo $chau_a[2] ?> </span>
											<b>(Châu Á)</b>
										</p>
										<p class="mb-2 justify-content-center flex-row flex-lg-column">
											<span><?php echo $chau_au[0] ?> / <?php echo $chau_au[1] ?> / <?php echo $chau_au[2] ?> </span>
											<b>(Châu Âu)</b>
										</p>
										<p class="mb-2 justify-content-center flex-row flex-lg-column">
											<span><?php echo $tai_xiu[0] ?> * <?php echo $tai_xiu[1] ?> * <?php echo $tai_xiu[2] ?> </span>
											<b>(Tài xỉu)</b>
										</p>
									</div>
								<?php } else { ?>
									<div class="d-flex justify-content-around flex-column flex-lg-row">
										<p class="mb-2 justify-content-center flex-row flex-lg-column">
											Dữ liệu đang được cập nhật
										</p>
									</div>
								<?php } ?>
							</div>
							<div class="team-away">
								<div class="logo">
									<a href="<?php echo getUrlPost($item) ?> title=" <?php echo $data_match->name_away ?>">
										<img width="38" height="38" loading="lazy" class="img-fluid" src="<?php echo getLogoClub($data_match->away_id, $item->title, '', false) ?>" title="<?php echo $data_match->name_away ?>" alt="logo-<?php echo $data_match->name_away ?>">
									</a>
								</div>
								<h3 class="name"><?php echo $data_match->name_away ?></h3>
							</div>
							<div class="data-meta">
								<a class="btn-sk btn" href="<?php echo getUrlPost($item) ?>" title="soi kèo">Soi
									kèo
								</a>
								<a rel="nofollow" target="_blank" class="btn-bet btn" href="<?php echo $this->_settings->link_cuoc ?? '#' ?>">Đặt cược</a>
								<?php if (!empty($data_match)) : ?>
									<a href="<?php echo getUrlMatch($data_match) ?>" title="Thông tin trận đấu <?php echo $data_match->name_home . ' và ' . $data_match->name_away ?>" class="btn btn-warning">Thông tin</a>
								<?php endif; ?>
								<!-- <a rel="nofollow" target="_blank" class="btn-live btn" href="https://avaoroi.com/">Xem live</a> -->
								<div class="btn-live btn">Xem live</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</section>
	</section>
<?php endif; ?>