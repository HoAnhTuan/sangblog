<?php if(!empty($data_match)) foreach ($data_match as $item): ?>
    <div class="live-block">
        <div class="align-items-center text-center mb-1">
            <div class="row no-gutters py-4 align-items-center ">
                <div class="col-12 col-md-2">
                    <div class="position-relative">
                        <?php if($item->match_status === 'live'): ?>
                            <div class="live-today text-center ">
                                <span class="dot green-dot"></span>
                                <span><?php echo !empty($item->elapsed) ? $item->elapsed . "'" : getTitleStatusMatch($item) ?></span>
                            </div>
                        <?php else: ?>
                            <?php echo time() + 60 * 60 > $item->start_timestamp ? '<div class="text-center ">Sắp đá '.timeAgo($item->start_time, "H:i").'</div>' : '<div class="text-center ">'.timeAgo($item->start_time, "H:i d/m")."</div>" ?>
                        <?php endif; ?>
                        <div class="mb-2 mb-md-0 font-weight-bold"><?php echo $item->league_name ?></div>
                    </div>
                </div>
                <div class="col-12 col-md-7">
                    <div class="row no-gutters align-items-center justify-content-center">
                        <div class="col-md-2 col-3">
                            <div class=" font-weight-bold"><?php echo $item->name_home ?></div>
                        </div>
                        <div class="col-md-2 col-3">
                            <div class="logo-club-live mx-auto">
                                <?php echo getLogoClub($item->home_id, $item->name_home, "img-fluid logo-club", 50, 50) ?>
                                <!--<img class="img-fluid logo-club" src="https://via.placeholder.com/50x50/C4C4C4" alt="Logo A.jpg">-->
                            </div>
                        </div>
                        <div class="col-md-2 col-12 order-2 order-md-0 py-2 py-md-0">
                            <?php if (time() + 60 * 60 < $item->start_timestamp) : ?>
                                <span class="about-to-play d-block">
		                          <span class="dot yellow-dot"></span>
		                          <span class="text">SẮP THI ĐẤU</span>
		                        </span>
                                <span class="font-weight-bold text-secondary"><?php echo date('H:i', $item->start_timestamp) ?></span> -
                                <span class="font-weight-normal text-secondary"><?php echo date('d/m', $item->start_timestamp) ?></span>
                            <?php else : ?>
                                <span class="live d-block">
			                          <span class="dot red-dot"></span>
			                          <span class="text">VS</span>
			                      </span>
                                <span class="font-weight-bold text-secondary"><?php echo $item->score_home . " - " . $item->score_away ?></span>
                            <?php endif; ?>

                        </div>
                        <div class="col-md-2 col-3">
                            <div class="logo-club-live mx-auto">
                                <?php echo getLogoClub($item->away_id, $item->name_away, "img-fluid logo-club", 80, 80) ?>
                                <!--<img class="img-fluid logo-club " src="https://via.placeholder.com/50x50/C4C4C4" alt="Logo B.jpg">-->
                            </div>
                        </div>
                        <div class="col-md-2 col-3">
                            <div class=" font-weight-bold"><?php echo $item->name_away ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 d-flex flex-row  justify-content-between">
                    <a href="javascript:void(0);"
                       class="btn live-btn-cuoc py-2 px-3 mx-auto rounded-10">
                        CƯỢC
                    </a>
                    <a href="javascript:void(0);"
                       class="btn live-btn-link py-2 px-3 mx-auto rounded-10">
                        LINK XEM
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>