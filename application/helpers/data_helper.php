<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('getByIdTournament')) {
    function getByIdTournament($id)
    {
        $_this = &get_instance();
        $_this->load->model('tournament_model');
        $model = new Tournament_model();
        $data = $model->getById($id);
        return $data;
    }
}

if (!function_exists('getByIdPost')) {
    function getByIdPost($id)
    {
        $_this = &get_instance();
        $_this->load->model('post_model');
        $model = new Post_model();
        $data = $model->getById($id);
        return $data;
    }
}

if (!function_exists('getByIdPage')) {
    function getByIdPage($id)
    {
        $_this = &get_instance();
        $_this->load->model('category_model');
        $model = new Category_model();
        $data = $model->getById($id);
        return $data;
    }
}
if (!function_exists('getSetting')) {
    function getSetting($key_setting, $name = '')
    {
        $instance = &get_instance();
        $instance->load->model('setting_model');
        $setting_model = new Setting_model();
        $data = $setting_model->get_setting_by_key($key_setting);
        $data = !empty($data->value_setting) ? json_decode($data->value_setting) : '';
        return !empty($data) ? (!empty($name) ? $data->$name : $data) : '';
    }
}
if (!function_exists('getDrag')) {
    function getDrag($type)
    {
        $_this = &get_instance();
        $_this->load->model('drag_model');
        $model = new Drag_model();
        $data = $model->getDataDrag($type);
        return $data;
    }
}
if (!function_exists('isMobileDevice')) {
    function isMobileDevice()
    {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
}
if (!function_exists('renderStarRating')) {
    function renderStarRating($number)
    {
        $maxRating = 5;
        $rating = (int)$number/2;
        $fullStar = '<i class="fas fa-star"></i>';
        $emptyStar = '<i class="fas fa-star text-secondary"></i>';
        $rating = $rating <= $maxRating ? $rating : $maxRating;

        $fullStarCount = round($rating);
        $emptyStarCount = round($maxRating - $fullStarCount);
        $html = str_repeat($fullStar,$fullStarCount);
        $html .= str_repeat($emptyStar,$emptyStarCount);
        return $html;
    }
}

if (!function_exists('getTag')) {
    function getTag($args = array())
    {
        $default = [
            'limit' => 10,
            'type' => 'tag',
            'order' => ['id' => 'DESC']
        ];

        $params = array_merge($default, $args);
        $_this = &get_instance();
        $_this->load->model('category_model');
        $model = new Category_model();
        $data = $model->getDataFE($params);
        return $data;
    }
}
if (!function_exists('getDataPost')) {
    function getDataPost($params = array())
    {
        $_this = &get_instance();
        $_this->load->model('post_model');
        $model = new Post_model();
        $data = $model->getDataFE($params);
        return $data;
    }
}

if (!function_exists('getDataCategory')) {
    function getDataCategory($type = '')
    {
        $_this = &get_instance();
        $_this->load->model('category_model');
        $model = new Category_model();
        $data = $model->_all_category($type);
        return $data;
    }
}
if (!function_exists('getCategoryChild')) {
    function getCategoryChild($parent_id = 0)
    {
        $_this = &get_instance();
        $_this->load->model('category_model');
        $model = new Category_model();
        $model->_recursive_child($model->_all_category(), $parent_id);
        return $model->_list_category_child;
    }
}

if (!function_exists('getAllTournamentId')) {
    function getAllTournamentId()
    {
        $_this = &get_instance();
        $_this->load->model('category_model');
        $model = new Category_model();
        return $model->getAllTournamentId();
    }
}

if (!function_exists('getRate')) {
    function getRate($args = array())
    {
        $_this = &get_instance();
        $_this->load->model('reviews_model');
        $reviews = new Reviews_model();
        $data = $reviews->getRate($args);
        return $data;
    }
}
if (!function_exists('callCURL')) {
    function callCURL($url, $data = array(), $type = "GET")
    {
        $resource = curl_init();
        curl_setopt($resource, CURLOPT_URL, $url);

        if ($type == "POST") {
            curl_setopt($resource, CURLOPT_POST, true);
            curl_setopt($resource, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($resource, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($resource, CURLOPT_TIMEOUT, 40);
        $result = curl_exec($resource);
        curl_close($resource);
        return $result;
    }
}

if (!function_exists('data_bet')) {
    function data_bet($bet_str)
    {
        if (!empty($bet_str)) {
            $bet_data = convert_bet_data($bet_str);
            $result = '';
            $asia = array();
            $asia[0] = isset($bet_data['live'][7]) ? $bet_data['live'][7] : '';
            $asia[1] = isset($bet_data['live'][8]) ? $bet_data['live'][8] : '';
            $asia[2] = isset($bet_data['live'][9]) ? $bet_data['live'][9] : '';
            $asia = parse_part_bet_data($asia);

            $uk = array();
            $uk[0] = isset($bet_data['live'][0]) ? $bet_data['live'][0] : '';
            $uk[1] = isset($bet_data['live'][1]) ? $bet_data['live'][1] : '';
            $uk[2] = isset($bet_data['live'][2]) ? $bet_data['live'][2] : '';

            $big = array();
            $big[0] = isset($bet_data['live'][11]) ? $bet_data['live'][11] : '';
            $big[1] = isset($bet_data['live'][12]) ? $bet_data['live'][12] : '';
            $big[2] = isset($bet_data['live'][13]) ? $bet_data['live'][13] : '';
            $big = parse_part_bet_data($big);

            $result .= '<p class="mb-1"><span class="font-weight-bold">' . $asia[0] . '*' . $asia[1] . '*' . $asia[2] . '</span> <span>(Châu Á)</span></p>';
            $result .= '<p class="mb-1"><span class="font-weight-bold">' . $uk[0] . '/' . $uk[1] . '/' . $uk[2] . '</span> <span>(Châu Âu)</span></p>';
            $result .= '<p class="mb-1"><span class="font-weight-bold">' . $big[0] . '*' . $big[1] . '*' . $big[2] . '</span> <span>(Tài xỉu)</span></p>';
        } else {
            $result = '<span>Dữ liệu đang được cập nhật</span>';
        }
        return $result;
    }
}

if (!function_exists('convert_bet_data')) {
    function convert_bet_data($str_bet_data)
    {
        $bet_data_ar = explode(';', $str_bet_data);
        $bet_data = array();
        for ($i = 0; $i < count($bet_data_ar); ++$i) {
            if ($bet_data_ar[$i] == 'Crown') {
                if ($bet_data_ar[$i + 1] != '') {
                    $bet_data['data'] = explode(',', $bet_data_ar[$i + 1]);
                }
                if ($bet_data_ar[$i + 2]) {
                    $bet_data['live'] = explode(',', $bet_data_ar[$i + 2]);
                }
                break;
            } elseif ($bet_data_ar[$i] == 'Bet365') {
                if ($bet_data_ar[$i + 1] != '') {
                    $bet_data['data'] = explode(',', $bet_data_ar[$i + 1]);
                }
                if ($bet_data_ar[$i + 2]) {
                    $bet_data['live'] = explode(',', $bet_data_ar[$i + 2]);
                }
                break;
            } elseif ($bet_data_ar[$i] == '10BET') {
                if ($bet_data_ar[$i + 1] != '') {
                    $bet_data['data'] = explode(',', $bet_data_ar[$i + 1]);
                }
                if ($bet_data_ar[$i + 2]) {
                    $bet_data['live'] = explode(',', $bet_data_ar[$i + 2]);
                }
                break;
            }
        }
        $bet_data = parse_bet_data($bet_data);
        return $bet_data;
    }
}

if (!function_exists('parse_part_bet_data')) {
    function parse_part_bet_data($bet)
    {

        foreach ($bet as $index => &$data) {

            if ($index == 1) continue;

            if ($data !== '') {
                if (strpos($data, '/') !== false) {
                    $data = explode('/', $data);
                    $data = $data[0] / $data[1];
                }

                $data = (float) $data;

                if ($data > 1) {
                    $data = $data - 2;
                }

                $data = number_format($data, 2);
            }
        }
        return $bet;
    }
}

if (!function_exists('parse_bet_data')) {
    function parse_bet_data($bet)
    {
        $run = [7, 9, 11, 13];
        foreach ($run as $i) {
            if (!isset($bet['live'][$i])) continue;

            $data = (float) $bet['live'][$i];

            if ($data > 1) {
                $data = $data - 2;
            }

            $data = number_format($data, 2);
            $bet['live'][$i] = $data;
        }

        foreach ($run as $j) {
            if (!isset($bet['data'][$j])) continue;

            $data = (float) $bet['data'][$j];

            if ($data > 1) {
                $data = $data - 2;
            }

            $data = number_format($data, 2);
            $bet['data'][$j] = $data;
        }

        return $bet;
    }
}
if (!function_exists('closetags')) {
    function closetags($html)
    {
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);
        for ($i = 0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</' . $openedtags[$i] . '>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
}


if (!function_exists('array_group_by')) {
    function array_group_by($arr, callable $key_selector)
    {
        $arr = json_decode(json_encode($arr), true);
        $result = array();
        foreach ($arr as $i) {
            $key = call_user_func($key_selector, $i);
            $result[$key][] = (object)$i;
        }
        return $result;
    }
}
