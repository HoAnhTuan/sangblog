<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 10/2/2018
 * Time: 11:43 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Linkstream_model extends STEVEN_Model
{
    public function __construct(){
        parent::__construct();
        $this->table = 'linkstream';
    }

    public function getById($id, $updateCache = false){
        $key = "linkstream_getById_$id";
        $data = $this->getCache($key);
        if(empty($data) || $updateCache == true){
            $params = [
                'id' => $id
            ];
            $result = $this->getDataApi($this->table.'/get_by_id',$params);
            $data = $result->data;
            $this->setCache($key,$data,30*60);
        }
        return $data;
    }
}