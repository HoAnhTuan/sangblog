<section class="section">
    <section class="section-header category-name">
        <h2 class="category-title">Soi kèo nổi bật</h2>
    </section>
    <section class="section-body">
        <div class="ft-news mb-3">
            <?php
            if (!empty($keo_noi_bat)) {
                foreach ($keo_noi_bat as $key => $item) : ;
                    ?>
                    <div class="section-item">
                        <div class="row mb-4">
                            <div class="col-md-7 mb-2 mb-md-0">
                                <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                    <span class="highlight"><?php echo $item->term_name?></span>
                                    <img class="w-100 img-fluid" width="545" height="340" src="<?= getImageThumb($item->thumbnail,545,340)?>" alt="<?= $item->title?>">
                                </a>
                            </div>
                            <div class="col-md-5">
                                <h3 class="title-big">
                                    <a href="<?php echo getUrlPost($item) ?>"><?= $item->title ?></a>
                                </h3>
                                <p class="font-13 mb-2 text-datetime"><span class="text-green font-weight-bold">Kèo nổi bật</span></p>
                                <p class="line-height-24 m-0"><?= $item->description ?></p>
                            </div>
                        </div>
                    </div>
                    <?php unset($keo_noi_bat[$key]);
                    break;
                endforeach;
            } ?>
            <div class="row">
                <?php
                if (!empty($keo_noi_bat)) {
                    foreach ($keo_noi_bat as $key => $item) :
                        ?>
                        <div class="section-item col-md-4">
                            <div class="row">
                                <div class="col-5 col-md-12 pr-0 pr-md-3">
                                    <a class="d-block mb-2" href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                        <span class="highlight"><?php echo $item->term_name?></span>
                                        <?php echo getThumbnail($item, 300, 180, "img-fluid") ?>
                                    </a>
                                </div>
                                <div class="col-7 col-md-12">
                                    <h4 class="title-small">
                                        <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>" class="text-dark font-16 line-height-24"><?= $item->title ?></a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <?php if ($key === 3) break;
                    endforeach;
                } ?>

            </div>
        </div>
    </section>
</section>
