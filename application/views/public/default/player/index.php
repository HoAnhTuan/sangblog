<!DOCTYPE html>
<html>
<head>
    <meta name="robots" content="noindex,nofollow">
    <style>
        body{
            overflow: hidden;
            background-color: #000000;
            margin: 0;
        }
        iframe{
            height: 100vh;
            border: none;
            overflow: hidden;
            width: 100%;
        }
        p{
            color: #ffffff;
        }
    </style>
</head>
<body>
<?php if(!empty($link)):
    if (preg_match("/youtube/i", $link)) {
        $id = getYoutubeKey($link);
        $link = "//www.youtube.com/embed/$id?controls=0&modestbranding=0";
    }
    ?>
    <iframe width="100%" height="100%" src="<?php echo $link ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
<?php else: ?>
    Loading...
<?php endif; ?>

<?php if(!empty($data_link)):
    if (preg_match("/youtube/i", $data_link->url_video)) {
        $id = getYoutubeKey($data_link->url_video);
        $link = "//www.youtube.com/embed/$id?controls=0&modestbranding=0";
    }else $link = $data_link->url_video;
    ?>
    <iframe width="100%" height="100%" src="<?php echo $link ?>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
<?php else: ?>
    Loading...
<?php endif; ?>
</body>
</html>