<section class="section mt-70">
    <section class="section-header category-name">
        <h1 class="category-title">Kết quả</h1>
    </section>
    <section class="section-body">
        <div class="table-result-bongda">
            <ul class="nav-table">
                <li class="active">
                    <a href="">10/12</a>
                </li>
                <li>
                    <a href="">10/12</a>
                </li>
                <li>
                    <a href="">10/12</a>
                </li>
                <li>
                    <a href="">10/12</a>
                </li>
                <li>
                    <a href="">10/12</a>
                </li>
                <li>
                    <a href="">10/12</a>
                </li>
            </ul>
            <h5 class="mb-0 table-heading">Vòng loại World cup 2022</h5>
            <table class="table table-striped">
                <tbody>
                    <tr class="team text-right">
                        <td>Manchester City<img class="ml-2 logo" src="https://bwarh.com/wp-content/uploads/2021/08/Logo-doi-bong-Manchester-City.png" alt=""></td>
                        <td class="text-center">
                            <div class="d-inline-block score">2 : 3</div>
                        </td>
                        <td class="team text-left"><img class="mr-2 logo" src="https://bwarh.com/wp-content/uploads/2021/08/Logo-doi-bong-Manchester-City.png" alt="">Chealse</td>
                    </tr>
                    <tr class="team text-right">
                        <td>Manchester City<img class="ml-2 logo" src="https://bwarh.com/wp-content/uploads/2021/08/Logo-doi-bong-Manchester-City.png" alt=""></td>
                        <td class="text-center">
                            <div class="d-inline-block score">2 : 3</div>
                        </td>
                        <td class="team text-left"><img class="mr-2 logo" src="https://bwarh.com/wp-content/uploads/2021/08/Logo-doi-bong-Manchester-City.png" alt="">Chealse</td>
                    </tr>

                </tbody>
            </table>
            <div class="text-center my-5">
                <button class="btn mx-auto my-3 btnLoadMore">Xem thêm</button>
            </div>
            <p class="des">
                <?php echo $oneItem->description ?? '' ?>
            </p>
        </div>
        <div class="content-news text-justify">
            <?php echo $oneItem->content ?? '' ?>
        </div>
    </section>
</section>