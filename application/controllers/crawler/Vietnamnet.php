<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\DomCrawler\Crawler;
class Vietnamnet extends Crawler_Controller
{
    private $_category;
    private $_post;
    private $_base_url;
    private $_client;
    private $_list_cate;

    public function __construct(){
        parent::__construct();
        $this->load->model(['category_model','post_model']);
        $this->_category     = new Category_model();
        $this->_post         = new Post_model();
        $this->_client = new Client(HttpClient::create(['timeout' => 60]));
        $this->_base_url = 'https://vietnamnet.vn';
        $this->_list_cate = [
          'https://vietnamnet.vn/jsx/loadmore/?domain=desktop&c=the-thao-euro&p=' => 5

        ];
    }

    public function start(){
        foreach ($this->_list_cate as $slug => $cate_id){
            for($i = 40; $i >=0; $i--){
                $this->getCategory($slug . $i . '&s=15&a=5', $cate_id, "post");
            }
        }
        die("END - Crawler from site {$this->_base_url}\n");
    }

    private function getCategory($url, $category_id, $type = 'post'){
        try {
            $client = HttpClient::create();
            $response = $client->request('GET', $url);
            $contentObject = $response->getContent();
            $contentObject = str_replace("retvar =", "", $contentObject);
            $contentArray = json_decode($contentObject, true);
            $listDetail = [];
             foreach ($contentArray as $item) {
                 array_push($listDetail, $item['link']);
            }
            echo "Start Crawler article from category => {$item['link']} \n";
            $listDetail = array_reverse($listDetail);
            if(!empty($listDetail)) foreach ($listDetail as $linkDetail) {
                $bot_crawler_detail = $this->_client->request('GET', $linkDetail);
                $data['title'] = $data['meta_title'] = $bot_crawler_detail->filter("h1")->text();
                $data['slug'] = $this->toSlug($data['title']);
                if (!$this->_post->checkExistByField("slug",$data['slug'])) {
                    echo "---------Crawl content from => $linkDetail ----------------- \n";
                    if (!$this->_post->checkExistByField("slug", $data['slug'])) {
                        $data['description'] = $data['meta_description'] = $bot_crawler_detail->filter('meta[property="og:description"]')->attr("content");
                        $bot_crawler_detail->filter('.inner-article')->each(function (Crawler $crawler) {
                            foreach ($crawler as $node) {
                                $node->parentNode->removeChild($node);
                            }
                        });
                        $bot_crawler_detail->filter('.article-relate')->each(function (Crawler $crawler) {
                            foreach ($crawler as $node) {
                                $node->parentNode->removeChild($node);
                            }
                        });
//                       $bot_crawler_detail->filter('#ArticleContent')->each(function (Crawler $crawler){
//                            var_dump($crawler->html()); ;
//                        });
                        $content = $bot_crawler_detail->filter('#ArticleContent')->html();
                        $content = $this->cleanContent($content);

                        if (!empty($content)) {
                            $content = $this->cleanContent($content);
                            $imgs = $bot_crawler_detail->filter('#ArticleContent img')->each(function ($node) {
                                if (preg_match('/http/i', $node->attr('src'))) $imgSrc = $node->attr('src');
                                else $imgSrc = $this->_base_url . $node->attr('src');
                                $imgSrc = strtok($imgSrc, '?');
                                $srcNew = $this->downloadImage($imgSrc, '', 'bongda');
                                return [
                                  'old' => $node->attr('src'),
                                  'new' => "/" . MEDIA_NAME . $srcNew
                                ];
                            });
                            if (!empty($imgs)) foreach ($imgs as $img) {
                                $content = str_replace($img['old'], $img['new'], $content);
                            }

                            $data['content'] = $content;

                            if ($bot_crawler_detail->filter('meta[property="og:image"]')->count() > 0)
                                $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'), '', 'bongda');
                            else
                                $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('#ArticleContent img', 0)->attr('src'), '', 'bongda');

                            $data['type'] = $type;
                            $data['is_robot'] = 1;
                            $data['user_id'] = 1;


                            if ($id = $this->_post->save($data)) {
                                $data_category = [
                                  "post_id" => $id,
                                  'category_id' => $category_id,
                                  'is_primary' => false
                                ];
                                if ($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)) {
                                    echo "Update category bài viết $id thành công ! \n";
                                }
                                echo "Thêm bài viết $id thành công ! \n";
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
}

