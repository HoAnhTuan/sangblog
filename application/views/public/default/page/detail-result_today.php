<?php if (!empty($oneItem)) :  ?>
	<main class="main-detail" data-url="<?php echo getUrlPost($oneItem) ?>">
		<div class="container">
			<div class="row">
				<?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
				<div class="col-12 mt-70 col-lg-9">
					<section class="section">
						<section class="section-header category-name">
							<h1 class="category-title"><?php echo $oneItem->title ?></h1>
						</section>
						<section class="section-body">
							<div class="table-result-bongda">
								<?php
								$diff_time = [-6, -5, -4, 0, -3, -2, -1];
								?>

								<div class="slides-swpiper" data-current-round="4">
									<div class="bxh-prev">
										<i class="fas fa-angle-left"></i>
									</div>
									<div class="swiper-container  bg-white px-1">
										<ul class="swiper-wrapper nav-table">
											<?php foreach ($diff_time as $key => $item) : ?>
												<li class="swiper-slide">
													<a href="javascript:;" data-time="<?php echo date('Y-m-d', strtotime("$item days")) ?>"><?php echo diff_time_vn(date('Y-m-d', strtotime("$item days"))) ?></a>
												</li>
											<?php endforeach; ?>
										</ul>
										<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span><span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
									</div>
									<div class="bxh-next">
										<i class="fas fa-angle-right"></i>
									</div>
								</div>
								<div id="ajax-table" class="mt-3">
									<?php $this->load->view(TEMPLATE_PATH . "match/_table_schedule", ['list_tournament' => $list_tournament ?? [], 'data_match' => $data_match ?? []]); ?>
								</div>
							</div>
						</section>
					</section>
					<?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer.php') ?>
				</div>
				<div class="col-12 mt-70 col-lg-3">
					<?php $this->load->view(TEMPLATE_PATH . 'block/nav_sidebar') ?>
					<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
				</div>
			</div>
		</div>
        <?php if(!empty($oneItem->content)) $this->load->view(TEMPLATE_PATH . 'block/_content_footer', ['content' => $oneItem->content, 'des' => $oneItem->description]) ?>
	</main>
	<script>
		var layout = '<?php echo $oneItem->layout ?>'
	</script>
<?php endif; ?>