<?php if(!empty($data_match)) foreach ($data_match as $item) : ?>
    <tr class="select-<?php echo $item->match_status ?>">
        <td style="width: 4rem">
            <?php if($item->match_status === 'live'): ?>
                <span class="text-danger"><?php echo !empty($item->elapsed) ? $item->elapsed . "'" : getTitleStatusMatch($item) ?></span>
            <?php else: ?>
                <?php echo time() + 60 * 60 > $item->start_timestamp ? '<small class="text-danger">Sắp đá '.timeAgo($item->start_time, "H:i").'</small>' : "<small>".timeAgo($item->start_time, "H:i d/m")."</small>" ?>
            <?php endif; ?>
        </td>
        <td width="30%" class="text-right">
            <a href="javascript:;" title="<?php echo $item->name_home ?>" class="text-dark"><?php echo $item->name_home ?></a>
        </td>
        <td style="width: 5rem" class="text-center max-content align-middle">
            <?php echo $item->score_home . " - " . $item->score_away ?>
        </td>
        <td width="40%">
            <a href="javascript:;" title="<?php echo $item->name_away ?>" class="text-dark"><?php echo $item->name_away ?></a>
        </td>
    </tr>
<?php endforeach; ?>