<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <?php $this->load->view(TEMPLATE_PATH . 'seo/meta_seo') ?>
    <link rel="shortcut icon" type="image/png" href="<?php echo $this->_settings->favicon ? getImageThumb($this->_settings->favicon, 32, 32) : site_url('public/favicon.ico') ?>">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.2.0/css/all.css">
    <!--<link rel="stylesheet" href="--><?php //echo TEMPLATE_ASSET . 'css/all_minify.min.css?v='.ASSET_VERSION 
                                        ?>
    <!--" >-->
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'css/swiper.min.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'css/bootstrap.min.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'css/custom.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'plugins/jssocials/jssocials.css?v=' . ASSET_VERSION ?>">
    <link rel="stylesheet" href="<?php echo TEMPLATE_ASSET . 'plugins/jssocials/jssocials-theme-flat.css?v=' . ASSET_VERSION ?>">
    <?php echo getSetting("data_seo", "style") ?>
    <?php echo getSetting("data_seo", "script") ?>

    <?php if ($this->_method === 'detail' && ($this->_controller === 'match' || $this->_controller === 'post')) : ?>
        <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/custom_match.js?v=' . ASSET_VERSION ?>"></script>
    <?php endif; ?>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="<?php echo $this->_controller; ?>">
    <script type='text/javascript'>
        const base_url = '<?php echo base_url(); ?>',
            media_url = '<?php echo MEDIA_URL . '/'; ?>';
        data_banner = '<?php echo showImageBanner() ?>';
    </script>
    <section class="section">
        <section class="section-body">
            <div class="table-result-bongda">
                <?php
                $diff_time = [1, 2, 3, 0, 4, 5, 6];
                ?>
                <div class="slides-swpiper" data-current-round="4">
                    <div class="bxh-prev">
                        <i class="fas fa-angle-left"></i>
                    </div>
                    <div class="swiper-container bg-white px-1">
                        <ul class="swiper-wrapper nav-table">
                            <?php foreach ($diff_time as $key => $item) : ?>
                                <li class="swiper-slide">
                                    <a href="javascript:;" data-time="<?php echo date('Y-m-d', strtotime("$item days")) ?>">
                                        <?php echo diff_time_vn(date('Y-m-d', strtotime("$item days"))) ?>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span><span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                    </div>
                    <div class="bxh-next">
                        <i class="fas fa-angle-right"></i>
                    </div>
                </div>

                <div id="ajax-table" class="mt-3">
                    <?php $this->load->view(TEMPLATE_PATH . "match/_table_schedule", ['list_tournament' => $list_tournament ?? [], 'data_match' => $data_match ?? []]); ?>
                </div>

                <p class="des">
                    <?php echo $oneItem->description ?? '' ?>
                </p>
            </div>
        </section>
    </section>
    <script>
        var layout = '<?php echo $oneItem->layout ?>'
    </script>


    <!--Wrapper End-->
    <!--<script defer type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'js/all_minify.min.js?v='.ASSET_VERSION 
                                                        ?>
    <!--"></script>-->
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/jquery.js?v=' . ASSET_VERSION ?>"></script>
    <!--<script defer type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'js/jquery.sticky-kit.min.js?v='.ASSET_VERSION 
                                                        ?>
    <!--"></script>-->
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/bootstrap.js?v=' . ASSET_VERSION ?>"></script>
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/swiper.min.js?v=' . ASSET_VERSION ?>"></script>
    <!--<script defer type="text/javascript" src="--><?php //echo TEMPLATE_ASSET . 'plugins/jssocials/jssocials.min.js?v='.ASSET_VERSION 
                                                        ?>
    <!--"></script>-->
    <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/custom.js?v=' . ASSET_VERSION ?>"></script>
    <?php if ($this->_controller === 'match' && $this->_method === 'detail') : ?>
        <script type="text/javascript" src="<?php echo TEMPLATE_ASSET . 'js/circle_process.js?v=' . ASSET_VERSION ?>"></script>
    <?php endif; ?>
</body>

</html>