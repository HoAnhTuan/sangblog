<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Debug extends Public_Controller
{
    protected $_data_category;
    protected $_data_post;
    protected $_data_match;
    protected $_data_log;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['category_model', 'post_model','match_model','logs_model']);
        $this->_data_post = new Post_model();
        $this->_data_category = new Category_model();
        $this->_data_match = new Match_model();
        $this->_data_log = new Logs_model();
    }

    public function cache(){
        $allCache = $this->cache->cache_info();
        echo "<ul>";
        if(!empty($allCache)) foreach ($allCache as $key => $item){
            $delete = "<a target='_blank' href='".base_url('debug/delete_cache?key='.$key)."'>Delete cache</a>";
            echo "<li>$key => $delete</li>";
        }
        echo "</ul>";
    }

    public function delete_cache_file($url = ''){
        if (empty($url)){
            $this->load->helper('file');
            $url = $this->input->get('url');
        }

        if(!empty($url)){
            $uri = str_replace(base_url(),'/',$url);
            if($this->output->delete_cache($uri)) echo 'Delete cache'.$uri."<br>";
            else  echo "$uri has been deleted !<br>";
        }else{
            if(delete_files(FCPATH . 'application' . DIRECTORY_SEPARATOR . 'cache')) die("Delete all page statistic success !");
            else  die("Delete all page statistic error !");
        }

    }

    public function delete_cache(){
        $key = $this->input->get('key');
        $key = str_replace(CACHE_PREFIX_NAME,'',$key);
        if(!empty($key)) {
            if($this->deleteCache($key)) die('Delete success !');
            else  die('Delete error !');
        }else{
            die('Not key => error !');
        }
    }

    public function update_cache(){
        $this->delete_cache_file(base_url());
        exit;
    }

    public function update_cache_home(){
        $this->delete_cache_file(base_url());
        exit;
    }

    public function update_cache_cate($slug){
        $this->delete_cache_file(base_url());
        exit;
    }

    public function update_cache_detail($id){
        $oneItem = $this->_data_post->get_post_by_id($id,true);
        $this->delete_cache_file(getUrlPost($oneItem));
        exit;
    }

    public function test(){
        $allPost = $this->_data_post->getDataAll('','','id, content');
        if(!empty($allPost)) foreach ($allPost as $item){
            $content = $item->content;
            $content = $this->executeImage($content, $item);
        }
    }

    private function executeImage($content, $itemPost){
        preg_match_all("~<img.*src\s*=\s*[\"']([^\"']+)[\"'][^>]*>~i", $content, $matches);

        if(!empty($matches[1])) foreach ($matches[1] as $item){
            $urlImg = "https://asoikeo.com" . $item;
//            echo $urlImg ."<br> \n";
            if($this->checkImgExist($urlImg) == false) {
//                echo "/src='".addcslashes($item, '/')."'/i";
                echo "IMAGE error \n";
                $content = preg_replace("/<img(.*)src=(\\'|\")(".addcslashes($item, '/').".*)(\\'|\")>/i","",$content);
                if($this->_data_post->update(['id' => $itemPost->id],['content' => $content])) echo "Updated contend id $itemPost->id ! \n";
            }
        }

        /*Xử lý ảnh chưa có title*/
        /*preg_match("/title=\"(.*?)\"/", $content, $matchesNoTitle);
        dd($matchesNoTitle);*/
        return $content;
    }

    private function checkImgExist($image){
        $ch = curl_init($image);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        if( $httpCode == 200 ){
            if(empty($size)) return false;
            return true;
        }
        return false;
    }

    public function test1(){
        $a = '{"id":"0","title":"Nhi\u1ec1u c\u1ea7u th\u1ee7 Tottenham d\u00ednh Covid-19, Ngo\u1ea1i h\u1ea1ng Anh nguy c\u01a1 gi\u00e1n \u0111o\u1ea1n","description":"D\u1ecbch b\u1ec7nh ti\u1ebfp t\u1ee5c g\u00e2y \u1ea3nh h\u01b0\u1edfng \u0111\u1ebfn gi\u1ea3i \u0111\u1ea5u cao nh\u1ea5t x\u1ee9 s\u01b0\u01a1ng m\u00f9. M\u1edbi \u0111\u00e2y, Tottenham x\u00e1c nh\u1eadn \u0111\u00e3 c\u00f3 6 c\u1ea7u th\u1ee7 d\u01b0\u01a1ng t\u00ednh v\u1edbi Covid-19.","meta_title":"Nhi\u1ec1u c\u1ea7u th\u1ee7 Tottenham d\u00ednh Covid-19, Ngo\u1ea1i h\u1ea1ng Anh nguy c\u01a1 gi\u00e1n \u0111o\u1ea1n","slug":"nhieu-cau-thu-tottenham-dinh-covid-19-ngoai-hang-anh-nguy-co-gian-doan","meta_description":"D\u1ecbch b\u1ec7nh ti\u1ebfp t\u1ee5c g\u00e2y \u1ea3nh h\u01b0\u1edfng \u0111\u1ebfn gi\u1ea3i \u0111\u1ea5u cao nh\u1ea5t x\u1ee9 s\u01b0\u01a1ng m\u00f9. M\u1edbi \u0111\u00e2y, Tottenham x\u00e1c nh\u1eadn \u0111\u00e3 c\u00f3 6 c\u1ea7u th\u1ee7 d\u01b0\u01a1ng t\u00ednh v\u1edbi Covid-19.","meta_keyword":"Tottenham, Covid-19, Ngo\u1ea1i h\u1ea1ng Anh","content":"<p style=\"text-align: justify;\">Tottenham \u0111ang th\u0103ng hoa d\u01b0\u1edbi th\u1eddi t&acirc;n HLV Conte. Chi\u1ebfn th\u1eafng thuy\u1ebft ph\u1ee5c 3-0 tr\u01b0\u1edbc Norwich cu\u1ed1i tu\u1ea7n qua l&agrave; \u0111i\u1ec3m s\u1ed1 th\u1ee9 10 sau 4 tr\u1eadn chi\u1ebfn l\u01b0\u1ee3c gia ng\u01b0\u1eddi Italia c\u1ea7m qu&acirc;n. K\u1ebft qu\u1ea3 n&agrave;y gi&uacute;p Tottenham tr\u1edf l\u1ea1i cu\u1ed9c \u0111ua gi&agrave;nh v&eacute; d\u1ef1 C&uacute;p C1 ch&acirc;u &Acirc;u m&ugrave;a t\u1edbi.<\/p>\r\n<p style=\"text-align: justify;\">Th\u1ebf nh\u01b0ng Tottenham l\u1ea1i ph\u1ea3i \u0111\u01b0\u01a1ng \u0111\u1ea7u v\u1edbi m\u1ed9t th\u1eed th&aacute;ch m\u1edbi c&ograve;n kh&oacute; kh\u0103n h\u01a1n r\u1ea5t nhi\u1ec1u, \u0111&oacute; ch&iacute;nh l&agrave; Covid-19. M\u1edbi \u0111&acirc;y, trang ch\u1ee7 \u0111\u1ed9i b&oacute;ng \u0111&atilde; \u0111\u01b0a ra th&ocirc;ng b&aacute;o v\u1ec1 nh\u1eefng tr\u01b0\u1eddng h\u1ee3p m\u1edbi nh\u1ea5t d\u01b0\u01a1ng t&iacute;nh v\u1edbi Covid-19. Theo \u0111&oacute; c&oacute; 6 c\u1ea7u th\u1ee7 v&agrave; m\u1ed9t s\u1ed1 ch\u01b0a x&aacute;c \u0111\u1ecbnh thu\u1ed9c th&agrave;nh ph\u1ea7n ban hu\u1ea5n luy\u1ec7n. Ngo&agrave;i ra c&ograve;n c&oacute; th&ecirc;m 4 c\u1ea7u th\u1ee7 trong di\u1ec7n nguy c\u01a1 cao l&agrave; Romero, Son Heung Min, Lucas Moura v&agrave; Ben Davies.<\/p>\r\n<p style=\"text-align: center;\"><img src=\"\/picture\/News\/12\/8-tot.jpg\" alt=\"\" width=\"740\" height=\"444\"><\/p>\r\n<p style=\"text-align: justify;\">\u0110&acirc;y \u0111\u1ec1u l&agrave; nh\u1eefng c\u1ea7u th\u1ee7 tr\u1ee5 c\u1ed9t, th\u01b0\u1eddng xuy&ecirc;n ra s&acirc;n c\u1ee7a Tottenham. Ch&iacute;nh v&igrave; th\u1ebf, nguy c\u01a1 l&acirc;y nhi\u1ec5m ch&eacute;o gi\u1eefa c&aacute;c c\u1ea7u th\u1ee7 tr&ecirc;n s&acirc;n l&agrave; r\u1ea5t l\u1edbn. Trong tr\u1eadn g\u1eb7p Norwich v\u1eeba qua, HLV Conte \u0111&atilde; kh&ocirc;ng th\u1ec3 s\u1eed d\u1ee5ng Emerson Royal v&agrave; Bryan Gil v\u1edbi l&yacute; do &ldquo;b\u1ecb \u1ed1m&rdquo;. Tuy nhi&ecirc;n sau khi c&oacute; k\u1ebft qu\u1ea3 x&eacute;t nghi\u1ec7m, c\u1ea3 2 \u0111\u1ec1u \u0111&atilde; d\u01b0\u01a1ng t&iacute;nh v\u1edbi Covid-19.<\/p>\r\n<p style=\"text-align: justify;\">Ngu\u1ed3n l&acirc;y c\u1ee7a Tottenham \u0111ang b\u1ecb nghi ng\u1edd l&agrave; ti\u1ec1n \u0111\u1ea1o Ivan Toney c\u1ee7a Brentford. Ch&acirc;n s&uacute;t n&agrave;y \u0111&atilde; d\u01b0\u01a1ng t&iacute;nh ngay sau tr\u1eadn \u0111\u1ea5u v\u1edbi Tottenham. S\u1ef1 vi\u1ec7c c&agrave;ng tr\u1edf n&ecirc;n ph\u1ee9c t\u1ea1p khi Toney l&agrave; tr\u01b0\u1eddng h\u1ee3p duy nh\u1ea5t b&ecirc;n ph&iacute;a Brentford. V&igrave; th\u1ebf c&aacute;c chuy&ecirc;n gia y t\u1ebf nghi ng\u1edd r\u1eb1ng Toney \u0111&atilde; mang m\u1ea7m b\u1ec7nh t\u1eeb c\u1ed9ng \u0111\u1ed3ng v&agrave;o \u0111\u1ed9i b&oacute;ng.<\/p>\r\n<p style=\"text-align: justify;\">N\u1ebfu c&aacute;c ca d\u01b0\u01a1ng t&iacute;nh \u0111\u01b0\u1ee3c x&aacute;c nh\u1eadn l&agrave; thu\u1ed9c bi\u1ebfn th\u1ec3 Omicron, t\u1ea5t c\u1ea3 nh\u1eefng ai c&oacute; ti\u1ebfp x&uacute;c g\u1ea7n v\u1edbi ng\u01b0\u1eddi b\u1ecb d\u01b0\u01a1ng t&iacute;nh s\u1ebd ph\u1ea3i c&aacute;ch ly trong 10 ng&agrave;y b\u1ea5t k\u1ec3 \u0111&atilde; ti&ecirc;m vaccine hay ch\u01b0a, theo lu\u1eadt m\u1edbi c\u1ee7a ch&iacute;nh ph\u1ee7 Anh. Do \u0111&oacute; kh&ocirc;ng ch\u1ec9 Tottenham v&agrave; Brentford m&agrave; hi\u1ec7n c\u1ea3 Norwich l\u1eabn Leeds United &ndash; \u0111\u1ed9i v\u1eeba g\u1eb7p Brentford cu\u1ed1i tu\u1ea7n qua &ndash; \u0111\u1ec1u \u0111ang ki\u1ec3m tra PCR \u0111\u1ec3 x&aacute;c \u0111\u1ecbnh xem c&oacute; ai l&acirc;y nhi\u1ec5m.<\/p>\r\n<p style=\"text-align: justify;\">Trong tr\u01b0\u1eddng h\u1ee3p x\u1ea5u, c&aacute;c \u0111\u1ed9i b&oacute;ng kh&ocirc;ng th\u1ec3 ra s&acirc;n thi \u0111\u1ea5u khi c&oacute; c\u1ea7u th\u1ee7 nhi\u1ec5m bi\u1ebfn th\u1ec3 m\u1edbi Omicron n&ecirc;n Ngo\u1ea1i h\u1ea1ng Anh ch\u1eafc ch\u1eafn b\u1ecb \u1ea3nh h\u01b0\u1edfng. \u0110\u1ea7u n\u0103m 2021, Aston Villa \u0111&atilde; ph\u1ea3i t\u1ea1m ng\u01b0ng ho\u1ea1t \u0111\u1ed9ng 2 tu\u1ea7n do d\u1ecbch b\u1ec7nh v&agrave; Tottenham, Brenford \u0111ang ph\u1ea3i ch\u1edd quy\u1ebft \u0111\u1ecbnh cu\u1ed1i c&ugrave;ng c\u1ee7a c\u01a1 quan ch\u1ee9c n\u0103ng.<\/p>\r\n<p style=\"text-align: justify;\">Qu&yacute; kh&aacute;ch truy c\u1eadp <a href=\"\/\">keonhacai<\/a> h\u1eb1ng ng&agrave;y \u0111\u1ec3 nh\u1eadn th&ocirc;ng tin soi k&egrave;o, nh\u1eadn \u0111\u1ecbnh c&aacute;c tr\u1eadn \u0111\u1ea5u hot v&agrave; c\u1eadp nh\u1eadt tin t\u1ee9c b&oacute;ng \u0111&aacute; nhanh nh\u1ea5t.<\/p>","is_status":1,"is_robot":1,"displayed_time":"2021-12-08 16:10:38","thumbnail":"\/News\/12\/8-tot.jpg","type":"post","is_featured":0,"user_id":41}';
        $data = json_decode($a);
        dd($data);
    }

    public function restore($id){
        $params = [
          'id' => $id,
        ];
        $logData = $this->_data_log->single($params,$this->_data_log->table);
        if(!empty($logData)){
            $data = json_decode($logData->data);
            if($logData->module === 'category'){
                if($id = $this->_data_category->save($data)){
                    echo "Restore $id Thành công" . getUrlCategory($data);
                }else{
                    echo "Restore Thất bại";
                }
            }else{
                if($this->_data_post->save($data)){
                    echo "Restore Thành công" . getUrlPost($data);
                }else{
                    echo "Restore Thất bại";
                }
            }
        }else{
            echo "No data";
        }

    }
}

