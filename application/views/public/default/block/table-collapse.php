<section class="section mt-70">
    <section class="section-header category-name">
        <h1 class="category-title">Live Core</h1>
    </section>
    <section class="section-body">
        <div class="table-collapse table-result-bongda">
            <table class="table table-header">
                <thead class="main-heading">
                    <tr>
                        <td>Thời gian</td>
                        <td style="width: 150px;">Đội nhà</td>
                        <td>Tỉ số</td>
                        <td style="width: 150px;">Đội khách</td>
                    </tr>
                </thead>
            </table>
            <?php if (!empty($data_match_group)) : ?>
                <?php foreach ($data_match_group as $tournament_name => $matches) : ?>
                    <table class="table table-value <?= $tournament_name === array_key_first($data_match_group) ? 'active' : '' ?>">
                        <thead class="sub-heading">
                            <tr>
                                <td colspan="4"><?php echo !empty($matches[0]->league_name) ? $matches[0]->league_name : 'Đang cập nhật'; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($matches)) foreach ($matches as $item) : ?>
                                <tr>
                                    <td>
                                        <?php if ($item->match_status === 'live') : ?>
                                            <span class="text-warning">
                                                <?php echo !empty($item->elapsed) ? $item->elapsed . "'" : date('d/m/Y', $item->start_timestamp); ?>
                                            </span>
                                        <?php else : ?>
                                            <?php echo time() + 60 * 60 > $item->start_timestamp ? '<span class="text-info">Sắp đá ' . timeAgo($item->start_time, "H:i") . '</span>' : "<span>" . timeAgo($item->start_time, "H:i d/m") . "</span>" ?>
                                        <?php endif; ?>
                                    </td>
                                    <td style="width: 150px;"><?php echo $item->name_home ?></td>
                                    <td>
                                        <?php if (!empty($item->elapsed)) : ?>
                                            <span><b><?php echo $item->score_home ?></b></span>
                                            <span> - </span>
                                            <span><b><?php echo $item->score_away ?></b></span>
                                        <?php else : ?>
                                            <span><b><?php echo date('H:i', $item->start_timestamp) ?></b></span>
                                        <?php endif; ?>
                                    </td>
                                    <td style="width: 150px;">
                                        <?php echo $item->name_away ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            <?php endif; ?>
            </table>
        </div>
    </section>
</section>