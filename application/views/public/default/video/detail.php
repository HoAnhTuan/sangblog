<?php if (!empty($oneItem)) : ?>
    <main class="container main-detail" data-url="<?php echo getUrlPost($oneItem) ?>">
        <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
            <article class="col-12 col-lg-8">

                <h1 class="title-home font-weight-bold mt-3">
                    <?php echo getStringCut($oneItem->title, 0, 120) ?>
                </h1>
                <div class="d-flex flex-wrap justify-content-between my-3">
                    <div class="font-13 text-secondary"><?php echo date_post_vn($oneItem->displayed_time) ?> - <?php echo timeAgo($oneItem->displayed_time, "H:i") ?></div>
                    <?php $this->load->view(TEMPLATE_PATH . "block/rate") ?>
                </div>
                <div class="font-14 font-weight-bold">
                    <?php echo $oneItem->description ?>
                </div>
                <div class="content-news text-justify  mt-3">
                    <?php if (!empty($oneItem->video)) :
                        //                        $urlVideo = getUrlPlayerVideo($oneItem);
                        $urlVideo = $oneItem->video;
                    ?>
                        <iframe width="100%" height="500" src="<?php echo $urlVideo ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                    <?php endif; ?>
                    <?php echo getTableOfContent($oneItem->content, $oneItem->title) ?>
                </div>

                <div class="social-share p-3 text-center border-bottom"></div>
                <div class="list-tag text-nowrap overflow-auto my-3">
                    <?php if (!empty($data_tag)) foreach ($data_tag as $item) : ?>
                        <a href="<?php echo getUrlTag($item) ?>" title="<?php echo $item->title ?>" class="btn btn-secondary text-white mr-2"><?php echo $item->title ?></a>
                    <?php endforeach; ?>
                </div>

                <div class="row">
                    <?php if (!empty($related_post)) foreach ($related_post as $item) : ?>
                        <div class="col-6 col-lg-4">
                            <a href="javascript:;" title="<?php echo $item->category_title ?>" class="input-comment text-uppercase text-white py-1 px-2 d-inline-block font-11 position-absolute cate-absolute"><?php echo $item->category_title ?></a>
                            <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                <?php echo getThumbnail($item, 350, 200, "img-fluid w-100 mb-3") ?>
                            </a>
                            <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                <h3 class="font-16 text-black2 max-line-2"><?php echo $item->title ?></h3>
                            </a>
                            <span class="text-secondary font-12"><?php echo timeAgo($item->displayed_time, "d/m/Y") ?></span>
                            <p class="max-line-2 text-secondary font-13"><?php echo $item->description ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </article>
            <div class="col-12 col-lg-4">
                <?php $this->load->view(TEMPLATE_PATH . 'block/new_post') ?>
                <div class="position-sticky" style="top: 45px">
                    <?php echo showContainerBanner('sidebar_middle', 0, '') ?>
                    <iframe id="chatbox" src="https://www5.cbox.ws/box/?boxid=927201&boxtag=3jw66g" width="100%" height="500" allowtransparency="yes" allow="autoplay" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto"></iframe>
                    <?php echo showContainerBanner('sidebar_bottom', 0, '') ?>
                </div>
            </div>
        </div>
    </main>
<?php endif; ?>