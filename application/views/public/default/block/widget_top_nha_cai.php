<div class="home-nhacai tab-content">
    <div class="head-nhacai py-2">
        <ul class="nav">
            <li class="nav-item mr-1">
                <a class="nav-link active btn btn-nhacai" data-toggle="tab" href="#nhacai">TOP NHÀ
                    CÁI</a>
            </li>
        </ul>
    </div>
    <div class="blocks tab-pane active" id="nhacai">
        <?php if (!empty($nha_cai)) : ?>
            <?php foreach ($nha_cai as $key => $item) : $data_dealer = json_decode($item->data_dealer); ?>
                <div class="block-nhacai">
                    <div class="row mb-3">
                        <div class="col-2">
                            <div class="rank-nhacai">
                                <span class="rank"><?php echo $item->order ?></span>
                            </div>
                        </div>
                        <div class="col-3">
                            <a href="<?php echo getUrlPost($item) ?>"
                               title="<?php echo $item->title ?>">
                                <?php echo getThumbnail($item, 60, 36, "nhacai-logo", false) ?>
                            </a>
                        </div>
                        <div class="col-6">
                            <h4 class="mb-1">
                                <a href="<?php echo getUrlPost($item) ?>"
                                   title="<?php echo $item->title ?>">
                                    <?php echo $data_dealer->title ?>
                                </a>
                            </h4>
                            <div>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <p class="font-weight-bold my-2 px-4"><?php echo $data_dealer->promote ?></p>
                    </div>
                    <div class="head-nhacai d-flex justify-content-around">
                        <a class="btn btn-gamebai bet-bottom" rel="nofollow" target="_blank"
                           href="https://sv88vn.vip/home"
                           title="<?php echo $item->title ?>">CƯỢC NGAY</a>
                        <a class="btn btn-nhacai review-bottom"
                           href="<?php echo getUrlPost($item) ?>"
                           title="<?php echo $item->title ?>">XEM REVIEW</a>
                    </div>
                    <hr>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>