<?php if (!empty($oneItem)): ?>
    <main>
	    <div class="container">
        <div class="row">
            <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
            <div class="col-12 col-lg-9">
		            <script async src="https://cse.google.com/cse.js?cx=2e7bf2108cc04f554"></script>
		            <div class="gcse-search"></div>
                <?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer.php') ?>
            </div>
		        <div class="col-12 col-lg-3">
	              <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_news') ?>
	              <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
		        </div>
        </div>
	    </div>
    </main>
<?php endif; ?>