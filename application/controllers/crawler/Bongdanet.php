<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

class Bongdanet extends Crawler_Controller
{
    private $_category;
    private $_post;
    private $_base_url;
    private $_client;
    private $_list_cate;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['category_model', 'post_model']);
        $this->_category = new Category_model();
        $this->_post = new Post_model();
        $this->_client = new Client(HttpClient::create(['timeout' => 60]));
        $this->_base_url = 'https://bongdanet.vn';
        $this->_list_cate = [
            'https://bongdanet.vn/nhan-dinh-bong-da/vl-world-cup-kv-concacaf/p2' => 21,
        ];
    }

    public function start()
    {
        foreach ($this->_list_cate as $slug => $cate_id) {
            $arr = explode('/', $slug);
            $page = end($arr);
            $slug = str_replace($page, '', $slug);
            $page = ltrim($page, 'p');

            for ($i = $page; $i >= 0; $i--) {
                echo $i . "\n";
                $this->getCategory($slug . 'p' . $i, $cate_id, "soikeo");
            }
        }
        die("END - Crawler from site {$this->_base_url}\n");
    }

    private function getCategory($url, $category_id, $type = 'post')
    {
        try {
            $bot_crawler = $this->_client->request('GET', $url);
            echo "Start Crawler article from category => {$bot_crawler->getUri()} \n";
            $listDetail = $bot_crawler->filter('.list-all-content-news-category .news-item > a')->each(function ($node) {
                return $this->_base_url . $node->attr("href");
            });
            $listDetail = array_reverse($listDetail);
            if (!empty($listDetail)) foreach ($listDetail as $linkDetail) {
                $bot_crawler_detail = $this->_client->request('GET', $linkDetail);
                $data['title'] = $data['meta_title'] = $bot_crawler_detail->filter("h1")->text();
                $data['slug'] = $this->toSlug($data['title']);
                if (!$this->_post->checkExistByField("slug", $data['slug'])) {
                    $data['description'] = $data['meta_description'] = $bot_crawler_detail->filter('meta[property="og:description"]')->attr("content");
                    $content = $bot_crawler_detail->filter('.detail-news-content .content-article .article-body')->html();
                    if (!empty($content)) {
                        $content = $this->cleanContent($content);
                        $content = str_replace('Bongdanet','',$content);
                        $imgs = $bot_crawler_detail->filter('.detail-news-content .content-article .article-body img')->each(function ($node) {
                            if (preg_match('/http/i', $node->attr('src'))) $imgSrc = $node->attr('src');
                            else $imgSrc = $this->_base_url . $node->attr('src');
                            $imgSrc = strtok($imgSrc, '?');
                            $srcNew = $this->downloadImage($imgSrc, '', 'soi-keo');
                            return [
                                'old' => $node->attr('src'),
                                'new' => !empty($srcNew) ? "/" . MEDIA_NAME . $srcNew : ''
                            ];
                        });

                        if (!empty($imgs)) foreach ($imgs as $img) {
                            $content = str_replace($img['old'], $img['new'], $content);
                        }
                        $content = $this->cleanContent($content);

                        if ($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'))
                            $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'), '', 'soi-keo');
                        else
                            $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('.detail-news-content .content-article .article-body img', 0)->attr('src'), '', 'soi-keo');

                        $content = '<img alt="'.$data['title'].'" src="'.MEDIA_NAME . $data['thumbnail'].'">'.$content;

                        $data['content'] = $content;
                        $data['type'] = $type;
                        $data['is_robot'] = 1;
                        $data['user_id'] = 1;
                        if ($id = $this->_post->save($data)) {
                            $data_category = [
                                "post_id" => $id,
                                'category_id' => $category_id,
                                'is_primary' => false
                            ];
                            if ($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)) {
                                echo "Update category bài viết $id thành công ! \n";
                            }
                            echo "Thêm bài viết $id thành công ! \n";
                        }
                    }
                /*} else {
                    $data['description'] = $data['meta_description'] = $bot_crawler_detail->filter('meta[property="og:description"]')->attr("content");
                    $content = $bot_crawler_detail->filter('.detail-news-content .content-article .article-body')->html();
                    if (!empty($content)) {
                        $content = $this->cleanContent($content);

                        $imgs = $bot_crawler_detail->filter('.detail-news-content .content-article .article-body img')->each(function ($node) {
                            if (preg_match('/http/i', $node->attr('data-src'))) $imgSrc = $node->attr('data-src');
                            else $imgSrc = $this->_base_url . $node->attr('data-src');
                            $imgSrc = strtok($imgSrc, '?');
                            $srcNew = $this->downloadImage($imgSrc, '', 'soi-keo');
                            return [
                                'old' => $node->attr('data-src'),
                                'new' => "/" . MEDIA_NAME . $srcNew
                            ];
                        });
                        if (!empty($imgs)) foreach ($imgs as $img) {
                            $content = str_replace($img['old'], $img['new'], $content);
                        }

                        $data['content'] = $content;

                        if ($bot_crawler_detail->filter('meta[property="og:image"]')->count() > 0)
                            $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'), '', 'soi-keo');
                        else
                            $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('.detail-news-content .content-article .article-body img', 0)->attr('data-src'), '', 'soi-keo');

                        $data['type'] = $type;

                        dd($data);
                        if ($this->_post->update(['slug', $data['slug']], $data)) {
                            echo "Update lại bài viết {$data['slug']} thành công ! \n";
                        }
                    }*/
                }
            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }

    public function updateContent()
    {
        $params = [
            'category_id' => 399,
            'order' => ['id' => 'desc'],
            'limit' => 20000
        ];
        $post = $this->_post->getData($params);
        foreach ($post as $item) {
            $url = 'https://asoikeo.com/' . $item->slug . "-p$item->id.html";
            try {
                $bot_crawler = $this->_client->request('GET', $url);
                $checkBase64 = $bot_crawler->filter('meta[property="og:image"]')->attr('content');

                if (strpos($checkBase64, 'gifbase64') !== false) {
                    if ($this->_post->checkExistByField("slug", $item->slug)) {
                        $imgs = $bot_crawler->filter('.post-content img')->each(function ($node) {
                            $imgSrc = $node->attr('data-src');
                            $srcNew = $this->downloadImage($imgSrc, '', 'soi-keo');
                            return [
                                'old' => $node->attr('data-src'),
                                'new' => "/" . MEDIA_NAME . $srcNew
                            ];
                        });
                        if (!empty($imgs[0]['new'])) {
                            $data['thumbnail'] = $this->downloadImage($imgs[0]['new'], '', 'soi-keo');
                        }
                        $checkErrImg = 0;
                        $old_content = $item->content ?? '';
                        $new_content = $old_content;
                        if (!empty($imgs)) foreach ($imgs as $img) {
                            if (!empty($img['new'])) {
                                $checkErrImg = 1;
                                $nameImg = $img['new'];
                                $countString = strlen($nameImg);
                                $textCut = strrpos($nameImg, "/");
                                $endOfString = $countString - $textCut;
                                $directory = substr($nameImg, 0, $countString - $endOfString);
                                $newNameImg = '/media/soi-keo' . str_replace($directory, '', $nameImg);
                                $base64 = 'src="/media/soi-keo/gifbase64r0lgoddhaqabapaaampdwwaaacwaaaaaaqabaaacakqbads.png"';
                                $new_content = str_replace($base64, "", $new_content);
                                $new_content = str_replace("data-src", "src", $new_content);
                                $new_content = str_replace($img['old'], $newNameImg, $new_content);
                            }
                        }
                        if ($checkErrImg == 1) {
                            $data['content'] = $new_content;
                            $this->db->where('id', $item->id);
                            if ($this->db->update('post', $data)) {
                                echo "Update lại bài viết {$item->id} thành công ! \n";
                            } else {
                                echo "Update  bài viết {$item->id} thất bại ! \n";
                            }
                        }

                    }
                } else {
                    echo "ko update $item->id" . '</br>';
                }
            } catch (Exception $e) {
                echo $e->getCode() . '\\n';
                echo $e->getMessage() . '\\n';
            }

        }
    }
}

