<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

class Bongdaplus extends Crawler_Controller
{
    private $_category;
    private $_post;
    private $_base_url;
    private $_client;
    private $_list_cate;

    public function __construct(){
        parent::__construct();
        $this->load->model(['category_model','post_model']);
        $this->_category     = new Category_model();
        $this->_post         = new Post_model();
        $this->_client = new Client(HttpClient::create(['timeout' => 60]));
        $this->_base_url = 'https://bongdaplus.vn';
        $this->_list_cate = [
          'https://bongdaplus.vn/catnewslatest/tennis/' => 22,
          'https://bongdaplus.vn/catnewslatest/the-thao/' => 24
        ];
    }

    public function start(){
        foreach ($this->_list_cate as $slug => $cate_id){
            for($i = 2; $i >= 0; $i--){
                $this->getCategory($slug . $i, $cate_id, "post");
            }
        }
        die("END - Crawler from site {$this->_base_url}\n");
    }

    private function getCategory($url, $category_id, $type = 'post'){
        try {
            $bot_crawler = $this->_client->request('GET', $url);
            echo "Start Crawler article from category => {$bot_crawler->getUri()} \n";
            $listDetail = $bot_crawler->filter('.lst .news a.thumb.lft')->each(function ($node) {
                return $this->_base_url . $node->attr("href");
            });
            $listDetail = array_reverse($listDetail);
            if(!empty($listDetail)) foreach ($listDetail as $linkDetail) {
                $bot_crawler_detail = $this->_client->request('GET', $linkDetail);
                $title = $data['meta_title'] = $bot_crawler_detail->filter("h1")->text();
                $data['title'] = substr($title, 0, 55);
                $data['slug'] = $this->toSlug($data['title']);
                if (!$this->_post->checkExistByField("slug",$data['slug'])) {
                    echo "---------Crawl content from => $linkDetail ----------------- \n";
                    if (!$this->_post->checkExistByField("slug", $data['slug'])) {
                        $desription = $data['meta_description'] = $bot_crawler_detail->filter('meta[property="og:description"]')->attr("content");
                        $data['description'] = substr($desription, 0, 120);
                        $content = $bot_crawler_detail->filter('#postContent')->html();
                        if (!empty($content)) {
                            $content = $this->cleanContent($content);
                            $imgs = $bot_crawler_detail->filter('#postContent img')->each(function ($node) {
                                if (preg_match('/http/i', $node->attr('src'))) $imgSrc = $node->attr('src');
                                else $imgSrc = $this->_base_url . $node->attr('src');
                                $srcNew = $this->downloadImage($imgSrc, '', 'post');
                                return [
                                  'old' => $node->attr('src'),
                                  'new' => !empty($srcNew) ? "/" . MEDIA_NAME . $srcNew : ''
                                ];
                            });
                            if (!empty($imgs)) foreach ($imgs as $img) {
                                $content = str_replace($img['old'], $img['new'], $content);
                            }
                            $content = $this->cleanContent($content);
                            if ($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'))
                                $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('meta[property="og:image"]')->attr('content'), '', $type);
                            else
                                $data['thumbnail'] = $this->downloadImage($bot_crawler_detail->filter('#postContent img', 0)->attr('src'), '', $type);

                            $data['content'] = $content;

                            $data['type'] = $type;
                            $data['is_robot'] = 1;
                            $data['user_id'] = 1;
                            if ($id = $this->_post->save($data)) {
                                $data_category = [
                                  "post_id" => $id,
                                  'category_id' => $category_id,
                                  'is_primary' => false
                                ];
                                if ($this->_post->insertOnUpdate($data_category, $this->_post->table_post_category)) {
                                    echo "Update category bài viết $id thành công ! \n";
                                }
                                echo "Thêm bài viết $id thành công ! \n";
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getCode() . '\\n';
            echo $e->getMessage() . '\\n';
        }
    }
}

