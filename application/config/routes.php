<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//remove trailing slash from uri
/*if (isset($_SERVER['REQUEST_URI'])){
    $uri = $_SERVER['REQUEST_URI'];
    if(($uri != "/") and preg_match('{/$}',$uri) ) {
        header( "HTTP/1.1 301 Moved Permanently" );
        header ('Location: '.preg_replace('{/$}', '', $uri));
        exit();
    }

    if(preg_match('~[A-Z]~', $uri)) {
        header( "HTTP/1.1 301 Moved Permanently" );
        header ('Location: ' . strtolower ( $uri ));
        exit();
    }
    if(substr($uri, -1) !== "/" && preg_match("/admin/i", $uri) != true){
        header( "HTTP/1.1 301 Moved Permanently" );
        header('Location: ' . strtolower ( $uri ) . "/");
    }
}*/
$route['default_controller'] = 'home';
$route['default'] = 'home';
$route['translate_uri_dashes'] = FALSE;
$route['404.html'] = 'page/notfound';
$route['admin'] = 'admin/dashboard';

/*Tournament*/
//$route['giai-dau/(:any)'] = 'category/detail/$1';

/*post*/
$route['(:any)-p(:num)'] = 'post/detail/$1/$2';


/*Vote*/
$route['reviews/ajax_vote']  = 'reviews/ajax_vote';

$route['tags/(:any)'] = 'category/tags/$1';
$route['tags/(:any)/(:num)'] = 'category/tags/$1/$2';

/*Search*/
$route['tim-kiem']  = 'search/index';

/*Sitemap*/
$route['sitemap-news.xml']  = 'seo/sitemap_google_news';
$route['rss_google_news.rss']  = 'seo/rssGoogleNews';
$route['sitemap.xml']  = 'seo/sitemap';
$route['sitemap-category.xml'] = 'seo/sitemap_category';
$route['sitemap-page.xml'] = 'seo/sitemap_page';
$route['sitemap-post-(\d\d\d\d_\d\d).xml'] = 'seo/sitemap_post/$1';

//$route['sitemap-home.xml']  = 'seo/sitemap_home';
//$route['sitemap-match.xml'] = 'seo/sitemap_match';
/*Sitemap*/

/*Match Live*/
$route['(:any)-l(:num).html'] = 'match/detail/$2';
//$route['x_o-(:any)-w(:num)-c(:any)'] = 'player/watch_other/$1/$2/$3';
//$route['x-(:any)-w(:num)-c(:any)'] = 'player/watch/$1/$2/$3';
$route['video-(:any)-l(:num)'] = 'player/watch_video/$1/$2';
/*author*/
$route['(:any)-a(:num).html'] = 'category/author/$2';
$route['(:any)-a(:num).html/(:num)'] = 'category/author/$2/$3';
/*Category*/
$route['(:any).html'] = 'category/detail/$1';
$route['(:any).html/(:num)'] = 'category/detail/$1/$2';




