<main class="bg-light">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="title-home main-title font-weight-bold pb-2 mb-0 mb-lg-1 text-center"><?php if (!empty($this->_settings->meta_title)) echo $this->_settings->meta_title ?></i></h1>
			</div>
			<div class="col-12 col-md-6">
				<?php echo showContainerBanner('top_left', 0, '') ?>
			</div>

			<div class="col-12 col-md-6">
				<?php echo showContainerBanner('top_right', 0, '') ?>
			</div>

			<div class="col-12 col-lg-9 mt-lg-3 pt-4">
				<?php $this->load->view(TEMPLATE_PATH . 'home/_soikeo_noibat') ?>
				<div class="widget-button">
					<?php echo menusButton(4, '', '', ''); ?>
				</div>

			</div>
			<div class="col-12 col-lg-3 pt-4">
				<?php echo showContainerBanner('sidebar_top', 0, '') ?>
				<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_news') ?>
			</div>
		</div>
	</div>

	<div class="container-fluid mt-2">
		<!--		--><?php //$this->load->view(TEMPLATE_PATH . 'home/_nhandinh') 
						?>
	</div>

	<div class="container  mt-5 pt-4">
		<div class="row">
			<div class="col-12 col-lg-9">
				<!--soi kèo bóng đá-->
				<?php $this->load->view(TEMPLATE_PATH . 'home/_soikeo') ?>
				<!--soi kèo bóng đá-->

				<?php $this->load->view(TEMPLATE_PATH . 'block/news_soccer.php') ?>

				<!--TOP NHA CAI-->
				<?php $this->load->view(TEMPLATE_PATH . 'home/top_nhacai') ?>

				<?php echo showContainerBanner('home_middle', 0, '') ?>

				<?php $this->load->view(TEMPLATE_PATH . 'block/home_tlk') ?>
			</div>
			<div class="col-12 col-lg-3">
				<?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
			</div>
			<div class="col-12 col-md-6">
				<?php echo showContainerBanner('home_footer_left', 0, '') ?>
			</div>
			<div class="col-12 col-md-6">
				<?php echo showContainerBanner('home_footer_right', 0, '') ?>
			</div>
		</div>
	</div>
	<?php if (!empty($this->_settings->content)) $this->load->view(TEMPLATE_PATH . 'block/_content_footer', ['content' => $this->_settings->content]) ?>
</main>