<?php if (!empty($oneItem)) : ?>
    <main class="">
        <div class="container">
            <div class="row">
                <?php $this->load->view(TEMPLATE_PATH . "block/breadcrumb") ?>
                <div class="col-12 col-md-6">
                    <?php echo showContainerBanner('top_left', 0, '') ?>
                </div>
                <div class="col-12 col-md-6">
                    <?php echo showContainerBanner('top_right', 0, '') ?>
                </div>
                <div class="col-12">
                    <h1 class="title-home font-weight-bold my-3 text-center">
                        <?php echo $oneItem->title ?>
                    </h1>

                </div>
                <div class="col-12 col-md-9">
                    <?php $this->load->view(TEMPLATE_PATH . '/block/author') ?>
                    <div id="ajax_content" class="row ajax-content">
                        <?php if (!empty($list_post)) foreach ($list_post as $item) : ?>
                            <div class="col-12 col-md-6">
                                <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                    <?php echo getThumbnail($item, 350, 200, "img-fluid w-100 mb-3") ?>
                                </a>
                                <a href="<?php echo getUrlPost($item) ?>" title="<?php echo $item->title ?>">
                                    <h3 class="font-16 text-black2 max-line-2"><?php echo $item->title ?></h3>
                                </a>
                                <span class="text-secondary font-12"><?php echo timeAgo($item->displayed_time, "d/m/Y") ?></span>
                                <p class="max-line-2 text-secondary font-13"><?php echo $item->description ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="text-center">
                        <button class="btn mx-auto my-3 btnLoadMore" data-page="2" data-url="<?php echo getUrlAuthor($oneItem) ?>" type="button">Xem thêm</button>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_news') ?>
                    <?php $this->load->view(TEMPLATE_PATH . 'block/sidebar_cado') ?>
                    <div class="position-sticky" style="top: 45px">
                        <?php echo showContainerBanner('sidebar_middle', 0, '') ?>
                        <?php echo showContainerBanner('sidebar_bottom', 0, '') ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php endif; ?>