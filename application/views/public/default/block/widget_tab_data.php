<div>
    <ul class="nav nav-tabs text-uppercase text-center menu-bxh font-weight-bold" id="myTab" role="tablist">
        <li class="nav-item col px-0 border-0">
            <a class="nav-link active text-white border-0" id="home-tab" data-toggle="tab" href="#kq" role="tab" aria-selected="true">kết quả</a>
        </li>
        <li class="nav-item col px-0 border-0">
            <a class="nav-link text-white border-0" id="profile-tab" data-toggle="tab" href="#ltd" role="tab" aria-selected="false">ltđ</a>
        </li>
        <li class="nav-item col px-0 border-0">
            <a class="nav-link text-white border-0" id="contact-tab" data-toggle="tab" href="#bxh" role="tab" aria-selected="false">bxh</a>
        </li>
    </ul>
    <div class="tab-content p-3 border shadow" id="myTabContent">
        <div class="tab-pane fade show active" id="kq" role="tabpanel">
            <ul class="list-unstyled mb-0">
                <?php $data_result = getDataCategory('result');if(!empty($data_result)) foreach ($data_result as $item): if($item->tournament_id != 36): ?>
                    <li class="d-flex align-items-center">
                        <?php echo getLogoTournament($item->tournament_id,$item->title,"logo-tournament mr-2") ?>
                        <a href="<?php echo getUrlPage($item) ?>" title="<?php echo $item->title ?>" class="text-black2 btn text-left"><?php echo $item->name ?></a>
                    </li>
                <?php endif; endforeach; ?>
            </ul>
        </div>
        <div class="tab-pane fade" id="ltd" role="tabpanel">
            <ul class="list-unstyled mb-0">
                <?php $data_schedule = getDataCategory('schedule');if(!empty($data_schedule)) foreach ($data_schedule as $item): if($item->tournament_id != 36): ?>
                    <li class="d-flex align-items-center">
                        <?php echo getLogoTournament($item->tournament_id,$item->title,"logo-tournament mr-2") ?>
                        <a href="<?php echo getUrlPage($item) ?>" title="<?php echo $item->title ?>" class="text-black2 btn text-left"><?php echo $item->name ?></a>
                    </li>
                <?php endif; endforeach; ?>
            </ul>
        </div>
        <div class="tab-pane fade" id="bxh" role="tabpanel">
            <ul class="list-unstyled mb-0">
                <?php $data_rank = getDataCategory('rank');if(!empty($data_rank)) foreach ($data_rank as $item): if($item->tournament_id != 36): ?>
                    <li class="d-flex align-items-center">
                        <?php echo getLogoTournament($item->tournament_id,$item->title,"logo-tournament mr-2") ?>
                        <a href="<?php echo getUrlPage($item) ?>" title="<?php echo $item->title ?>" class="text-black2 btn text-left"><?php echo $item->name ?></a>
                    </li>
                <?php endif; endforeach; ?>
            </ul>
        </div>
    </div>
</div>