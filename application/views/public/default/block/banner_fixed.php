<aside class="container position-relative">
    <div class="BannerStickyLeft d-none d-md-block">
        <div class="BannerStickyLeft_content" style="">
            <?php echo showContainerBanner('fixed_left',2, '') ?>
        </div>
    </div>
    <div class="BannerStickyRight d-none d-md-block">
        <div class="BannerStickyRight_content" style="">
            <?php echo showContainerBanner('fixed_right',2, '') ?>
        </div>
    </div>
</aside>
