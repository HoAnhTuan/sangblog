<?php
    $list_tag = getTag();
    if (!empty($list_tag)) :
?>
        <div class="widget widget-tag__most__viewed">
            <h2 class="widget-title">Từ khóa</h2>

            <div class="list-tag">
                <?php foreach ($list_tag as $key => $item) : ?>
                    <a class="tag" href="<?php echo getUrlTag($item) ?>"><?php echo $item->title ?></a>
                <?php endforeach; ?>
            </div>
        </div>

<?php endif;
